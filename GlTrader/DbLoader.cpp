#include "stdafx.h"
#include "DbLoader.h"
#include "DbHelper.h"
#include "Entity.h"
#include "Constants.h"
#include "Utils.h"
#include "Config.h"
#include "OrderMgr.h"
#include "Context.h"

DbLoader* DbLoader::m_pInstance;

DbLoader::DbLoader()
    : m_pCfgDbHelper(nullptr)
    , m_pInDbHelper(nullptr)
    , m_pOutDbHelper(nullptr)
{
}


DbLoader::~DbLoader()
{
    SAFE_DELETE(m_pCfgDbHelper);
    SAFE_DELETE(m_pInDbHelper);
    SAFE_DELETE(m_pOutDbHelper);
}

DbLoader* DbLoader::getInstance()
{
    if (m_pInstance == nullptr)
    {
        m_pInstance = new DbLoader();
    }

    return m_pInstance;
}

void DbLoader::destoryInstance()
{
    SAFE_DELETE(m_pInstance);
}

void DbLoader::setContext(Context* pContext)
{
    m_pContext = pContext;
}

bool DbLoader::initialize(QString& strErrMsg)
{
    bool success = false;
    do
    {
        if (!createDbDir())
        {
            strErrMsg = QStringLiteral("创建目录") + CONSTANT::DIR_DB + QStringLiteral("失败");
            break;
        }

        if (!initCfgDb())
        {
            strErrMsg = QStringLiteral("加载数据库文件") + CONSTANT::CONFIG_DB + QStringLiteral("失败");
            break;
        }

        //必须先初始化out.db，以生成当日委托和成交列表
        if (!initOutDb())
        {
            strErrMsg = QStringLiteral("初始化数据库文件") + Utils::getOutDbName().toUtf8().data() + QStringLiteral("失败");
            break;
        }

        if (!initInDb())
        {
            strErrMsg = QStringLiteral("初始化数据库文件") + Utils::getInDbName().toUtf8().data() + QStringLiteral("失败");
            break;
        }

        success = true;
    } while (0);

    return success;
}

bool DbLoader::createDbDir()
{
    // 检查目录是否存在，若不存在则新建
    QDir dir ;
    if (!dir.exists(CONSTANT::DIR_DB))
    {
        if (!dir.mkpath(CONSTANT::DIR_DB))
        {
            qCritical() << "Create dir failed, dir = " << CONSTANT::DIR_DB;
            return false;
        }
    }

    return true;
}

bool DbLoader::initCfgDb()
{
    m_pCfgDbHelper = new DbHelper;

    if (!m_pCfgDbHelper->open(CONSTANT::CONFIG_DB))
    {
        qCritical() << "Open database failed: " << CONSTANT::CONFIG_DB;
        SAFE_DELETE(m_pCfgDbHelper);
        return false;
    }

    bool success = false;
    do
    {
        if (!initDict()) break;

        success = true;
    } while (0);

    m_pCfgDbHelper->close();
    SAFE_DELETE(m_pCfgDbHelper);

    return success;
}

bool DbLoader::initDict()
{
    //tdict 数据字典表
    DbRecordSetPtr pRecordSet = new DbRecordSet();
    int cnt = m_pCfgDbHelper->getRecord(DBTABLE::TDICT::TABLE, pRecordSet);
    if (cnt <= 0)
    {
        qCritical() << "DbLoader::initDict failed";
        return false;
    }

    for (unsigned i = 0; i < pRecordSet->size(); i++)
    {
        DbKVPtr pRecord = pRecordSet->at(i);

        int iDictId = (*pRecord)[DBTABLE::TDICT::COL_DICT_ID].toInt();
        QString& strItem = (*pRecord)[DBTABLE::TDICT::COL_ITEM];
        QString& strRemark = (*pRecord)[DBTABLE::TDICT::COL_REMARK];

        auto pMap = m_pContext->queryDict(iDictId);
        pMap->insert(strItem, strRemark);
    }
    DbHelper::freeResultSet(pRecordSet);

    return true;
}

bool DbLoader::createInDbTable()
{
    //TODO 这个文件里所有的sql、表名、字段名不能写死，都要用宏定义代替
    const char* pTEntrustCreateSql = 
        "CREATE TABLE IF NOT EXISTS tentrust ("\
        "  id INTEGER PRIMARY KEY AUTOINCREMENT,"\
        "  entrust_time TIMESTAMP NOT NULL"\
        "  DEFAULT (datetime('now', 'localtime') ),"\
        "  batch_no INTEGER,"\
        "  account_code TEXT (32),"\
        "  asset_no TEXT (16),"\
        "  combi_no TEXT (16),"\
        "  market_no TEXT (3),"\
        "  stock_code TEXT (16),"\
        "  entrust_direction TEXT (4),"\
        "  price_type TEXT (1),"\
        "  entrust_price DOUBLE,"\
        "  entrust_amount DOUBLE,"\
        "  ext_access_system_id INTEGER"\
        ");";

    QString strErrMsg;
    bool bRet = m_pInDbHelper->execSql(pTEntrustCreateSql, strErrMsg);
    if (!bRet)
    {
        qCritical() << "Create table error: " << strErrMsg;
        return false;
    }

    const char* pTWithdrawCreateSql =
        "CREATE TABLE IF NOT EXISTS twithdraw ("\
        "  id INTEGER PRIMARY KEY AUTOINCREMENT,"\
        "  entrust_time TIMESTAMP NOT NULL"\
        "  DEFAULT (datetime('now', 'localtime') ),"\
        "  entrust_no INTEGER"\
        "  );";
    bRet = m_pInDbHelper->execSql(pTWithdrawCreateSql, strErrMsg);
    if (!bRet)
    {
        qCritical() << "Create table error: " << strErrMsg;
        return false;
    }

    return true;
}

bool DbLoader::createOutDbTable()
{
    const char* pTEntrustListCreateSql =
        "CREATE TABLE IF NOT EXISTS tentrustlist ("\
        "  id INTEGER PRIMARY KEY,"\
        "  business_time TIMESTAMP,"\
        "  entrust_no INTEGER,"\
        "  entrust_status TEXT (1),"\
        "  account_code TEXT (32),"\
        "  asset_no TEXT (16),"\
        "  combi_no TEXT (16),"\
        "  market_no TEXT (3),"\
        "  stock_code TEXT (16),"\
        "  entrust_direction TEXT (4),"\
        "  price_type TEXT (1),"\
        "  entrust_price DOUBLE,"\
        "  entrust_amount DOUBLE,"\
        "  cancel_deal_amount DOUBLE,"\
        "  fail_cause TEXT (256),"\
        "  ext_access_system_id INTEGER,"\
        "  ext_system_id INTEGER"\
        ");";

    QString strErrMsg;
    bool bRet = m_pOutDbHelper->execSql(pTEntrustListCreateSql, strErrMsg);
    if (!bRet)
    {
        qCritical() << "Create table error: " << strErrMsg;
        return false;
    }

    const char* pTDealListCreateSql =
        "CREATE TABLE IF NOT EXISTS tdeallist (	"\
        "	deal_time TIMESTAMP,	"\
        "	deal_no INTEGER,	"\
        "	entrust_no INTEGER,	"\
        "	entrust_status TEXT (1),	"\
        "	account_code TEXT (32),	"\
        "	asset_no TEXT (16),	"\
        "	combi_no TEXT (16),	"\
        "	market_no TEXT (3),	"\
        "	stock_code TEXT (16),	"\
        "	entrust_direction TEXT (4),	"\
        "	price_type TEXT (1),	"\
        "	deal_price DOUBLE,	"\
        "	deal_amount DOUBLE,	"\
        "	deal_balance DOUBLE,	"\
        "	deal_fee DOUBLE,	"\
        "	ext_access_system_id INTEGER	"\
        "	);	";
    bRet = m_pOutDbHelper->execSql(pTDealListCreateSql, strErrMsg);
    if (!bRet)
    {
        qCritical() << "Create table error: " << strErrMsg;
        return false;
    }

    const char* pTRunConfigCreateSql =
        "CREATE TABLE IF NOT EXISTS trunconfig (	"\
        "	item TEXT (64) PRIMARY KEY,	"\
        "	value TEXT (64)	"\
        "	);	";
    bRet = m_pOutDbHelper->execSql(pTRunConfigCreateSql, strErrMsg);
    if (!bRet)
    {
        qCritical() << "Create table error: " << strErrMsg;
        return false;
    }

    return true;
}

bool DbLoader::initInDb()
{
    m_pInDbHelper = new DbHelper;

    //打开当天的in.db，如果没有，则创建
    if (!m_pInDbHelper->open(Utils::getInDbName().toUtf8().data()))
    {
        qCritical() << "Open database failed: " << Utils::getInDbName();
        SAFE_DELETE(m_pInDbHelper);
        return false;
    }

    //建表
    if (!createInDbTable())
    {
        qCritical() << "Create table error, db = " << Utils::getInDbName();
        m_pInDbHelper->close();
        SAFE_DELETE(m_pInDbHelper);
        return false;
    }

    //载入待报委托和撤单表
    if (ConfigMgr::getData().bAutoMonitor && !loadInDb())
    {
        qCritical() << "Load in.db error, db = " << Utils::getInDbName();
        m_pInDbHelper->close();
        SAFE_DELETE(m_pInDbHelper);
        return false;
    }

    //关闭
    m_pInDbHelper->close();
    SAFE_DELETE(m_pInDbHelper);

    //备份历史库
    //if (!Utils::backupHisDb(CONSTANT::IN_DB_NAME))
    //{
    //    qWarning() << "Backup history in db failed";
    //}

    return true;
}

bool DbLoader::initOutDb()
{
    //打开当天的out.db，如果没有，则创建
    m_pOutDbHelper = new DbHelper;

    if (!m_pOutDbHelper->open(Utils::getOutDbName().toUtf8().data()))
    {
        qCritical() << "Open database failed: " << Utils::getOutDbName();
        SAFE_DELETE(m_pOutDbHelper);
        return false;
    }

    //建表
    if (!createOutDbTable())
    {
        qCritical() << "Create table error, db = " << Utils::getOutDbName();
        m_pOutDbHelper->close();
        SAFE_DELETE(m_pOutDbHelper);
        return false;
    }

    //载入当天的数据
    if (!loadOutDb())
    {
        qCritical() << "Load data failed, db = " << Utils::getOutDbName();

        m_pOutDbHelper->close();
        SAFE_DELETE(m_pOutDbHelper);
        return false;
    }

    //关闭
    m_pOutDbHelper->close();
    SAFE_DELETE(m_pOutDbHelper);

    //备份历史库
    //if (!Utils::backupHisDb(CONSTANT::OUT_DB_NAME))
    //{
    //    qWarning() << "Backup history out db failed";
    //}

    return true;
}

bool DbLoader::loadOutDb()
{
    //读入out db中当天所有的委托和成交信息
    //tentrustlist
    DbRecordSetPtr pRecordSet = new DbRecordSet();

    QString sql = QString("select * from %1 order by %4 asc")
        .arg(DBTABLE::TENTRUSTLIST::TABLE)
        .arg(DBTABLE::TENTRUSTLIST::COL_ID);
    int cnt = m_pOutDbHelper->getRecordBySql(sql.toUtf8().data(), pRecordSet);
    if (cnt >= 0)
    {
        qDebug() << "Load " << cnt << " entrust records in " << DBTABLE::TENTRUSTLIST::TABLE;
    }
    else
    {
        qCritical() << "Query table" << DBTABLE::TENTRUSTLIST::TABLE << "failed";
        DbHelper::freeResultSet(pRecordSet);
        return false;
    }
    for (unsigned i = 0; i < pRecordSet->size(); i++)
    {
        DbKVPtr pRecord = pRecordSet->at(i);
        Order *pOrder = new Order;

        pOrder->iInternalId = (*pRecord)[DBTABLE::TENTRUSTLIST::COL_ID].toInt();
        //TODO 要区分dtEntrustTime和dtBusinessTime
        pOrder->dtEntrustTime = QDateTime::fromString((*pRecord)[DBTABLE::TENTRUSTLIST::COL_BUSINESS_TIME], "yyyy-MM-dd hh:mm:ss");
        pOrder->dtBusinessTime = QDateTime::fromString((*pRecord)[DBTABLE::TENTRUSTLIST::COL_BUSINESS_TIME], "yyyy-MM-dd hh:mm:ss");
        //TODO 增加委托批号 pOrder->iBatchNo = (*pRecord)[DBTABLE::TENTRUSTLIST::COL_BATCH_NO].toInt();
        pOrder->iEntrustNo = (*pRecord)[DBTABLE::TENTRUSTLIST::COL_ENTRUST_NO].toInt();
        pOrder->strEntrustStatus = (*pRecord)[DBTABLE::TENTRUSTLIST::COL_ENTRUST_STATUS];
        pOrder->strAccountCode = (*pRecord)[DBTABLE::TENTRUSTLIST::COL_ACCOUNT_CODE];
        pOrder->strAssetNo = (*pRecord)[DBTABLE::TENTRUSTLIST::COL_ASSET_NO];
        pOrder->strCombiNo = (*pRecord)[DBTABLE::TENTRUSTLIST::COL_COMBI_NO];
        pOrder->strMarketNo = (*pRecord)[DBTABLE::TENTRUSTLIST::COL_MARKET_NO];
        pOrder->strStockCode = (*pRecord)[DBTABLE::TENTRUSTLIST::COL_STOCK_CODE];
        pOrder->strEntrustDirection = (*pRecord)[DBTABLE::TENTRUSTLIST::COL_ENTRUST_DIRECTION];
        pOrder->strPriceType = (*pRecord)[DBTABLE::TENTRUSTLIST::COL_PRICE_TYPE];
        pOrder->fEntrustPrice = (*pRecord)[DBTABLE::TENTRUSTLIST::COL_ENTRUST_PRICE].toFloat();
        pOrder->fEntrustAmount = (*pRecord)[DBTABLE::TENTRUSTLIST::COL_ENTRUST_AMOUNT].toFloat();
        pOrder->fCancelDealAmount = (*pRecord)[DBTABLE::TENTRUSTLIST::COL_CANCEL_DEAL_AMOUNT].toFloat();
        pOrder->strFailCause = (*pRecord)[DBTABLE::TENTRUSTLIST::COL_FAIL_CAUSE];
        pOrder->iExtAccessSystemId = (*pRecord)[DBTABLE::TENTRUSTLIST::COL_EXT_ACCESS_SYSTEM_ID].toInt();
        pOrder->iExtSystemId = (*pRecord)[DBTABLE::TENTRUSTLIST::COL_EXT_SYSTEM_ID].toInt();

        m_pContext->addOrder(pOrder);
        m_pContext->addOrderByEntrustNo(pOrder);
    }
    DbHelper::freeResultSet(pRecordSet);

    //tdeallist
    pRecordSet = new DbRecordSet();

    sql = QString("select * from %1")
        .arg(DBTABLE::TDEALLIST::TABLE);
    cnt = m_pOutDbHelper->getRecordBySql(sql.toUtf8().data(), pRecordSet);
    if (cnt >= 0)
    {
        qDebug() << "Load " << cnt << " deal records in " << DBTABLE::TDEALLIST::TABLE;
    }
    else
    {
        qCritical() << "Query table" << DBTABLE::TDEALLIST::TABLE << "failed";
        DbHelper::freeResultSet(pRecordSet);
        return false;
    }
    for (unsigned i = 0; i < pRecordSet->size(); i++)
    {
        DbKVPtr pRecord = pRecordSet->at(i);
        Deal *pDeal = new Deal;

        int iEntrustNo = (*pRecord)[DBTABLE::TDEALLIST::COL_ENTRUST_NO].toInt();
        pDeal->dtDealTime = QDateTime::fromString((*pRecord)[DBTABLE::TDEALLIST::COL_DEAL_TIME], "yyyy-MM-dd hh:mm:ss");
        pDeal->strDealNo = (*pRecord)[DBTABLE::TDEALLIST::COL_DEAL_NO];
        pDeal->fDealAmount = (*pRecord)[DBTABLE::TDEALLIST::COL_DEAL_AMOUNT].toFloat();
        pDeal->fDealPrice = (*pRecord)[DBTABLE::TDEALLIST::COL_DEAL_PRICE].toFloat();
        pDeal->fDealBalance = (*pRecord)[DBTABLE::TDEALLIST::COL_DEAL_BALANCE].toFloat();
        pDeal->fDealfee = (*pRecord)[DBTABLE::TDEALLIST::COL_DEAL_FEE].toFloat();
        pDeal->bDisplay = false;
        pDeal->bOutput = true;

        Order* pOrder = m_pContext->getOrderByEntrustNo(iEntrustNo);
        if (pOrder == nullptr)
        {
            qCritical() << "Can't find deal record by entrust_no, iEntrustNo = " << iEntrustNo;
        }
        else
        {
            pOrder->vecDealDetail.push_back(pDeal);
        }
    }
    DbHelper::freeResultSet(pRecordSet);

    //trunconfig
    pRecordSet = new DbRecordSet();
    cnt = m_pOutDbHelper->getRecord(DBTABLE::TRUNCONFIG::TABLE, pRecordSet);
    if (cnt < 0)
    {
        qCritical() << "Query table" << DBTABLE::TRUNCONFIG::TABLE << "failed";
        DbHelper::freeResultSet(pRecordSet);
        return false;
    }

    for (unsigned i = 0; i < pRecordSet->size(); i++)
    {
        DbKVPtr pRecord = pRecordSet->at(i);
        TRunConfig data;

        data.item = (*pRecord)[DBTABLE::TRUNCONFIG::COL_ITEM];
        data.value = (*pRecord)[DBTABLE::TRUNCONFIG::COL_VALUE];

        m_pContext->addRunConfig(data.item, data.value);
    }

    //test write
    DbKV values;
    values.insert(DBTABLE::TRUNCONFIG::COL_ITEM, RUN_CONFIG::LAST_PROGRAM_START);
    values.insert(DBTABLE::TRUNCONFIG::COL_VALUE, QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss"));

    QString strErrMsg;
    bool bRet = m_pOutDbHelper->insertOrReplaceRecord(DBTABLE::TRUNCONFIG::TABLE, &values, strErrMsg);
    if (!bRet)
    {
        qCritical() << "ReportThread::onNewEntrustHandled error: " << strErrMsg;
        DbHelper::freeResultSet(pRecordSet);
        return false;
    }

    DbHelper::freeResultSet(pRecordSet);

    return true;
}

bool DbLoader::loadInDb()
{
    int iMaxEntrustId = m_pContext->queryRunConfig(RUN_CONFIG::MAX_ENTRUST_ID).toInt();

    DbRecordSet entrustRecordSet;

    //查询最新的订单，篮子委托不超过500条
    //select * from tentrust where id > m_iMaxEntrustId
    QString sql = QString("select * from %1 where %2 > %3 order by %4 asc")
        .arg(DBTABLE::TENTRUST::TABLE)
        .arg(DBTABLE::TENTRUST::COL_ID)
        .arg(iMaxEntrustId)
        .arg(DBTABLE::TENTRUST::COL_ID);
    int cnt = m_pInDbHelper->getRecordBySql(sql.toUtf8().data(), &entrustRecordSet);
    if (cnt < 0)
    {
        qCritical() << "Query table error: " << DBTABLE::TENTRUST::TABLE;
        return false;
    }

    qDebug() << "Found " << cnt << " new orders";

    for (unsigned i = 0; i < entrustRecordSet.size(); i++)
    {
        DbKVPtr pRecord = entrustRecordSet.at(i);
        SAFE_NEW(Order, pOrder);

        pOrder->eEntrustType = EntrustType::Entrust;
        pOrder->iInternalId = (*pRecord)[DBTABLE::TENTRUST::COL_ID].toInt();
        pOrder->dtEntrustTime = QDateTime::fromString((*pRecord)[DBTABLE::TENTRUST::COL_ENTRUST_TIME], "yyyy-MM-dd hh:mm:ss");
        pOrder->iBatchNo = (*pRecord)[DBTABLE::TENTRUST::COL_BATCH_NO].toInt();
        pOrder->strAccountCode = (*pRecord)[DBTABLE::TENTRUST::COL_ACCOUNT_CODE];
        pOrder->strAssetNo = (*pRecord)[DBTABLE::TENTRUST::COL_ASSET_NO];
        pOrder->strCombiNo = (*pRecord)[DBTABLE::TENTRUST::COL_COMBI_NO];
        pOrder->strMarketNo = (*pRecord)[DBTABLE::TENTRUST::COL_MARKET_NO];
        pOrder->strStockCode = (*pRecord)[DBTABLE::TENTRUST::COL_STOCK_CODE];
        pOrder->strEntrustDirection = (*pRecord)[DBTABLE::TENTRUST::COL_ENTRUST_DIRECTION];
        pOrder->strPriceType = (*pRecord)[DBTABLE::TENTRUST::COL_PRICE_TYPE];
        pOrder->fEntrustPrice = (*pRecord)[DBTABLE::TENTRUST::COL_ENTRUST_PRICE].toFloat();
        pOrder->fEntrustAmount = (*pRecord)[DBTABLE::TENTRUST::COL_ENTRUST_AMOUNT].toFloat();
        pOrder->iExtAccessSystemId = (*pRecord)[DBTABLE::TENTRUST::COL_EXT_ACCESS_SYSTEM_ID].toInt();
        pOrder->strEntrustStatus = ENTRUST_STATUS::UNACK;

        m_pContext->addUntreatedOrder(pOrder);

        if (pOrder->iInternalId > iMaxEntrustId)
        {
            iMaxEntrustId = pOrder->iInternalId;
        }
    }
    m_pContext->addRunConfig(RUN_CONFIG::MAX_ENTRUST_ID, QString("%1").arg(iMaxEntrustId));

    for (unsigned i = 0; i < entrustRecordSet.size(); i++)
    {
        SAFE_DELETE(entrustRecordSet[i]);
    }
    entrustRecordSet.clear();

    //查询最新的撤单申请
    DbRecordSet withdrawRecordSet;
    int iMaxWithdrawId = m_pContext->queryRunConfig(RUN_CONFIG::MAX_WITHDRAW_ID).toInt();

    //select * from twithdraw where id > iMaxWithdrawId
    sql = QString("select * from %1 where %2 > %3  order by %4 asc")
        .arg(DBTABLE::TWITHDRAW::TABLE)
        .arg(DBTABLE::TWITHDRAW::COL_ID)
        .arg(iMaxWithdrawId)
        .arg(DBTABLE::TWITHDRAW::COL_ID);
    cnt = m_pInDbHelper->getRecordBySql(sql.toUtf8().data(), &withdrawRecordSet);
    if (cnt < 0)
    {
        qCritical() << "Query table error: " << DBTABLE::TWITHDRAW::TABLE;
        return false;
    }

    qDebug() << "Found " << cnt << " new withdraw orders";
    for (unsigned i = 0; i < withdrawRecordSet.size(); i++)
    {
        DbKVPtr pRecord = withdrawRecordSet.at(i);
        SAFE_NEW(Order, pOrder);

        pOrder->eEntrustType = EntrustType::Withdraw;
        pOrder->iInternalId = (*pRecord)[DBTABLE::TWITHDRAW::COL_ID].toInt();
        pOrder->dtWithdrawTime = QDateTime::fromString((*pRecord)[DBTABLE::TWITHDRAW::COL_ENTRUST_TIME], "yyyy-MM-dd hh:mm:ss");
        pOrder->iEntrustNo = (*pRecord)[DBTABLE::TWITHDRAW::COL_ENTRUST_NO].toInt();

        //根据此iEntrustNo到CounterThread中查找原始订单，然后再撤单
        Order* pOrignOrder = m_pContext->getOrderByEntrustNo(pOrder->iEntrustNo);
        if (nullptr == pOrignOrder)
        {
            qWarning() << "Can't find orign order, iEntrustNo =" << pOrder->iEntrustNo;
            SAFE_DELETE(pOrder);
            continue;
        }

        int iInternalId = pOrder->iInternalId;
        pOrignOrder->dtWithdrawTime = pOrder->dtWithdrawTime;

        pOrder->clone(*pOrignOrder);
        pOrder->iInternalId = iInternalId;
        pOrder->eEntrustType = EntrustType::Withdraw;
        pOrder->strEntrustStatus = ENTRUST_STATUS::UNACK;

        m_pContext->addUntreatedOrder(pOrder);

        if (pOrder->iInternalId > iMaxWithdrawId)
        {
            iMaxWithdrawId = pOrder->iInternalId;
        }
    }
    m_pContext->addRunConfig(RUN_CONFIG::MAX_WITHDRAW_ID, QString("%1").arg(iMaxWithdrawId));

    for (unsigned i = 0; i < withdrawRecordSet.size(); i++)
    {
        SAFE_DELETE(withdrawRecordSet[i]);
    }
    withdrawRecordSet.clear();

    return true;
}