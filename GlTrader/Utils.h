#pragma once

#include "Constants.h"

class IF2UnPacker;
class IF2Packer;
class IBizMessage;

struct TMemoryLog
{
    int iAddress;
    const char* pszFilename;
    int iLine;

    TMemoryLog(int a, const char* f, int l)
        : iAddress(a)
        , pszFilename(f)
        , iLine(l)
    {}
};

class Utils
{
public:
    static void initLog();
    static void setLogLevel(LogLevel level);
    static void outputMessage(QtMsgType type, const QMessageLogContext &context, const QString &msg);
    static void logOn();
    static void logOff();

    static QString getComputerName();
    static QString getHDSerialId();

    static QString toQString(const char* ptr);
    static QString toQString(const std::string& str);

    static QString toQString(const QByteArray& array, int iTabCnt = 0);

    static QString toQString(IF2UnPacker* pUnPacker);
    static QString toQString(IF2Packer* pPacker);
    static QString toQString(IBizMessage* pBizMessage);

    static IBizMessage* cloneIBizMessage(IBizMessage* pBizMessage);
    static void releaseIBizMessage(IBizMessage* pBizMessage);

    static QString getInDbName();
    static QString getOutDbName();
    //static bool backupHisDb(const QString& strDbPrefixName);

    //获取项目构建时间
    static QDateTime getBuildTime();

    static void turnOnLogMemory();
    static void regNewMemory(void* ptr, const char* pszFilename, int iLine, const char* pszTypename);
    static void regDeleteMemory(void* ptr);
    static QString AllocatedMemoryToString();

public:
    static bool m_bLogMemoryAllocate;

private:
    static int iLogOn;
    static QString strLogFileName;
    static LogLevel eLogLevel;
    static QMap<int, TMemoryLog*> m_memoryLogMap;
};

#define SAFE_NEW(type, p) \
    type* p; \
    do {\
        p = new type; \
        if(Utils::m_bLogMemoryAllocate)\
        {\
            Utils::regNewMemory(p, __FILE__, __LINE__, #type); \
        }\
    } while (0)

#define SAFE_NEW_2(type, p) \
    do {\
        p = new type; \
        if(Utils::m_bLogMemoryAllocate)\
        {\
            Utils::regNewMemory(p, __FILE__, __LINE__, #type); \
        }\
    } while (0)


#define SAFE_DELETE(p) \
    do{\
	    if (nullptr != p)\
	    {\
            if(Utils::m_bLogMemoryAllocate)\
            {\
                Utils::regDeleteMemory(p); \
            }\
            delete p;\
            p = nullptr;\
	    }\
    } while (0)

#define SAFE_DELETE_LIST(p) \
    do {\
        if (nullptr != p)\
        {\
            for (int i = 0; i < p->size(); i++)\
            {\
                if ((*p)[i] != 0)\
                {\
                    delete (*p)[i];\
                }\
            }\
            delete p;\
            p = nullptr;\
        }\
    } while (0)

#define SAFE_RELEASE(p) \
    do{\
	    if (nullptr != p)\
	    {\
            p->Release();\
            p = nullptr;\
	    }\
    } while (0)

#define SAFE_QUIT_THREAD(thread) \
    do{\
        if (thread)\
        {\
            thread->quit();\
            thread->wait();\
            delete thread;\
            thread = nullptr;\
        }\
    } while (0)
