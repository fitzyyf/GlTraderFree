#pragma once

#include "Constants.h"

namespace CONFIG_ITEM
{
    //connection
    const char* const GROUP_CONNECTION = "connection";

    const char* const USERNAME = "username";
    const char* const PASSWORD = "password";
    const char* const REMEMBER_PASSWORD = "remember_password";
    const char* const SERVERS = "servers";
    
    //heartbeat_time	客户端给服务端发送心跳的间隔时间，单位为秒，最小值为5秒，不配或者配 <= 0，表示不开启客户端心跳
    const char* const HEARTBEAT_TIME = "heartbeat_time";
    //if_error_log	是否记录错误日志的参数，1表示记录，0表示不记录，默认不记录。
    const char* const IF_ERROR_LOG = "if_error_log";
    //Logdir	记录日志的目录，默认在程序运行的当前目录，只有开启记录日志，才会记录
    const char* const LOGDIR = "Logdir";
    //if_sendRecv_log	记录发包和收包的日志，用于客户端查找问题，1表示记录，0表示不记录，默认不记录
    const char* const IF_SENDRECV_LOG = "if_sendRecv_log";
    //max_log_file_size	打开记录日志功能时，单个日志文件的最大大小。单位M，缺省值为50
    const char* const MAX_LOG_FILE_SIZE = "max_log_file_size";
    //max_log_file_count	打开记录日志功能时，最多允许产生的日志文件的个数。缺省值5
    const char* const MAX_LOG_FILE_COUNT = "max_log_file_count";

    //writedata	是否落地完整收发数据，用于排查接收发送数据是否丢失。1表示落地，0表示不落地。默认不落地。
    const char* const WRITEDATA = "writedata";
    //if_reqInfo_clipping	记录日志时根据配置项可以设置是否记录完整请求包，默认是记录完整包，1表示不记录完整包，0表示记录完整包。
    const char* const IF_REQINFO_CLIIPPING = "if_reqInfo_clipping";

    //auto_reconnect	指定是否自动重连，1:是，非1:否（缺省）。
    const char* const AUTO_RECONNECT = "auto_reconnect";
    //reconnect_timeout	指定异步自动重连模式下，异步请求建立连接的超时时间  单位：ms    默认：1000
    const char* const RECONNECT_TIMEOUT = "reconnect_timeout";


    //log
    const char* const GROUP_LOG = "log";

    const char* const LOG_LEVEL = "log_level";

    //interface
    const char* const GROUP_INTERFACE = "interface";

    //const char* const IN_DB = "in_db";
    //const char* const OUT_DB = "out_db";
    const char* const IN_DB_MONITOR_INTERVAL = "in_db_monitor_interval";

    //ui
    const char* const GROUP_UI = "ui";

    const char* const QUICK_ENTRUST = "quick_entrust";
    const char* const AUTO_MONITOR = "auto_monitor";

}

struct ConfigData
{
    //const char* const IN_DB = "in.db";
    //const char* const OUT_DB = "out.db";
    const char* const SERVER_ADDR = "";
    const int MONITOR_INTERVAL = 100;//ms

    QString strUsername;
    QByteArray arrPassword;
    bool bRememberPassword;
    QString strServers;

    int iHeartbeatTime;
    int iIfErrorLog;
    QString strLogDir;
    int iIfSendRecvLog;
    int iMaxLogFileSize;
    int iMaxLogFileCount;

    int iWriteData;
    int iIfReqInfoClipping;

    int iAutoReconnect;
    int iReconnectTimeout;//ms

    int iLogLevel;

    //QString strInDb;
    //QString strOutDb;
    int iInDbMonitorInterval;

    bool bQuickEntrust;
    bool bAutoMonitor;

    ConfigData();
    void clone(const ConfigData& o);
    ConfigData& operator =(const ConfigData& o);
};

class ConfigMgr
{
public:
    static ConfigMgr* getInstance();
    static ConfigMgr* newInstance(const QString& fileName);
    static void destoryInstance();

    void load();
    void save();

    static ConfigData& getData();

private:
    ConfigMgr(const QString& fileName);
    ~ConfigMgr();

    void setGroupName(const QString& groupName);

    void save(const QString& name, bool val);
    void save(const QString& name, qint16 val);
    void save(const QString& name, quint16 val);
    void save(const QString& name, qint32 val);
    void save(const QString& name, quint32 val);
    void save(const QString& name, qint64 val);
    void save(const QString& name, quint64 val);
    void save(const QString& name, const QString& val);
    void save(const QString& name, const QByteArray& val);

    void load(const QString& name, bool& val);
    void load(const QString& name, qint16& val);
    void load(const QString& name, quint16& val);
    void load(const QString& name, qint32& val);
    void load(const QString& name, quint32& val);
    void load(const QString& name, qint64& val);
    void load(const QString& name, quint64& val);
    void load(const QString& name, QString& val);
    void load(const QString& name, QByteArray& val);

private:
    static ConfigMgr* m_pInstance;

    QSettings* m_pSettings;
    QString m_groupName;

    ConfigData m_data;
};

