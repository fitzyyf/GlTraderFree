#ifndef XXTEA_H
#define XXTEA_H

#include <QtGlobal>
#include <QByteArray>

//typedef char int8 ;
//typedef unsigned char quint8 ;
//typedef short int16 ;
//typedef unsigned short quint16 ;
//typedef long int32 ;
//typedef unsigned long quint32 ;

typedef struct tagTEACTX
{
    quint8 buf[8] ;
    quint8 bufPre[8] ;
    const quint8 *pKey ; //ָ��16�ֽڵ�key
    quint8 *pCrypt ;
    quint8 *pCryptPre ;
} TEACTX, *LPTEACTX ;

void PrintBuffer(const quint8 *buf, quint32 ulLen);

quint32 Encrypt(TEACTX *pCtx, const quint8 *pPlain, quint32 ulPlainLen,
    const quint8 *pKey, quint8 *pOut, quint32 *pOutLen);

quint32 Decrypt(TEACTX *pCtx, const quint8 *pCipher, quint32 ulCipherLen,
    const quint8 *pKey, quint8 *pOut, quint32 *pOutLen);

class Xxtea
{
public:
    static QByteArray encrypt(QByteArray src, QByteArray key = QByteArray::fromHex("19 f0 1a 01 0b 2a 09 0c  1c 2c 0b 3a 09 0c 1c 0b"));

    static QByteArray decrypt(QByteArray src, QByteArray key = QByteArray::fromHex("19 f0 1a 01 0b 2a 09 0c  1c 2c 0b 3a 09 0c 1c 0b"));
};

#endif // XXTEA_H
