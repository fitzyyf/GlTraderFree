#include "stdafx.h"

#include "DlgAbout.h"
#include "Utils.h"
#include "Constants.h"
#include "IconHelper.h"

DlgAbout::DlgAbout(QWidget *parent)
    : QDialog(parent)
{
    ui.setupUi(this);

    //设置窗体标题栏隐藏
    this->setWindowFlags(Qt::FramelessWindowHint | Qt::WindowSystemMenuHint | Qt::WindowMinMaxButtonsHint);
    ui.lblAppVersion->setText(CONSTANT::APP_VERSION);
    ui.lblBuildTime->setText(Utils::getBuildTime().toString("yyyy-MM-dd HH:mm:ss").toUtf8().data());
}

void DlgAbout::mousePressEvent(QMouseEvent *e)
{
    //按任意键退出
    //if (e->button() == Qt::LeftButton) 
    {
        this->close();
    }
}
