#pragma once

// 将宏的值，转换为字符串（这两个宏，有可能被重定义，可以改为其它名称）
#define _S(x)    #x
#define __S(x)    _S(x)

// 使用数字表示的版本号
#define VERSION_1 0
#define VERSION_2 0
#define VERSION_3 0
#define VERSION_4 0

// 使用字符串表示的版本号
#define VERSION_S_1 __S(VERSION_1)
#define VERSION_S_2 __S(VERSION_2)
#define VERSION_S_3 __S(VERSION_3)
#define VERSION_S_4 __S(VERSION_4)

typedef enum 
{
    Debug = 5,
    Info = 4,
    Warn = 3,
    Critical = 2,
    Fatal = 1,
}LogLevel;


typedef enum 
{
    RunLog_Info,
    RunLog_Important,
    RunLog_Error,
}RunLogType;

namespace CONSTANT
{
    const char* const APP_NAME = "恒生PB文件下单助手";
    const char* const APP_VERSION = VERSION_S_1 "." VERSION_S_2 "." VERSION_S_3;
    const char* const VERSION_TYPE = "试用版";

    const char* const LOG_FILE_DIR= "./log/";
    const char* const LOG_FILE_NAME_PREFIX = "gltrader";
    const char* const LOG_FILE_NAME_POSTFIX = ".log";
    const int LOG_CNT = 30;

    const char* const DIR_DB = "db";

    const char* const CONFIG_DB = "gltrader.db";
    const char* const CONFIG_FILE_NAME = "config.ini";

    const char* const IN_DB_NAME = "in";
    const char* const OUT_DB_NAME = "out";
    const char* const DB_POSTFIX = ".db";

    const char* const LOGIN_NAME = "GlTrader";
    const char* const LICENSE_FILE = "license.dat";

    const char* const SAFE_LEVEL = "pwd";
    const char* const COMM_PWD = "1";

    const int SYSTEM_NO = 100;
    char* const BIZ_NAME = "GLTRADER";
    char* const TOPIC_NAME = "ufx_topic";
    const bool IS_REBUILD = true;
    const bool IS_REPLACE = false;
    const int SEND_INTERVAL = 0;//这个字段如果设置为1，则1秒内只发一个消息，其他消息全部丢弃!!!!!!
    char* const FILTER_OPERATOR_NO = "operator_no";

    const int HEARTBEAT_INTERVAL = 1 * 60 * 1000; //ms
    const int PACKER_VERSION = 2;

}

namespace RUN_CONFIG
{
    const char* const MAX_ENTRUST_ID = "max_entrust_id";
    const char* const MAX_WITHDRAW_ID = "max_withdraw_id";
    const char* const LAST_PROGRAM_START = "last_program_start";
}

namespace FUNC_NO
{
    const int HEARTBEAT = 10000;//心跳
    const int LOGIN = 10001;//登录
    const int LOGOUT = 10002;//登出

    const int FUND_QUERY = 30001;//账户查询
    const int ASSET_QUERY = 30002;//资产单元查询
    const int COMBI_QUERY = 30003;//组合查询

    const int STOCK_QUERY = 31001;//证券持仓查询
    const int COMBI_FUND_QUERY = 34001;//账户资金查询

    const int ENTRUST = 91001;//普通买卖委托
    const int ENTRUST_COMBI = 91090;//篮子委托
    //const int ENTRUST_WITHDRAW = 91101;//委托撤单-old
    const int ENTRUST_WITHDRAW = 91114;//委托撤单-new 此接口在91101基础上增加了账户编号和组合编号传入，用于权限校验，请使用该接口，效率更高。
}

namespace PUSH_MSG_TYPE
{
    char* const ENTRUST = "a";               //委托下达
    char* const ENTRUST_CONFIRM = "b";       //委托确认
    char* const ENTRUST_FAIL = "c";          //委托废单
    char* const ENTRUST_WITHDRAW = "d";      //委托撤单
    char* const ENTRUST_WITHDRAW_DEAL = "e"; //委托撤成
    char* const ENTRUST_WITHDRAW_FAIL = "f"; //委托撤废
    char* const ENTRUST_DEAL = "g";          //委托成交
}

typedef enum
{
    Syn = 0,//同步
    Asy = 1,//异步
}CommMode;

typedef enum
{
    Entrust = 0, //委托
    Withdraw = 1,//撤单
}EntrustType;

typedef enum
{
    Success = 1, //成功
    Fail = 2,    //失败
}CancelSuccessFlag;

namespace ERROR_CODE
{
    const int SUCCESS = 0;
    const int FAIL = -1;
}

namespace PB_ERROR_CODE
{
    const int SUCCESS = 0;
    const int NOT_INIT = -100;//未初始化

    const int INVALID_USER_TOKEN = 1000;//用户未登录或token失效
}

namespace PARAMETER
{
    const char* const ERROR_INFO = "error_info";

    //待处理委托
    const char* const ORDER_IN = "order_in";
    //正在处理的委托
    const char* const ORDER_OUT = "order_out";

    //待处理篮子委托
    const char* const ORDER_LIST_IN = "order_list_in";
    //正在处理的篮子委托
    const char* const ORDER_LIST_OUT = "order_list_out";
}

namespace TBL_ENTRUST_COL
{
    const QString CHECKBOX = QStringLiteral("");//复选框
    const QString NO = QStringLiteral("序号");//主键
    const QString ACCOUNT_CODE = QStringLiteral("产品代码");
    const QString ACCOUNT_NAME = QStringLiteral("产品名称");
    const QString COMBI_NO = QStringLiteral("单元编号");
    const QString COMBI_NAME = QStringLiteral("单元名称");
    const QString MARKET_NO = QStringLiteral("交易市场");
    const QString STOCK_CODE = QStringLiteral("证券代码");
    const QString STOCK_NAME = QStringLiteral("证券名称");//TODO 如何获取证券名称
    const QString ENTRUST_DIRECTION = QStringLiteral("委托方向");
    const QString PRICE_TYPE = QStringLiteral("价格类型");
    const QString ENTRUST_PRICE = QStringLiteral("委托价格");
    const QString ENTRUST_AMOUNT = QStringLiteral("委托数量");
    const QString ENTRUST_BALANCE = QStringLiteral("委托金额");
    const QString FALG = QStringLiteral("标识");//委托/撤单
    const QString EXT_ACCESS_SYSTEM_ID = QStringLiteral("外部序号");
    const QString ORIGN_ENTRUST_NO = QStringLiteral("原委托序号");//如果是撤单，则填入entrust_no
}

namespace TBL_ENTRUSTLIST_COL
{
    const QString NO = QStringLiteral("序号");
    const QString BUSINESS_DATE = QStringLiteral("委托日期");
    const QString BUSINESS_TIME = QStringLiteral("委托时间");
    const QString ENTRUST_NO = QStringLiteral("委托序号");
    const QString ENTRUST_STATUS = QStringLiteral("委托状态");
    const QString ACCOUNT_CODE = QStringLiteral("产品代码");
    const QString ACCOUNT_NAME = QStringLiteral("产品名称");
    const QString COMBI_NO = QStringLiteral("单元编号");
    const QString COMBI_NAME = QStringLiteral("单元名称");
    const QString MARKET_NO = QStringLiteral("交易市场");
    const QString STOCK_CODE = QStringLiteral("证券代码");
    const QString STOCK_NAME = QStringLiteral("证券名称");
    const QString ENTRUST_DIRECTION = QStringLiteral("委托方向");
    const QString PRICE_TYPE = QStringLiteral("价格类型");
    const QString ENTRUST_PRICE = QStringLiteral("委托价格");
    const QString ENTRUST_AMOUNT = QStringLiteral("委托数量");
    const QString ENTRUST_BALANCE = QStringLiteral("委托金额");
    const QString CANCEL_DEAL_AMOUNT = QStringLiteral("撤成数量");
    const QString FAIL_CAUSE = QStringLiteral("废单原因");
    const QString EXT_ACCESS_SYSTEM_ID = QStringLiteral("外部序号");
    const QString SEND_HANDLE = QStringLiteral("发送序号");
}

namespace TBL_DEALLIST_COL
{
    const QString DEAL_DATE = QStringLiteral("成交日期");
    const QString DEAL_TIME = QStringLiteral("成交时间");
    const QString DEAL_NO = QStringLiteral("成交编号");
    const QString ENTRUST_NO = QStringLiteral("委托序号");
    const QString ACCOUNT_CODE = QStringLiteral("产品代码");
    const QString ACCOUNT_NAME = QStringLiteral("产品名称");
    const QString COMBI_NO = QStringLiteral("单元编号");
    const QString COMBI_NAME = QStringLiteral("单元名称");
    const QString MARKET_NO = QStringLiteral("交易市场");
    const QString STOCK_CODE = QStringLiteral("证券代码");
    const QString STOCK_NAME = QStringLiteral("证券名称");
    const QString ENTRUST_DIRECTION = QStringLiteral("委托方向");
    const QString DEAL_AMOUNT = QStringLiteral("成交数量");
    const QString DEAL_PRICE = QStringLiteral("成交价格");
    const QString DEAL_BALANCE = QStringLiteral("成交金额");
    const QString DEAL_FEE = QStringLiteral("总费用");
    const QString EXT_ACCESS_SYSTEM_ID = QStringLiteral("外部序号");
}

//1	account_type	账户类型
//5	invest_type	投资类型
//8	position_flag	多空标志
//9	instance_type	交易实例类型
//10	currency_code	币种
//11	account_category	资产账户类型
//12	origin_type	账户来源
//13	replace_flag	现金替代标志
//14	ambusiness_type_limits	业务类别
//15	risk_type	风控类别

//17	entrust_fail_code	委托失败代码
//18	option_type	期权类型
//19	purchase_way	申赎方式
//20	adjust_mode	资金调整类型
//21	stock_type_limits	证券类别权限
//22	business_op_flag	业务操作类型
//23	future_kind_id	期货品种序号
//24	bind_status	股东指定状态
namespace DICT_ID
{
    const int ENTRUST_STATUS = 3;     //3	entrust_state	委托状态
    const int MARKET_NO = 4;          //4	market_no	交易市场
    const int ENTRUST_DIRECTION = 6;  //6	entrust_direction	委托方向
    const int PRICE_TYPE = 7;         //7	price_type	价格类型
    const int RISK_OPERATION = 16;    //16	risk_operation	风控触警操作

    //自定义
    const int USER_DEFINE = 100;
    const int ACCOUNT_CODE = USER_DEFINE + 1;
    const int ASSET_NO     = USER_DEFINE + 2;
    const int COMBI_NO     = USER_DEFINE + 3;
}

namespace MARKET_NO
{
    const char* const SH = "1";//上交所
    const char* const SZ = "2";//深交所
    const char* const SHFE = "3";//上期所
    const char* const CZCE = "4";//郑商所
    const char* const CFFEX = "7";//中金所
    const char* const DCE = "9";//大商所
    const char* const INE = "K";//能源期货交易所
    const char* const NEEQ = "10";//股转市场
    const char* const HK_SH = "35";//港股通（沪）
    const char* const HK_SZ = "o";//港股通（深）
}

//entrust_direction	委托方向
//'1'	买入
//'2'	卖出
//'3'	债券买入
//'4'	债券卖出
//'5'	融资(正)回购
//'6'	融券(逆)回购
//'9'	配股（配债）认购
//'10'	债转股
//'11'	债回售
//'12'	申购
//'13'	基金认购
//'17'	转托管
//'26'	ETF申购
//'27'	ETF赎回
//'28'	行权认购
//'29'	行权认沽
//'30'	提交质押
//'31'	转回质押
//'32'	沪深交易所融资融券展期
//'50'	基金分拆
//'51'	基金合并
//'53'	开基申购
//'54'	开基赎回
//'55'	债券认购
//'63'	保证券锁定（股票期权）, 担保品交存（融资融券）
//'64'	保证券解锁（股票期权）, 担保品提取（融资融券）
//'67'	融券卖出
//'68'	买券还券
//'69'	直接还款
//'70'	直接还券
//'75'	融资买入
//'76'	卖券还款
//'83'	放弃行权（郑商所、大商所商品期权）
//'84'	询价（上交所期权询价）
namespace ENTRUST_DIRECTION
{
    const char* const BUY = "1";
    const char* const SELL = "2";
}

//price_type	价格类型
//'0'	限价
//'1'	任意价(期货)
//'2'	市价剩余转限价（上交所股票期权）
//'3'	市价剩余撤消（上交所股票期权）
//'4'	FOK限价（上交所股票期权）
//'5'	FOK市价（上交所股票期权）
//'a'	五档即成剩撤（上交所市价）
//'b'	五档即成剩转（上交所市价）
//'c'	限价盘（零股）(港股通)
//'e'	竞价限价盘(港股通)
//'g'	增强限价盘(港股通)
//'i'	竞价限价盘FOK(港股通)
//'j'	增强限价盘FOK(港股通)
//'k'	定价申报(股转市场)
//'l'	协议转让成交确认申报(股转市场)
//'m'	协议转让互报成交确认申报(股转市场)
//'A'	五档即成剩撤（深交所市价）
//'C'	即成剩撤（深交所市价）
//'D'	对手方最优（深交所市价）
//'E'	本方最优（深交所市价）
//'F'	全额成或撤（FOK市价）（深交所市价）
//'G'	全额成或撤(FOK限价)（上期所、中金所、深交所、能源交易所）
//'K'	即成剩撤（FAK）（上期所、郑商所、中金所、能源交易所）
//'X'	任意价转限价（中金所）
//'L'	五档即成剩撤（中金所五档市价）
//'M'	五档即成剩转（中金所五档市价转限价）
//'N'	最优一档即成剩撤（中金所最优价）
//'O'	最优一档即成剩转（中金所最优价）
namespace PRICE_TYPE
{
    const char* const XIAN_JIA = "0";
}

//3	entrust_state	委托状态
//'0'	未响应
//'1'	未报
//'2'	待报
//'3'	正报
//'4'	已报
//'5'	废单
//'6'	部成
//'7'	已成
//'8'	部撤
//'9'	已撤
//'a'	待撤
//'b'	未审批
//'c'	审批拒绝
//'d'	未审批即撤销
namespace ENTRUST_STATUS
{
    const char* const UNACK = "0";
    const char* const UNREPORT = "1";
    const char* const TO_REPORT = "2";
    const char* const REPORTING = "3";
    const char* const REPORTED = "4";
    const char* const FAIL = "5";
    const char* const DEAL_PART = "6";
    const char* const DEAL = "7";
    const char* const WITHDRAW_PART = "8";
    const char* const WITHDRAW = "9";
    const char* const TO_WITHDRAW = "a";

    const char* const UNAPPROVE = "b";
    const char* const APPROVE_REJECT = "c";
    const char* const UNAPPROVE_CANCEL = "d";
}
