#pragma once

namespace DBTABLE
{
    //************************************************************
    //
    //  基础配置数据库
    //
    //************************************************************
    //数据字典表
    namespace TDICT
    {
        //表名
        const char* const TABLE = "TDICT";
        //字段
        const char* const COL_DICT_ID = "DICT_ID";
        const char* const COL_ITEM = "ITEM";
        const char* const COL_REMARK = "REMARK";
    }

    //************************************************************
    //
    //  用户输入 数据库
    //
    //************************************************************
    //委托表
    namespace TENTRUST
    {
        //表名
        const char* const TABLE = "TENTRUST";
        //字段
        const char* const COL_ID = "ID";//自增序列，无需关注
        const char* const COL_ENTRUST_TIME = "ENTRUST_TIME";//数据库自动生成
        const char* const COL_BATCH_NO = "BATCH_NO";
        const char* const COL_ACCOUNT_CODE = "ACCOUNT_CODE";
        const char* const COL_ASSET_NO = "ASSET_NO";
        const char* const COL_COMBI_NO = "COMBI_NO";
        const char* const COL_MARKET_NO = "MARKET_NO";
        const char* const COL_STOCK_CODE = "STOCK_CODE";
        const char* const COL_ENTRUST_DIRECTION = "ENTRUST_DIRECTION";
        const char* const COL_PRICE_TYPE = "PRICE_TYPE";
        const char* const COL_ENTRUST_PRICE = "ENTRUST_PRICE";
        const char* const COL_ENTRUST_AMOUNT = "ENTRUST_AMOUNT";
        const char* const COL_EXT_ACCESS_SYSTEM_ID = "EXT_ACCESS_SYSTEM_ID";//外部接入系统的委托序号
    }

    //撤单表
    namespace TWITHDRAW
    {
        //表名
        const char* const TABLE = "TWITHDRAW";
        //字段
        const char* const COL_ID = "ID";//自增序列，无需关注
        const char* const COL_ENTRUST_TIME = "ENTRUST_TIME";//数据库自动生成
        const char* const COL_ENTRUST_NO = "ENTRUST_NO";
    }

    //************************************************************
    //
    //  数据输出 数据库
    //
    //************************************************************
    //委托查询表
    namespace TENTRUSTLIST
    {
        //表名
        const char* const TABLE = "TENTRUSTLIST";
        //字段
        const char* const COL_ID = "ID";
        const char* const COL_BUSINESS_TIME = "BUSINESS_TIME";//委托时间或申报时间
        const char* const COL_ENTRUST_NO = "ENTRUST_NO";
        const char* const COL_ENTRUST_STATUS = "ENTRUST_STATUS";
        const char* const COL_ACCOUNT_CODE = "ACCOUNT_CODE";
        const char* const COL_ASSET_NO = "ASSET_NO";
        const char* const COL_COMBI_NO = "COMBI_NO";
        const char* const COL_STOCKHOLDER_ID = "STOCKHOLDER_ID";
        const char* const COL_REPORT_SEAT = "REPORT_SEAT";
        const char* const COL_MARKET_NO = "MARKET_NO";
        const char* const COL_STOCK_CODE = "STOCK_CODE";
        const char* const COL_ENTRUST_DIRECTION = "ENTRUST_DIRECTION";
        const char* const COL_PRICE_TYPE = "PRICE_TYPE";
        const char* const COL_ENTRUST_PRICE = "ENTRUST_PRICE";
        const char* const COL_ENTRUST_AMOUNT = "ENTRUST_AMOUNT";
        const char* const COL_CANCEL_DEAL_AMOUNT = "CANCEL_DEAL_AMOUNT";//撤成数量
        const char* const COL_FAIL_CAUSE = "FAIL_CAUSE"; //废单原因
        const char* const COL_EXT_ACCESS_SYSTEM_ID = "EXT_ACCESS_SYSTEM_ID";
        const char* const COL_EXT_SYSTEM_ID = "EXT_SYSTEM_ID";
    }

    //成交查询表
    namespace TDEALLIST
    {
        //表名
        const char* const TABLE = "TDEALLIST";
        //字段
        const char* const COL_DEAL_TIME = "DEAL_TIME";
        const char* const COL_DEAL_NO = "DEAL_NO";
        const char* const COL_ENTRUST_NO = "ENTRUST_NO";
        const char* const COL_ENTRUST_STATUS = "ENTRUST_STATUS";
        const char* const COL_ACCOUNT_CODE = "ACCOUNT_CODE";
        const char* const COL_ASSET_NO = "ASSET_NO";
        const char* const COL_COMBI_NO = "COMBI_NO";
        const char* const COL_STOCKHOLDER_ID = "STOCKHOLDER_ID";
        const char* const COL_REPORT_SEAT = "REPORT_SEAT";
        const char* const COL_MARKET_NO = "MARKET_NO";
        const char* const COL_STOCK_CODE = "STOCK_CODE";
        const char* const COL_ENTRUST_DIRECTION = "ENTRUST_DIRECTION";
        const char* const COL_PRICE_TYPE = "PRICE_TYPE";
        const char* const COL_DEAL_AMOUNT = "DEAL_AMOUNT";
        const char* const COL_DEAL_PRICE = "DEAL_PRICE";
        const char* const COL_DEAL_BALANCE = "DEAL_BALANCE";
        const char* const COL_DEAL_FEE = "DEAL_FEE";
        const char* const COL_EXT_ACCESS_SYSTEM_ID = "EXT_ACCESS_SYSTEM_ID";
    }

    //运行配置表（内部表）
    namespace TRUNCONFIG
    {
        //表名
        const char* const TABLE = "TRUNCONFIG";
        //字段
        const char* const COL_ITEM = "ITEM";
        const char* const COL_VALUE = "VALUE";
    }
}
