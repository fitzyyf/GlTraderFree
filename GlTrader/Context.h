#pragma once

#include <QMap>
#include <QVector>

#include "OrderMgr.h"

class Context
{
public:
    Context();
    virtual ~Context();

    void reset();

    QString getUsername() const;
    void setUsername(const QString& strUsername);

    QString getPassword() const;
    void setPassword(const QString& strPassword);

    QString getUserToken() const;
    void setUserToken(const QString& strUserToken);

    QString getIp() const;
    void setIp(const QString& strIp);

    QString getMac() const;
    void setMac(const QString& strMac);

    QString getHDSerialId() const;
    void setHdSerialNo(const QString& strHdSerialNo);

    QString getT2Password() const;
    void setT2Password(const QString& strT2Password);

    QString getAccountCode() const;
    void setAccountCode(const QString& strAccountCode);

    QString getCombNo() const;
    void setCombNo(const QString& strCombNo);

    int getNewPacketId();

    //所有委托
    inline OrderList& getOrderList()    { return m_orderList; }
    Order* getOrder(int iExtSystemId);
    void addOrder(Order* pOrder);

    Order* getOrderByEntrustNo(int iEntrustNo);
    void addOrderByEntrustNo(Order* pOrder);

    //服务端未响应委托
    OrderList* getUnackOrderList(int hSendHandle);
    void addUnackOrder(Order* pOrder);
    void setOrderAcked(int hSendHandle);

    //待处理委托
    OrderList& getUntreatedOrderList();
    void addUntreatedOrder(Order* pOrder);
    void clearUntreatedOrders();

    QString queryDict(int iDictId, const QString& strItem);
    QMap<QString, QString>* queryDict(int iDictId);
    void addDict(int iDictId, const QString& strItem, const QString& strRemark);

    QString queryRunConfig(const QString& strItem);
    void addRunConfig(const QString& strItem, const QString& strValue);

private:
    QString m_strUsername;
    QString m_strPassword;
    QString m_strUserToken;

    QString m_strIp;
    QString m_strMac;
    QString m_strHDSerialId;

    QString m_strT2Password;

    QString m_strAccountCode;//产品编号
    QString m_strCombNo;//组合编号
    int     m_iPacketId;

    QMap<int, Order*> m_orderMap;//所有订单，map key: iExtSystemId
    QMap<int, Order*> m_orderMapEntrustNo;//所有订单，map key: iEntrustNo
    QVector<Order*> m_orderList;//所有订单， 有序

    QVector<Order*> m_untreatedOrderList;//未处理订单（在待处理委托列表中，尚未发送给柜台，仅在程序启动的时候从in db载入时使用）
    //如果前端检查错误,PB返回的错误信息中不包含extsystemid，所以只能依靠hSend来定位到哪条委托有问题
    QMap<int, OrderList*> m_unackOrderMap;//未被柜台处理的订单(已发送给柜台，但未收到应答)，key: hSend。

    QMap<int, QMap<QString, QString>*> m_dictMap;
    QMap<QString, QString> m_runConfigMap;
};

