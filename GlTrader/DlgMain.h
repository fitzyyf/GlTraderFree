#pragma once

#include <QDialog>
#include <QtMultimedia/QMediaPlayer>

#include "ui_Main.h"
#include "OrderMgr.h"

class Context;
class CounterThread;
class EntrustThread;
class ReportThread;

class DlgMain : public QDialog
{
    Q_OBJECT

public:
    DlgMain(CounterThread* pCounterThread, ReportThread* pReportThread, Context* pContext);
    virtual ~DlgMain();

    virtual void closeEvent(QCloseEvent *event) override;

protected:
    bool eventFilter(QObject *obj, QEvent *event);
    void mouseMoveEvent(QMouseEvent *e);
    void mousePressEvent(QMouseEvent *e);
    void mouseReleaseEvent(QMouseEvent *);

Q_SIGNALS:
    void sigLogout();

    void sigEntrustReq(OrderList*);
    void sigWithdrawReq(OrderList* pOrderList);

    void sigNewEntrustHandled(int iOrderInternalId);
    void sigNewWithdrawHandled(int iOrderInternalId);

public slots:
    void onBtnWinMaxClicked();
    void onBtnWinMinClicked();

    void onActionSetting();
    void onActionAbout();

    void onClickButtonEntrust();
    //void onSelectAllUntreated();

    void onBtnRemoveAllUntreatedClicked();
    void onBtnSelectReverseClicked();
    void onBtnSelectAllClicked();

    void onNewEntrust(OrderList* pOrderList);
    void onEntrustResult(Order* pOrder);

    void onNewWithdraw(OrderList* pOrderList);
    //void onWithdrawResult(Order* pOrder);

    void onLogoutResult(int iRetCode, QString strErrMsg);

    void onClickCheckStartMonitor(int checkState);
    void onClickCheckQuickEntrust(int checkState);
    void onClickCheckSelectEntrustList();

    void onRunLog(int type, QString message);

private:
    void initUi();
    void initSignalSlot();
    void initThread();
    void startThread();

    //void startMonitor();
    //void stopMonitor();

    void updateEntrustList(const Order* pOrder);
    void updateDealList(const Order* pOrder);

    bool isEntrustHidden(int iRow, bool bUndeal, bool bDeal, bool bWithdraw, bool bFail);

private:
    Ui::DlgMain ui;
    QMenu m_menu;

    QPoint m_ptMousePoint;
    bool m_bMousePressed;
    bool m_bWinMax;
    QRect m_recWinLocation;

    QAction* m_pActSelectAllUntreated;
    //bool m_bSelectAllUntreated;

    Context* m_pContext;
    CounterThread* m_pCounterThread;
    EntrustThread* m_pEntrustThread;
    ReportThread* m_pReportThread;
    QMap<int, Order*> m_untreatedOrderMap;

    QMap<QString, int> m_tblEntrustColMap;
    QMap<QString, int> m_tblEntrustListColMap;
    QMap<QString, int> m_tblDealListColMap;

    bool m_bLogout;

    QMediaPlayer* m_pMediaPlayerError;
    QMediaPlayer* m_pMediaPlayerImportant;
};
