#include "stdafx.h"

#include <iostream>
#include <windows.h>

#include "hundsun/t2sdk_interface.h"
#include "Utils.h"
#include "Constants.h"
#include "DbLoader.h"

#define POINTER_TO_INT(p) ((int)  (long) (p))

int Utils::iLogOn = 1;
QString Utils::strLogFileName;
LogLevel Utils::eLogLevel = LogLevel::Debug;
bool Utils::m_bLogMemoryAllocate = false;
QMap<int, TMemoryLog*> Utils::m_memoryLogMap;

void Utils::initLog()
{
    qInstallMessageHandler(Utils::outputMessage);

    //创建log目录
    QDir dirLog;
    if (!dirLog.exists(CONSTANT::LOG_FILE_DIR))
    {
        dirLog.mkdir(CONSTANT::LOG_FILE_DIR);
    }

    //log文件名
    strLogFileName = CONSTANT::LOG_FILE_DIR;
    strLogFileName.append(CONSTANT::LOG_FILE_NAME_PREFIX);
    strLogFileName.append(QDateTime::currentDateTime().toString("yyyyMMdd"));
    strLogFileName.append(CONSTANT::LOG_FILE_NAME_POSTFIX);

    //只保留最近的n个日志文件
    QDir dir;
    QStringList filters;
    filters << QString("%1????????%2").arg(CONSTANT::LOG_FILE_NAME_PREFIX).arg(CONSTANT::LOG_FILE_NAME_POSTFIX);
    dir.setNameFilters(filters);
    dir.setPath(CONSTANT::LOG_FILE_DIR);

    QDirIterator iter(dir);
    QStringList fileList;
    while (iter.hasNext())
    {
        iter.next();
        QFileInfo info = iter.fileInfo();
        fileList << (info.absoluteFilePath());
    }
    
    int iDelCnt = fileList.size() - CONSTANT::LOG_CNT;
    if (iDelCnt > 0)
    {
        fileList.sort();
        for(int i = 0; i < iDelCnt; i++)
        {
            qDebug() << "Delete log file:" << fileList[i];
            QFile file(fileList[i]);
            file.remove();
        }
    }
}

void Utils::setLogLevel(LogLevel level)
{
    eLogLevel = level;
}

void Utils::outputMessage(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    if (iLogOn < 1 && QtDebugMsg == type)
    {
        return;
    }

    if ((eLogLevel < LogLevel::Debug && type == QtDebugMsg)
        || (eLogLevel < LogLevel::Info && (type == QtDebugMsg || type == QtInfoMsg))
        || (eLogLevel < LogLevel::Warn && (type == QtDebugMsg || type == QtInfoMsg || type == QtWarningMsg))
        || (eLogLevel < LogLevel::Critical && (type == QtDebugMsg || type == QtInfoMsg || type == QtWarningMsg || type == QtCriticalMsg))
        )
    {
        return;
    }

    static QMutex mutex;
    mutex.lock();

    //[2018-09-12 21:09:10.123][debug][file:line]>

    QString text;
    switch (type)
    {
    case QtDebugMsg:
        text = QString("DEBUG");
        break;

    case QtInfoMsg:
        text = QString("INFO");
        break;

    case QtWarningMsg:
        text = QString("WARN");
        break;

    case QtCriticalMsg:
        text = QString("ERROR");
        break;

    case QtFatalMsg:
        text = QString("FATAL");
    }

    QString filepath = QString(context.file);
    QString filename = filepath;
    int idx = filepath.lastIndexOf('\\');
    if (idx < 0)
    {
        idx = filepath.lastIndexOf('/');
    }
    if (idx >= 0)
    {
        filename = filepath.right(filepath.length() - idx - 1);
    }

    QString message = QString("[%1][%2][%5][%3:%4]>>%6")
        .arg(QDateTime::currentDateTime().toString("MM-dd hh:mm:ss.zzz"))
        .arg(text)
        .arg(filename)
        .arg(context.line)
        .arg(QString::number(quintptr(QThread::currentThreadId())))
        .arg(msg);

    std::cerr << message.toStdString().data() << std::endl;

    QFile file(strLogFileName);
    file.open(QIODevice::WriteOnly | QIODevice::Append);
    QTextStream text_stream(&file);
    text_stream << message << "\r\n";
    file.flush();
    file.close();

    mutex.unlock();
}

void Utils::logOn()
{
    ++iLogOn;

    if (iLogOn > 0)
    {
        iLogOn = 1;
    }
}

void Utils::logOff()
{
    --iLogOn;
}

QString Utils::getComputerName()
{
    const int MAX_BUFFER_LEN = 500;
    char  szBuffer[MAX_BUFFER_LEN];
    DWORD dwNameLen;

    dwNameLen = MAX_BUFFER_LEN;
    if (GetComputerNameA(szBuffer, &dwNameLen))
    {
        return Utils::toQString(szBuffer);
    }
    else
    {
        return "";
    }
}

QString Utils::getHDSerialId()
{
    QProcess p;
    //WMIC DISKDRIVE where DeviceID="\\\\.\\PHYSICALDRIVE0" get  SerialNumber
    p.start("WMIC", QStringList() << "DISKDRIVE" << /*"where" << "DeviceID=\"\\\\.\\PHYSICALDRIVE0\"" <<*/ "get" << "SerialNumber,DeviceID");
    p.waitForStarted();
    p.waitForFinished();
    QString strTemp = QString::fromLocal8Bit(p.readAllStandardOutput());
    qDebug() << "WMIC DISKDRIVE get SerialNumber,DeviceID\r\n" << strTemp.toUtf8().data();

    QStringList lines = strTemp.split("\r\r\n");
    for (auto line : lines)
    {
        if (line.contains("PHYSICALDRIVE0"))
        {
            QStringList items = line.split(" ");
            for (auto item : items)
            {
                if (!item.contains("PHYSICALDRIVE0") && item.length() > 0)
                {
                    qDebug() << "Harddisk serial id: " << item;
                    return item;
                }
            }
        }
    }

    return "";
}

QString Utils::toQString(const char* ptr)
{
    return QString::fromLocal8Bit(ptr);
}

QString Utils::toQString(const std::string& str)
{
    return QString::fromLocal8Bit(str.c_str());
}

QString Utils::toQString(const QByteArray& array, int iTabCnt)
{
    QString tab;
    for (int i = 0; i < iTabCnt; i++)
    {
        tab.append("\t");
    }

    QString str;

    //str.append("\n        0  1  2  3  4  5  6  7   8  9  a  b  c  d  e  f");

    //0000    0A 3B D3  ........... A7    abce2cdsdf
    //0010    0A 3B D3  ........... A7    abce2cdsdf
    for (int i = 0; i < array.length(); i++)
    {
        //000
        if (i % 16 == 0)
        {
            str += QString("\n") + tab + QString("%1   ").arg(i, 4, 16, QLatin1Char('0')).toLower();
        }

        //0A 3B D3  ........... A7
        if ((i + 8 + 1) % 16 == 0)
        {
            str += QString("%1  ").arg((quint8)array[i], 2, 16, QLatin1Char('0')).toLower();
        }
        else
        {
            str += QString("%1 ").arg((quint8)array[i], 2, 16, QLatin1Char('0')).toLower();
        }

        //abce2cdsdf
        if ((i + 1) % 16 == 0 || (i + 1) == array.length())
        {
            //需要填充的空格数
            int padding = 16 - i % 16 - 1;
            for (int j = 0; j < padding; j++)
            {
                str += QString("   ");
            }
            if (padding > 8)
            {
                str.append(" ");
            }
            str += QString("    ");

            for (int m = i - i % 16; m <= i; m++)
            {
                char c = array[m];
                if (c >= 32 && c <= 126) //从32到126共95个可见字符. 
                {
                    str += QString(c);
                }
                else
                {
                    str += QString('.');
                }
            }

        }
    }

    return str;
}

QString Utils::toQString(IF2Packer* pPacker)
{
    return Utils::toQString(pPacker->UnPack());
}

QString Utils::toQString(IF2UnPacker* unPacker)
{
    QString buf;

    int i = 0, t = 0, j = 0, k = 0;

    buf += QString("DatasetCount = %1").arg(unPacker->GetDatasetCount());
    for (i = 0; i < unPacker->GetDatasetCount(); ++i)
    {
        // 设置当前结果集
        unPacker->SetCurrentDatasetByIndex(i);

        // 打印字段
        buf += "\r\n\r\n";
        for (t = 0; t < unPacker->GetColCount(); ++t)
        {
            buf += QString("\"%1\",").arg(unPacker->GetColName(t));
            //printf("%20s", unPacker->GetColName(t));
        }

        // 打印所有记录
        for (j = 0; j < (int)unPacker->GetRowCount(); ++j)
        {
            unPacker->Go(j + 1); //取值范围[1, GetRowCount()]，fuck！！！

            // 打印每条记录
            buf += "\r\n";
            for (k = 0; k < unPacker->GetColCount(); ++k)
            {
                char type = unPacker->GetColType(k);
                switch (type)
                {
                case 'I':
                    buf += QString("\"%1\",").arg(unPacker->GetIntByIndex(k));
                    break;

                case 'C':
                    buf += QString("\"%1\",").arg(unPacker->GetCharByIndex(k));
                    break;

                case 'S':
                    buf += QString("\"%1\",").arg(Utils::toQString(unPacker->GetStrByIndex(k)));
                    break;

                case 'F':
                    buf += QString("\"%1\",").arg(unPacker->GetDoubleByIndex(k));
                    break;

                case 'R':
                {
                    int nLength = 0;
                    void *lpData = unPacker->GetRawByIndex(k, &nLength);
                    buf += QString("\"raw(len=%1)\",").arg(nLength);
                    //QByteArray array;
                    //array.resize(nLength);
                    //memcpy(array.data(), lpData, nLength);
                    //Utils::toQString(array);
                    break;
                }

                default:
                    // 未知数据类型
                    buf += QString("\"Unknown type:%1\",").arg(type);
                    break;
                }
            }

            unPacker->Next();
        }
    }

    buf += "\r\n";
    return buf;
}

QString Utils::toQString(IBizMessage* pBizMessage)
{
    QString buf("\r\n");
    buf += QString("FunctionId      = %1\r\n").arg(pBizMessage->GetFunction());//功能号
    buf += QString("FunctionId      = %1\r\n").arg(pBizMessage->GetPacketType());//包类型
    buf += QString("BranchNo        = %1\r\n").arg(pBizMessage->GetBranchNo());//营业部号
    buf += QString("SystemNo        = %1\r\n").arg(pBizMessage->GetSystemNo());//系统号
    buf += QString("SubSystemNo     = %1\r\n").arg(pBizMessage->GetSubSystemNo());//子系统号
    buf += QString("SenderId        = %1\r\n").arg(pBizMessage->GetSenderId());//发送者编号
    buf += QString("PacketId        = %1\r\n").arg(pBizMessage->GetPacketId());//包序号
    buf += QString("ErrorNo         = %1\r\n").arg(pBizMessage->GetErrorNo());//错误号
    buf += QString("ErrorInfo       = %1\r\n").arg(Utils::toQString(pBizMessage->GetErrorInfo()).toUtf8().data());//错误信息
    buf += QString("ReturnCode      = %1\r\n").arg(pBizMessage->GetReturnCode());//返回码
    buf += QString("IssueType       = %1\r\n").arg(pBizMessage->GetIssueType());//订阅类型
    buf += QString("SequeceNo       = %1\r\n").arg(pBizMessage->GetSequeceNo());//序号
    buf += QString("CompanyID       = %1\r\n").arg(pBizMessage->GetCompanyID());//公司编号
    buf += QString("SenderCompanyID = %1\r\n").arg(pBizMessage->GetSenderCompanyID());//发送者公司编号

    //业务内容
    int iLen = 0;
    const void* pBuffer = pBizMessage->GetContent(iLen);
    IF2UnPacker* pRespUnPacker = NewUnPacker((void *)pBuffer, iLen);
    pRespUnPacker->AddRef();
    buf += QString("Content         = %1").arg(Utils::toQString(pRespUnPacker));
    pRespUnPacker->Release();

    //buf += QString("SubSystemNo = %1\r\n").arg(pBizMessage->GetTargetInfo(BIZROUTE_INFO& targetInfo));//目的地路由
    //buf += QString("SubSystemNo = %1\r\n").arg(pBizMessage->GetSendInfo(BIZROUTE_INFO& sendInfo));//发送者路由
    //buf += QString("KeyInfo = %1\r\n").arg(pBizMessage->GetKeyInfo(int& iLen));//关键字段信息
    //buf += QString("AppData = %1\r\n").arg(pBizMessage->GetAppData(int& nAppLen));//附加数据，订阅推送时原样返回
    //buf += QString("Buff = %1\r\n").arg(pBizMessage->GetBuff(int& nBuffLen));//二进制

    return buf;
}

IBizMessage* Utils::cloneIBizMessage(IBizMessage* pBizMessage)
{
    IBizMessage* lpNewBizMessage = NewBizMessage();

    lpNewBizMessage->SetFunction(pBizMessage->GetFunction());
    lpNewBizMessage->SetPacketType(pBizMessage->GetPacketType());
    lpNewBizMessage->SetBranchNo(pBizMessage->GetBranchNo());
    lpNewBizMessage->SetSystemNo(pBizMessage->GetSystemNo());
    lpNewBizMessage->SetSubSystemNo(pBizMessage->GetSubSystemNo());

    lpNewBizMessage->SetSenderId(pBizMessage->GetSenderId());
    lpNewBizMessage->SetPacketId(pBizMessage->GetPacketId());
    lpNewBizMessage->SetErrorNo(pBizMessage->GetErrorNo());

    int iErrorInfo = strlen(pBizMessage->GetErrorInfo());
    //char* pErrorInfo = new char[iErrorInfo + 1];
    //memcpy(pErrorInfo, pBizMessage->GetErrorInfo(), iErrorInfo);
    //pErrorInfo[iErrorInfo] = 0;
    lpNewBizMessage->SetErrorInfo(pBizMessage->GetErrorInfo());

    lpNewBizMessage->SetReturnCode(pBizMessage->GetReturnCode());

    lpNewBizMessage->SetIssueType(pBizMessage->GetIssueType());
    lpNewBizMessage->SetSequeceNo(pBizMessage->GetSequeceNo());
    lpNewBizMessage->SetCompanyID(pBizMessage->GetCompanyID());
    lpNewBizMessage->SetSenderCompanyID(pBizMessage->GetSenderCompanyID());

    //没什么用，不搞了
    //lpNewBizMessage->SetTargetInfo(pBizMessage->GetTargetInfo());
    //lpNewBizMessage->SetSendInfo(pBizMessage->GetSendInfo());

    {
        int iLen = 0;
        const void* p = pBizMessage->GetKeyInfo(iLen);
        char* pNew = new char[iLen];
        memcpy(pNew, p, iLen);
        lpNewBizMessage->SetKeyInfo(pNew, iLen);
        delete pNew;
    }

    {
        int iLen = 0;
        const void* p = pBizMessage->GetAppData(iLen);
        char* pNew = new char[iLen];
        memcpy(pNew, p, iLen);
        lpNewBizMessage->SetAppData(pNew, iLen);
        delete pNew;
    }

    {
        int iLen = 0;
        const void* p = pBizMessage->GetBuff(iLen);
        char* pNew = new char[iLen];
        memcpy(pNew, p, iLen);
        lpNewBizMessage->SetBuff(pNew, iLen);
        delete pNew;
    }

    return lpNewBizMessage;
}

void Utils::releaseIBizMessage(IBizMessage* pBizMessage)
{

    pBizMessage->Release();
}

QString Utils::getInDbName()
{
    //db/in_20190101.db
    return QString("%1/%2_%3%4")
        .arg(CONSTANT::DIR_DB)
        .arg(CONSTANT::IN_DB_NAME)
        .arg(QDateTime::currentDateTime().toString("yyyyMMdd"))
        .arg(CONSTANT::DB_POSTFIX);
}

QString Utils::getOutDbName()
{
    //db/out_20190101.db
    return QString("%1/%2_%3%4")
        .arg(CONSTANT::DIR_DB)
        .arg(CONSTANT::OUT_DB_NAME)
        .arg(QDateTime::currentDateTime().toString("yyyyMMdd"))
        .arg(CONSTANT::DB_POSTFIX);
}


//bool Utils::backupHisDb(const QString& strDbPrefixName)
//{
//    QDir dir;
//    QStringList filters;
//    filters << strDbPrefixName + "_????????" + CONSTANT::DB_POSTFIX; //out_????????.db
//    dir.setNameFilters(filters);
//    dir.setPath("./");
//
//    QDirIterator iter(dir);
//    QStringList fileList;
//    while (iter.hasNext())
//    {
//        iter.next();
//        QFileInfo info = iter.fileInfo();
//        fileList << (info.fileName());
//    }
//
//    for (int i = 0; i < fileList.size(); i++)
//    {
//        QString srcFileName = fileList[i];
//        if (Utils::getInDbName() == srcFileName || Utils::getOutDbName() == srcFileName)
//        {
//            continue;
//        }
//
//        //移到db_bak目录
//        QString dstFileName = QString(CONSTANT::DIR_DB)
//            + "/"
//            + srcFileName;
//        if (!QFile::rename(srcFileName, dstFileName))
//        {
//            qWarning() << "Rename file error, src = " << fileList[i] << ", dst = " << dstFileName;
//            continue;
//        }
//        else
//        {
//            qInfo() << "Backup " << fileList[i] << " to " << dstFileName;
//        }
//    }
//
//    return true;
//}

QDateTime Utils::getBuildTime()
{
    QDate buildDate = QLocale(QLocale::English).toDate(QString(__DATE__).replace("  ", " 0"), "MMM dd yyyy"); 
    QTime buildTime = QTime::fromString(__TIME__, "hh:mm:ss");
    return QDateTime(buildDate, buildTime);
}

void Utils::turnOnLogMemory()
{
    m_bLogMemoryAllocate = true;
}

void Utils::regNewMemory(void* ptr, const char* pszFilename, int iLine, const char* pszTypename)
{
    m_memoryLogMap.insert(POINTER_TO_INT(ptr), new TMemoryLog(POINTER_TO_INT(ptr), pszFilename, iLine));
}

void Utils::regDeleteMemory(void* ptr)
{
    int iAddr = POINTER_TO_INT(ptr);
    auto it = m_memoryLogMap.find(iAddr);
    if (it != m_memoryLogMap.end())
    {
        delete it.value();
        m_memoryLogMap.erase(it);
    }
}

QString Utils::AllocatedMemoryToString()
{
    QString str("Allocated memory:");
    for (auto it = m_memoryLogMap.begin(); it != m_memoryLogMap.end(); it++)
    {
        str += QString::asprintf("\r\n[0x%d]%s:%d", it.key(), it.value()->pszFilename, it.value()->iLine);
    }

    return str;
}
