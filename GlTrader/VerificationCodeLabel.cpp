#include "stdafx.h"
#include "VerificationCodeLabel.h"
#include "Utils.h"

VerificationCodeLabel::VerificationCodeLabel(QWidget *parent) 
    : QLabel(parent)
    , m_iLetterCnt(4)
{
    //生成随机种子
    qsrand(QTime::currentTime().second() * 1000 + QTime::currentTime().msec());
    m_pColorArray = new QColor[m_iLetterCnt];
    m_pVerificationCodeArray = new QChar[m_iLetterCnt];
    m_iNoicePointCnt = this->width() * 4;
}

VerificationCodeLabel::~VerificationCodeLabel()
{
    SAFE_DELETE(m_pVerificationCodeArray);
    SAFE_DELETE(m_pColorArray);
}

//重写绘制事件,以此来生成验证码
void VerificationCodeLabel::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);

    //产生4个不同的字符
    produceVerificationCode();

    //产生4个不同的颜色
    produceRandomColor();

    //绘制验证码
    QFont font;
    font.setPointSize(16);
    painter.setFont(font);
    QPoint p;
    for (int i = 0; i < m_iLetterCnt; ++i)
    {
        p.setX(i*(this->width() / m_iLetterCnt) + this->width() / 16);
        p.setY(this->height() / 1.2);
        painter.setPen(m_pColorArray[i]);
        painter.drawText(p, QString(m_pVerificationCodeArray[i]));
    }

    QString str = QString(m_pVerificationCodeArray);
    str.resize(4);
    emit sigVerifyCodeUpdated(str);

    ////绘制噪点
    //for (int j = 0; j < m_iNoicePointCnt; ++j)
    //{
    //    p.setX(qrand() % this->width());
    //    p.setY(qrand() % this->height());
    //    painter.setPen(m_pColorArray[j % 4]);
    //    painter.drawPoint(p);
    //}
}

//这是一个用来生成验证码的函数
void VerificationCodeLabel::produceVerificationCode()
{
    for (int i = 0; i < m_iLetterCnt; ++i)
    {
        m_pVerificationCodeArray[i] = produceRandomLetter();
    }
}

//产生一个随机的字符
QChar VerificationCodeLabel::produceRandomLetter()
{
    const char* const pDict = "23456789abcdefghijkmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ";
    const int iLen = strlen(pDict);
    return *(pDict + qrand() % iLen);

    //QChar c;
    //int flag = qrand() % m_iLetterCnt;
    //switch (flag)
    //{
    //case 0:
    //    c = '0' + qrand() % 10;
    //    break;
    //case 1:
    //    c = 'A' + qrand() % 26;
    //    break;
    //case 2:
    //    c = 'a' + qrand() % 26;
    //    break;
    //default:
    //    c = '0' + qrand() % 10;
    //    break;
    //}
    //return c;
}

//产生随机的颜色
void VerificationCodeLabel::produceRandomColor()
{
    for (int i = 0; i < m_iLetterCnt; ++i)
    {
        m_pColorArray[i] = QColor(qrand() % 255, qrand() % 255, qrand() % 255);
    }
    return;
}

void VerificationCodeLabel::mousePressEvent(QMouseEvent *ev)
{
    update();
}
