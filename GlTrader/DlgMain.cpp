#include "stdafx.h"
#include "DlgMain.h"
#include "CounterThread.h"
#include "EntrustThread.h"
#include "ReportThread.h"
#include "Utils.h"
#include "Context.h"
#include "IconHelper.h"
#include "DlgMsgBox.h"
#include "DlgAbout.h"

DlgMain::DlgMain(CounterThread* pCounterThread, ReportThread* pReportThread, Context* pContext)
    : QDialog(nullptr)
    , m_pContext(pContext)
    , m_pActSelectAllUntreated(nullptr)
    , m_pMediaPlayerError(nullptr)
    , m_pMediaPlayerImportant(nullptr)
{
    m_bLogout = false;
    m_pCounterThread = pCounterThread;
    m_pReportThread = pReportThread;
    m_pEntrustThread = nullptr;

    ui.setupUi(this);

    this->initUi();
    this->initThread();
    this->initSignalSlot();
    this->startThread();
}

DlgMain::~DlgMain()
{
    SAFE_DELETE(m_pMediaPlayerError);
    SAFE_DELETE(m_pMediaPlayerImportant);
    SAFE_QUIT_THREAD(m_pEntrustThread);
}

void DlgMain::initUi()
{
    //窗口关闭后自动析构。如果不调用析构函数，会被检测到内存泄漏（实际未泄漏）
    setAttribute(Qt::WA_DeleteOnClose);

    //窗体显示在屏幕中间位置
    QDesktopWidget *deskdop = QApplication::desktop();
    move((deskdop->width() - this->width()) / 2, (deskdop->height() - this->height()) / 2);

    //设置窗体标题栏隐藏
    this->setWindowFlags(Qt::FramelessWindowHint | Qt::WindowSystemMenuHint | Qt::WindowMinMaxButtonsHint);
    m_recWinLocation = this->geometry();
    m_bWinMax = false;
    m_bMousePressed = false;

    //主菜单
    //QAction *pActSetting = new QAction(&m_menu);
    //pActSetting->setText(QStringLiteral("设置"));
    //connect(pActSetting, SIGNAL(triggered()), this, SLOT(onActionSetting()));

    QAction *pActAbout = new QAction(&m_menu);
    pActAbout->setText(QStringLiteral("关于"));
    connect(pActAbout, SIGNAL(triggered()), this, SLOT(onActionAbout()));

    //m_menu.addAction(pActSetting);
    m_menu.addAction(pActAbout);

    ui.btnMenu->setMenu(&m_menu);


    //安装事件监听器,让标题栏识别鼠标双击
    ui.lblTitle->installEventFilter(this);
    ui.lblTitle->setText(Utils::toQString(CONSTANT::APP_NAME) + " " + Utils::toQString(CONSTANT::VERSION_TYPE));

    IconHelper::Instance()->SetIcon(ui.btnClose, QChar(0xf00d), 10);
    IconHelper::Instance()->SetIcon(ui.btnWinMax, QChar(0xf096), 10);
    IconHelper::Instance()->SetIcon(ui.btnWinMin, QChar(0xf068), 10);
    IconHelper::Instance()->SetIcon(ui.btnMenu, QChar(0xf0c9), 10);

    onBtnWinMaxClicked();

    //垂直分裂器
    ui.splitter->setStretchFactor(0, 2);
    ui.splitter->setStretchFactor(0, 1);

    //选项卡
    ui.tabWidget->setCurrentIndex(0);

    //状态条
    ui.lblRunStatus->setText(QStringLiteral("欢迎使用") + Utils::toQString(CONSTANT::APP_NAME) + " " + Utils::toQString(CONSTANT::VERSION_TYPE));
    ui.lblUsername->setText(m_pContext->getUsername());

#define ADD_TBL_COL(tblMap, listString, no, colName) \
    do{\
        listString.push_back(colName); \
        tblMap.insert(colName, no); \
    }while(0)


    //待报委托
    {
        int seq = 0;
        QList<QString> lstString;

        ADD_TBL_COL(m_tblEntrustColMap, lstString, seq++, TBL_ENTRUST_COL::CHECKBOX);//复选框
        ADD_TBL_COL(m_tblEntrustColMap, lstString, seq++, TBL_ENTRUST_COL::NO);//主键
        ADD_TBL_COL(m_tblEntrustColMap, lstString, seq++, TBL_ENTRUST_COL::ACCOUNT_CODE);
        ADD_TBL_COL(m_tblEntrustColMap, lstString, seq++, TBL_ENTRUST_COL::ACCOUNT_NAME);
        ADD_TBL_COL(m_tblEntrustColMap, lstString, seq++, TBL_ENTRUST_COL::COMBI_NO);
        ADD_TBL_COL(m_tblEntrustColMap, lstString, seq++, TBL_ENTRUST_COL::COMBI_NAME);
        ADD_TBL_COL(m_tblEntrustColMap, lstString, seq++, TBL_ENTRUST_COL::MARKET_NO);
        ADD_TBL_COL(m_tblEntrustColMap, lstString, seq++, TBL_ENTRUST_COL::STOCK_CODE);
        ADD_TBL_COL(m_tblEntrustColMap, lstString, seq++, TBL_ENTRUST_COL::STOCK_NAME);//TODO 如何获取证券名称
        ADD_TBL_COL(m_tblEntrustColMap, lstString, seq++, TBL_ENTRUST_COL::ENTRUST_DIRECTION);
        ADD_TBL_COL(m_tblEntrustColMap, lstString, seq++, TBL_ENTRUST_COL::PRICE_TYPE);
        ADD_TBL_COL(m_tblEntrustColMap, lstString, seq++, TBL_ENTRUST_COL::ENTRUST_PRICE);
        ADD_TBL_COL(m_tblEntrustColMap, lstString, seq++, TBL_ENTRUST_COL::ENTRUST_AMOUNT);
        ADD_TBL_COL(m_tblEntrustColMap, lstString, seq++, TBL_ENTRUST_COL::ENTRUST_BALANCE);
        ADD_TBL_COL(m_tblEntrustColMap, lstString, seq++, TBL_ENTRUST_COL::FALG);//委托/撤单
        ADD_TBL_COL(m_tblEntrustColMap, lstString, seq++, TBL_ENTRUST_COL::EXT_ACCESS_SYSTEM_ID);
        ADD_TBL_COL(m_tblEntrustColMap, lstString, seq++, TBL_ENTRUST_COL::ORIGN_ENTRUST_NO);//如果是撤单，则填入原委托entrust_no

        QStringList list(lstString);
        ui.tblUntreatedList->setColumnCount(list.size());
        ui.tblUntreatedList->setHorizontalHeaderLabels(list);
        for (int i = 0; i < list.size(); i++)
        {
            ui.tblUntreatedList->setColumnWidth(i, 80);
        }
        ui.tblUntreatedList->setColumnWidth(m_tblEntrustColMap[TBL_ENTRUST_COL::CHECKBOX], 20);//复选框
        ui.tblUntreatedList->setColumnWidth(m_tblEntrustColMap[TBL_ENTRUST_COL::NO], 40); //id
        ui.tblUntreatedList->setColumnWidth(m_tblEntrustColMap[TBL_ENTRUST_COL::STOCK_NAME], 0);

        ui.tblUntreatedList->setEditTriggers(QAbstractItemView::NoEditTriggers);
    }
    //选择、反选所有待处理委托
    m_pActSelectAllUntreated = new QAction(this);
    m_pActSelectAllUntreated->setShortcut(tr("ctrl+a"));
    this->addAction(m_pActSelectAllUntreated);


    //委托查询
    {
        int seq = 0;
        QList<QString> lstString;

        ADD_TBL_COL(m_tblEntrustListColMap, lstString, seq++, TBL_ENTRUSTLIST_COL::NO);
        ADD_TBL_COL(m_tblEntrustListColMap, lstString, seq++, TBL_ENTRUSTLIST_COL::BUSINESS_DATE);
        ADD_TBL_COL(m_tblEntrustListColMap, lstString, seq++, TBL_ENTRUSTLIST_COL::BUSINESS_TIME);
        ADD_TBL_COL(m_tblEntrustListColMap, lstString, seq++, TBL_ENTRUSTLIST_COL::ENTRUST_NO);
        ADD_TBL_COL(m_tblEntrustListColMap, lstString, seq++, TBL_ENTRUSTLIST_COL::ENTRUST_STATUS);
        ADD_TBL_COL(m_tblEntrustListColMap, lstString, seq++, TBL_ENTRUSTLIST_COL::ACCOUNT_CODE);
        ADD_TBL_COL(m_tblEntrustListColMap, lstString, seq++, TBL_ENTRUSTLIST_COL::ACCOUNT_NAME);
        ADD_TBL_COL(m_tblEntrustListColMap, lstString, seq++, TBL_ENTRUSTLIST_COL::COMBI_NO);
        ADD_TBL_COL(m_tblEntrustListColMap, lstString, seq++, TBL_ENTRUSTLIST_COL::COMBI_NAME);
        ADD_TBL_COL(m_tblEntrustListColMap, lstString, seq++, TBL_ENTRUSTLIST_COL::MARKET_NO);
        ADD_TBL_COL(m_tblEntrustListColMap, lstString, seq++, TBL_ENTRUSTLIST_COL::STOCK_CODE);
        ADD_TBL_COL(m_tblEntrustListColMap, lstString, seq++, TBL_ENTRUSTLIST_COL::STOCK_NAME);
        ADD_TBL_COL(m_tblEntrustListColMap, lstString, seq++, TBL_ENTRUSTLIST_COL::ENTRUST_DIRECTION);
        ADD_TBL_COL(m_tblEntrustListColMap, lstString, seq++, TBL_ENTRUSTLIST_COL::PRICE_TYPE);
        ADD_TBL_COL(m_tblEntrustListColMap, lstString, seq++, TBL_ENTRUSTLIST_COL::ENTRUST_PRICE);
        ADD_TBL_COL(m_tblEntrustListColMap, lstString, seq++, TBL_ENTRUSTLIST_COL::ENTRUST_AMOUNT);
        ADD_TBL_COL(m_tblEntrustListColMap, lstString, seq++, TBL_ENTRUSTLIST_COL::ENTRUST_BALANCE);
        ADD_TBL_COL(m_tblEntrustListColMap, lstString, seq++, TBL_ENTRUSTLIST_COL::CANCEL_DEAL_AMOUNT);
        ADD_TBL_COL(m_tblEntrustListColMap, lstString, seq++, TBL_ENTRUSTLIST_COL::FAIL_CAUSE);
        ADD_TBL_COL(m_tblEntrustListColMap, lstString, seq++, TBL_ENTRUSTLIST_COL::EXT_ACCESS_SYSTEM_ID);
        ADD_TBL_COL(m_tblEntrustListColMap, lstString, seq++, TBL_ENTRUSTLIST_COL::SEND_HANDLE);

        QStringList list(lstString);
        ui.tblEntrustList->setColumnCount(list.size());
        ui.tblEntrustList->setHorizontalHeaderLabels(list);
        for (int i = 0; i < list.size(); i++)
        {
            ui.tblEntrustList->setColumnWidth(i, 80);
        }
        ui.tblEntrustList->setColumnWidth(m_tblEntrustListColMap[TBL_ENTRUSTLIST_COL::NO], 40);
        ui.tblEntrustList->setColumnWidth(m_tblEntrustListColMap[TBL_ENTRUSTLIST_COL::STOCK_NAME], 0);
        ui.tblEntrustList->setColumnWidth(m_tblEntrustListColMap[TBL_ENTRUSTLIST_COL::SEND_HANDLE], 0);

        ui.tblEntrustList->setEditTriggers(QAbstractItemView::NoEditTriggers);

        ui.tblEntrustList->verticalHeader()->setHidden(true);// 隐藏行号
    }

    //成交查询
    {
        int seq = 0;
        QList<QString> lstString;

        ADD_TBL_COL(m_tblDealListColMap, lstString, seq++, TBL_DEALLIST_COL::DEAL_DATE);
        ADD_TBL_COL(m_tblDealListColMap, lstString, seq++, TBL_DEALLIST_COL::DEAL_TIME);
        ADD_TBL_COL(m_tblDealListColMap, lstString, seq++, TBL_DEALLIST_COL::DEAL_NO);
        ADD_TBL_COL(m_tblDealListColMap, lstString, seq++, TBL_DEALLIST_COL::ENTRUST_NO);
        ADD_TBL_COL(m_tblDealListColMap, lstString, seq++, TBL_DEALLIST_COL::ACCOUNT_CODE);
        ADD_TBL_COL(m_tblDealListColMap, lstString, seq++, TBL_DEALLIST_COL::ACCOUNT_NAME);
        ADD_TBL_COL(m_tblDealListColMap, lstString, seq++, TBL_DEALLIST_COL::COMBI_NO);
        ADD_TBL_COL(m_tblDealListColMap, lstString, seq++, TBL_DEALLIST_COL::COMBI_NAME);
        ADD_TBL_COL(m_tblDealListColMap, lstString, seq++, TBL_DEALLIST_COL::MARKET_NO);
        ADD_TBL_COL(m_tblDealListColMap, lstString, seq++, TBL_DEALLIST_COL::STOCK_CODE);
        ADD_TBL_COL(m_tblDealListColMap, lstString, seq++, TBL_DEALLIST_COL::STOCK_NAME);
        ADD_TBL_COL(m_tblDealListColMap, lstString, seq++, TBL_DEALLIST_COL::ENTRUST_DIRECTION);
        ADD_TBL_COL(m_tblDealListColMap, lstString, seq++, TBL_DEALLIST_COL::DEAL_AMOUNT);
        ADD_TBL_COL(m_tblDealListColMap, lstString, seq++, TBL_DEALLIST_COL::DEAL_PRICE);
        ADD_TBL_COL(m_tblDealListColMap, lstString, seq++, TBL_DEALLIST_COL::DEAL_BALANCE);
        ADD_TBL_COL(m_tblDealListColMap, lstString, seq++, TBL_DEALLIST_COL::DEAL_FEE);
        ADD_TBL_COL(m_tblDealListColMap, lstString, seq++, TBL_DEALLIST_COL::EXT_ACCESS_SYSTEM_ID);

        QStringList list(lstString);
        ui.tblDealList->setColumnCount(list.size());
        ui.tblDealList->setHorizontalHeaderLabels(list);
        for (int i = 0; i < list.size(); i++)
        {
            ui.tblDealList->setColumnWidth(i, 80);
        }
        ui.tblDealList->setColumnWidth(m_tblDealListColMap[TBL_DEALLIST_COL::STOCK_NAME], 0);

        ui.tblDealList->setEditTriggers(QAbstractItemView::NoEditTriggers);
    }
#undef ADD_TBL_COL

    //快速下单
    ui.chkQuickEntrust->setChecked(ConfigMgr::getData().bQuickEntrust);

    //委托查询筛选
    ui.chkUndeal->setChecked(true);
    ui.chkDeal->setChecked(true);
    ui.chkWithdraw->setChecked(true);
    ui.chkFail->setChecked(true);

    //自动启动监控
    if (ConfigMgr::getData().bAutoMonitor)
    {
        ui.chkStartMonitor->setChecked(true);
        
        this->onRunLog(RunLog_Info, QStringLiteral("文件监控已开始！"));
    }
    else
    {
        ui.chkStartMonitor->setChecked(false);

        this->onRunLog(RunLog_Info, QStringLiteral("文件监控已停止！"));
    }

    //委托成交列表
    int iDealCnt = 0;
    OrderList& orderList = m_pContext->getOrderList();
    for (int i = 0; i < orderList.size(); i++)
    {
        iDealCnt += orderList[i]->vecDealDetail.size();
        onEntrustResult(orderList[i]);
    }
    onRunLog(RunLog_Info, QString(QStringLiteral("载入 %1 条委托查询数据， %2 条成交查询数据").arg(orderList.size()).arg(iDealCnt)));

    //待报委托
    orderList = m_pContext->getUntreatedOrderList();
    SAFE_NEW(OrderList, pEntrustOrderList);
    SAFE_NEW(OrderList, pWithdrawOrderList);
    for (int i = 0; i < orderList.size(); i++)
    {
        if (orderList[i]->eEntrustType == EntrustType::Entrust)
        {
            pEntrustOrderList->push_back(orderList[i]);
        }
        else
        {
            pWithdrawOrderList->push_back(orderList[i]);
        }
    }

    if (pEntrustOrderList->size() > 0)
    {
        this->onRunLog(RunLog_Important, QStringLiteral("载入 %1 条新订单委托，请点击下单按钮进行下单").arg(pEntrustOrderList->size()));
    }
    if (pWithdrawOrderList->size() > 0)
    {
        this->onRunLog(RunLog_Important, QStringLiteral("载入 %1 条撤单委托，请点击下单按钮进行下单").arg(pWithdrawOrderList->size()));
    }

    this->onNewEntrust(pEntrustOrderList);
    this->onNewWithdraw(pWithdrawOrderList);

    //载入警告音（不能放资源中）
    m_pMediaPlayerError = new QMediaPlayer;
    m_pMediaPlayerError->setMedia(QUrl("sound/error.wav"));
    m_pMediaPlayerError->setVolume(100);

    m_pMediaPlayerImportant = new QMediaPlayer;
    m_pMediaPlayerImportant->setMedia(QUrl("sound/important.wav"));
    m_pMediaPlayerImportant->setVolume(100);

}

void DlgMain::initSignalSlot()
{
    //ui
    connect(ui.btnClose, SIGNAL(clicked()), this, SLOT(close()));
    connect(ui.btnWinMax, SIGNAL(clicked()), this, SLOT(onBtnWinMaxClicked()));
    connect(ui.btnWinMin, SIGNAL(clicked()), this, SLOT(onBtnWinMinClicked()));

    connect(ui.btnEntrust, SIGNAL(clicked()), this, SLOT(onClickButtonEntrust()));

    connect(m_pActSelectAllUntreated, SIGNAL(triggered()), this, SLOT(onBtnSelectAllClicked()));

    connect(ui.chkStartMonitor, SIGNAL(stateChanged(int)), this, SLOT(onClickCheckStartMonitor(int)));
    connect(ui.chkQuickEntrust, SIGNAL(stateChanged(int)), this, SLOT(onClickCheckQuickEntrust(int)));

    connect(ui.btnSelectAll, SIGNAL(clicked()), this, SLOT(onBtnSelectAllClicked()));
    connect(ui.btnSelectReverse, SIGNAL(clicked()), this, SLOT(onBtnSelectReverseClicked()));
    connect(ui.btnRemoveAllUntreated, SIGNAL(clicked()), this, SLOT(onBtnRemoveAllUntreatedClicked()));

    connect(ui.chkUndeal, SIGNAL(stateChanged(int)), this, SLOT(onClickCheckSelectEntrustList()));
    connect(ui.chkDeal, SIGNAL(stateChanged(int)), this, SLOT(onClickCheckSelectEntrustList()));
    connect(ui.chkWithdraw, SIGNAL(stateChanged(int)), this, SLOT(onClickCheckSelectEntrustList()));
    connect(ui.chkFail, SIGNAL(stateChanged(int)), this, SLOT(onClickCheckSelectEntrustList()));

    //ui -> 柜台线程
    connect(this, SIGNAL(sigLogout()), m_pCounterThread, SLOT(onLogout()));

    connect(this, SIGNAL(sigEntrustReq(OrderList*)), m_pCounterThread, SLOT(onEntrustCombi(OrderList*)));
    connect(this, SIGNAL(sigWithdrawReq(OrderList*)), m_pCounterThread, SLOT(onWithdraw(OrderList*)));

    //ui -> 回报线程，在委托成功发送到柜台后更新委托和撤单最大id
    connect(this, SIGNAL(sigNewEntrustHandled(int)), m_pReportThread, SLOT(onNewEntrustHandled(int)));
    connect(this, SIGNAL(sigNewWithdrawHandled(int)), m_pReportThread, SLOT(onNewWithdrawHandled(int)));

    //柜台线程->ui
    connect(m_pCounterThread, SIGNAL(sigRunLog(int, QString)), this, SLOT(onRunLog(int, QString)));
    connect(m_pCounterThread, SIGNAL(sigLogoutResult(int, QString)), this, SLOT(onLogoutResult(int, QString)));
    //包括委托和撤单
    connect(m_pCounterThread, SIGNAL(sigEntrustResult(Order*)), this, SLOT(onEntrustResult(Order*)));

    //新委托线程->ui
    connect(m_pEntrustThread, SIGNAL(sigRunLog(int, QString)), this, SLOT(onRunLog(int, QString)));
    if (ui.chkQuickEntrust->isChecked())
    {
        //将接收到的新委托信号直接发送给柜台线程（需根据设置来决定是否自动下单）
        connect(m_pEntrustThread, SIGNAL(sigNewEntrust(OrderList*)), this, SIGNAL(sigEntrustReq(OrderList*)));
        connect(m_pEntrustThread, SIGNAL(sigNewWithdraw(OrderList*)), this, SIGNAL(sigWithdrawReq(OrderList*)));
    }
    else
    {
        //界面显示新委托
        connect(m_pEntrustThread, SIGNAL(sigNewEntrust(OrderList*)), this, SLOT(onNewEntrust(OrderList*)));
        connect(m_pEntrustThread, SIGNAL(sigNewWithdraw(OrderList*)), this, SLOT(onNewWithdraw(OrderList*)));
    }

    //回报线程->ui
    connect(m_pReportThread, SIGNAL(sigRunLog(int, QString)), this, SLOT(onRunLog(int, QString)));

    //柜台线程->回报线程，在委托成功发送到柜台后更新委托和撤单最大id
    connect(m_pCounterThread, SIGNAL(sigNewEntrustHandled(int)), m_pReportThread, SLOT(onNewEntrustHandled(int)));
    connect(m_pCounterThread, SIGNAL(sigNewWithdrawHandled(int)), m_pReportThread, SLOT(onNewWithdrawHandled(int)));

    //根据撤单委托中的委托编号查询原委托
    connect(m_pEntrustThread, SIGNAL(sigQueryOrderByEntrustNo(OrderList*)), m_pCounterThread, SLOT(onQueryOrderByEntrustNo(OrderList*)));
    connect(m_pCounterThread, SIGNAL(sigQueryOrderByEntrustNoResult(OrderList*)), m_pEntrustThread, SLOT(onQueryOrderByEntrustNoResult(OrderList*)));

}

void DlgMain::initThread()
{
    //设置已经处理的最大委托id和最大撤单id
    int iMaxEntrustId = m_pContext->queryRunConfig(RUN_CONFIG::MAX_ENTRUST_ID).toInt();
    int iMaxWithdrawId = m_pContext->queryRunConfig(RUN_CONFIG::MAX_WITHDRAW_ID).toInt();

    //先启动回报线程
    if (nullptr == m_pReportThread)
    {
        m_pReportThread = new ReportThread(ConfigMgr::getData());
        //m_pReportThread->start();
    }

    //再启动委托线程
    if (nullptr == m_pEntrustThread)
    {
        m_pEntrustThread = new EntrustThread(ConfigMgr::getData());

        m_pEntrustThread->setMaxId(iMaxEntrustId, iMaxWithdrawId);

        //m_pEntrustThread->start();
    }

    //if (ConfigMgr::getData().bAutoMonitor)
    //{
    //    m_pEntrustThread->startMonitor();
    //}

}

void DlgMain::startThread()
{
    //先启动回报线程
    m_pReportThread->start();

    //再启动委托线程
    m_pEntrustThread->start();

    //最后开启委托监控
    if (ConfigMgr::getData().bAutoMonitor)
    {
        m_pEntrustThread->startMonitor();
    }

}

bool DlgMain::eventFilter(QObject *obj, QEvent *event)
{
    if (event->type() == QEvent::MouseButtonDblClick) {
        this->onBtnWinMaxClicked();
        return true;
    }
    return QObject::eventFilter(obj, event);
}

void DlgMain::mouseMoveEvent(QMouseEvent *e)
{
    if (m_bMousePressed && (e->buttons() == Qt::LeftButton) && !m_bWinMax)
    {
        this->move(e->globalPos() - m_ptMousePoint);
        e->accept();
    }
}

void DlgMain::mousePressEvent(QMouseEvent *e)
{
    if (e->button() == Qt::LeftButton)
    {
        m_bMousePressed = true;
        m_ptMousePoint = e->globalPos() - this->pos();
        e->accept();
    }
}

void DlgMain::mouseReleaseEvent(QMouseEvent *)
{
    m_bMousePressed = false;
}

void DlgMain::onBtnWinMaxClicked()
{
    if (m_bWinMax) {
        this->setGeometry(m_recWinLocation);
        IconHelper::Instance()->SetIcon(ui.btnWinMax, QChar(0xf096), 10);
        ui.btnWinMax->setToolTip(QStringLiteral("最大化"));
    }
    else {
        m_recWinLocation = this->geometry();
        this->setGeometry(qApp->desktop()->availableGeometry());
        IconHelper::Instance()->SetIcon(ui.btnWinMax, QChar(0xf079), 10);
        ui.btnWinMax->setToolTip(QStringLiteral("还原"));
    }
    m_bWinMax = !m_bWinMax;
}

void DlgMain::onBtnWinMinClicked()
{
    this->showMinimized();
}

void DlgMain::closeEvent(QCloseEvent *event)
{
    if (m_bLogout)
    {
        ConfigMgr::getInstance()->save();
        event->accept();

        qDebug() << "Exit program";
        return;
    }

    DlgMsgBox* pMsgBox = new DlgMsgBox;
    pMsgBox->SetMessage(DlgMsgBox::Question, QStringLiteral("提示"), QStringLiteral("确认退出本软件吗？"));
    int iRet = pMsgBox->exec();
    if (iRet == QDialog::Accepted)//确认
    {
        emit sigLogout();
        event->ignore();
    }
    else
    {
        event->ignore();
    }

    //int iRet = QMessageBox::information(this, QStringLiteral("提示"), QStringLiteral("你确定退出本软件？"), QStringLiteral("确定"), QStringLiteral("取消"), 0, 1);
    //switch (iRet)
    //{
    //case 0://确定
    //    emit sigLogout();
    //    event->ignore();
    //    break;
    //case 1://取消
    //default:
    //    event->ignore();
    //    break;
    //}
}

void DlgMain::onLogoutResult(int iRetCode, QString strErrMsg)
{
    //先销毁DlgLogin，再销毁DlgMain
    SAFE_QUIT_THREAD(m_pEntrustThread);

    //qApp->quit();

    m_bLogout = true;
    this->close();
}

void DlgMain::onClickCheckStartMonitor(int checkState)
{
    if (Qt::Checked == checkState)
    {
        m_pEntrustThread->startMonitor();
        ConfigMgr::getData().bAutoMonitor = true;

        this->onRunLog(RunLog_Info, QStringLiteral("文件监控已开始！"));
    }
    else
    {
        m_pEntrustThread->stopMonitor();
        ConfigMgr::getData().bAutoMonitor = false;

        this->onRunLog(RunLog_Important, QStringLiteral("文件监控已停止！"));
    }
}

//void DlgMain::onSelectAllUntreated()
//{
//    Qt::CheckState state = Qt::Checked;
//    if (m_bSelectAllUntreated)
//    {
//        state = Qt::Checked;
//    }
//    else
//    {
//        state = Qt::Unchecked;
//    }
//    m_bSelectAllUntreated = !m_bSelectAllUntreated;
//
//    for (int i = ui.tblUntreatedList->rowCount() - 1; i >= 0; i--)
//    {
//        QTableWidgetItem* pCheck = ui.tblUntreatedList->item(i, m_tblEntrustColMap[TBL_ENTRUST_COL::CHECKBOX]);
//        pCheck->setCheckState(state);
//    }
//}

//点击下单按钮
void DlgMain::onClickButtonEntrust()
{
    OrderList* pOrderListEntrust = new OrderList;
    OrderList* pOrderListWithdraw = new OrderList;

    for (int i = ui.tblUntreatedList->rowCount() - 1; i >= 0; i--)
    {
        QTableWidgetItem* check = ui.tblUntreatedList->item(i, m_tblEntrustColMap[TBL_ENTRUST_COL::CHECKBOX]);
        if (check->checkState() == Qt::Checked)
        {
            int id = ui.tblUntreatedList->item(i, m_tblEntrustColMap[TBL_ENTRUST_COL::NO])->text().toInt();
            auto it = m_untreatedOrderMap.find(id);
            if (it != m_untreatedOrderMap.end())
            {
                //区分是委托还是撤单
                if (it.value()->eEntrustType == EntrustType::Entrust)
                {
                    pOrderListEntrust->push_back(it.value());
                }
                else
                {
                    pOrderListWithdraw->push_back(it.value());
                }

                m_untreatedOrderMap.remove(id);
            }
            else
            {
                qCritical() << "Cant't find order: id = " << id;
            }
            ui.tblUntreatedList->removeRow(i);
        }

    }

    if (pOrderListEntrust->size() > 0)
    {
        emit sigEntrustReq(pOrderListEntrust);
    }
    else
    {
        SAFE_DELETE(pOrderListEntrust);
    }

    if (pOrderListWithdraw->size() > 0)
    {
        emit sigWithdrawReq(pOrderListWithdraw);
    }
    else
    {
        SAFE_DELETE(pOrderListWithdraw);
    }
}

void DlgMain::onClickCheckQuickEntrust(int checkState)
{
    if (checkState == Qt::Checked)//快速下单
    {
        disconnect(m_pEntrustThread, SIGNAL(sigNewEntrust(OrderList*)), this, SLOT(onNewEntrust(OrderList*)));
        disconnect(m_pEntrustThread, SIGNAL(sigNewWithdraw(OrderList*)), this, SLOT(onNewWithdraw(OrderList*)));

        connect(m_pEntrustThread, SIGNAL(sigNewEntrust(OrderList*)), this, SIGNAL(sigEntrustReq(OrderList*)));
        connect(m_pEntrustThread, SIGNAL(sigNewWithdraw(OrderList*)), this, SIGNAL(sigWithdrawReq(OrderList*)));

        ConfigMgr::getData().bQuickEntrust = true;

        this->onRunLog(RunLog_Important, QStringLiteral("快速下单已开启，但历史待处理委托不会自动下单"));
    }
    else//手工下单
    {
        disconnect(m_pEntrustThread, SIGNAL(sigNewEntrust(OrderList*)), this, SIGNAL(sigEntrustReq(OrderList*)));
        disconnect(m_pEntrustThread, SIGNAL(sigNewWithdraw(OrderList*)), this, SIGNAL(sigWithdrawReq(OrderList*)));

        connect(m_pEntrustThread, SIGNAL(sigNewEntrust(OrderList*)), this, SLOT(onNewEntrust(OrderList*)));
        connect(m_pEntrustThread, SIGNAL(sigNewWithdraw(OrderList*)), this, SLOT(onNewWithdraw(OrderList*)));

        ConfigMgr::getData().bQuickEntrust = false;
        this->onRunLog(RunLog_Important, QStringLiteral("快速下单已关闭，请手工下单"));
    }

}

void DlgMain::onBtnRemoveAllUntreatedClicked()
{
    if (ui.tblUntreatedList->rowCount() == 0)
    {
        DlgMsgBox* pMsgBox = new DlgMsgBox;
        pMsgBox->SetMessage(DlgMsgBox::Info, QStringLiteral("提示"), QStringLiteral("没有待报委托"));
        pMsgBox->exec();
        return;
    }

    DlgMsgBox* pMsgBox = new DlgMsgBox;
    pMsgBox->SetMessage(DlgMsgBox::Question, QStringLiteral("警告"), QStringLiteral("确认清空所有待报委托吗？"));
    int iRet = pMsgBox->exec();
    if (QDialog::Accepted != iRet)
    {
        return;
    }

    int cnt = ui.tblUntreatedList->rowCount();
    for (int i = ui.tblUntreatedList->rowCount() - 1; i >= 0; i--)
    {
        int id = ui.tblUntreatedList->item(i, m_tblEntrustColMap[TBL_ENTRUST_COL::NO])->text().toInt();
        auto it = m_untreatedOrderMap.find(id);
        if (it == m_untreatedOrderMap.end())
        {
            continue;
        }

        Order* pOrder = it.value();
        if (pOrder->eEntrustType == EntrustType::Entrust)
        {
            emit sigNewEntrustHandled(pOrder->iInternalId);
        }
        else
        {
            emit sigNewWithdrawHandled(pOrder->iInternalId);
        }

        delete pOrder;
        m_untreatedOrderMap.remove(id);

        ui.tblUntreatedList->removeRow(i);
    }

    m_untreatedOrderMap.clear();

    this->onRunLog(RunLog_Important, QStringLiteral("已清空 %1 条待报记录").arg(cnt));
}

void DlgMain::onBtnSelectReverseClicked()
{
    for (int i = ui.tblUntreatedList->rowCount() - 1; i >= 0; i--)
    {
        QTableWidgetItem* pCheck = ui.tblUntreatedList->item(i, m_tblEntrustColMap[TBL_ENTRUST_COL::CHECKBOX]);
        if (pCheck->checkState() == Qt::Checked)
        {
            pCheck->setCheckState(Qt::Unchecked);
        }
        else
        {
            pCheck->setCheckState(Qt::Checked);
        }
    }
}

void DlgMain::onBtnSelectAllClicked()
{
    for (int i = ui.tblUntreatedList->rowCount() - 1; i >= 0; i--)
    {
        QTableWidgetItem* pCheck = ui.tblUntreatedList->item(i, m_tblEntrustColMap[TBL_ENTRUST_COL::CHECKBOX]);
        pCheck->setCheckState(Qt::Checked);
    }
}

void DlgMain::onNewEntrust(QVector<Order*>* pOrderList)
{
    for (int i = 0; i < pOrderList->size(); i++)
    {
        Order* pOrder = (*pOrderList)[i];

        //更新待处理委托列表
        qDebug() << "DlgMain::onNewEntrust, " << pOrder->toQString().toUtf8().data();

        int row = 0, col = 0;
        ui.tblUntreatedList->insertRow(row);

        QTableWidgetItem *check = new QTableWidgetItem();
        check->setCheckState(Qt::Checked);
        ui.tblUntreatedList->setItem(row, col++, check); //插入复选框

        ui.tblUntreatedList->setItem(row, col++, new QTableWidgetItem(QString("%1").arg(pOrder->iInternalId)));
        ui.tblUntreatedList->setItem(row, col++, new QTableWidgetItem(pOrder->strAccountCode));
        ui.tblUntreatedList->setItem(row, col++, new QTableWidgetItem(m_pContext->queryDict(DICT_ID::ACCOUNT_CODE, pOrder->strAccountCode)));
        ui.tblUntreatedList->setItem(row, col++, new QTableWidgetItem(pOrder->strCombiNo));
        ui.tblUntreatedList->setItem(row, col++, new QTableWidgetItem(m_pContext->queryDict(DICT_ID::COMBI_NO, pOrder->strCombiNo)));
        ui.tblUntreatedList->setItem(row, col++, new QTableWidgetItem(m_pContext->queryDict(DICT_ID::MARKET_NO, pOrder->strMarketNo)));
        ui.tblUntreatedList->setItem(row, col++, new QTableWidgetItem(pOrder->strStockCode));
        ui.tblUntreatedList->setItem(row, col++, new QTableWidgetItem(pOrder->strStockName));
        ui.tblUntreatedList->setItem(row, col++, new QTableWidgetItem(m_pContext->queryDict(DICT_ID::ENTRUST_DIRECTION, pOrder->strEntrustDirection)));
        ui.tblUntreatedList->setItem(row, col++, new QTableWidgetItem(m_pContext->queryDict(DICT_ID::PRICE_TYPE, pOrder->strPriceType)));
        ui.tblUntreatedList->setItem(row, col++, new QTableWidgetItem(QString("%1").arg(pOrder->fEntrustPrice)));
        ui.tblUntreatedList->setItem(row, col++, new QTableWidgetItem(QString("%1").arg(pOrder->fEntrustAmount)));
        ui.tblUntreatedList->setItem(row, col++, new QTableWidgetItem(QString("%1").arg(pOrder->fEntrustPrice * pOrder->fEntrustAmount)));
        ui.tblUntreatedList->setItem(row, col++, new QTableWidgetItem(QString(QStringLiteral("委托"))));
        ui.tblUntreatedList->setItem(row, col++, new QTableWidgetItem(QString("%1").arg(pOrder->iExtAccessSystemId)));
        ui.tblUntreatedList->setItem(row, col++, new QTableWidgetItem(QString("")));

        m_untreatedOrderMap.insert(pOrder->iInternalId, pOrder);
    }
    SAFE_DELETE(pOrderList);
}

void DlgMain::onEntrustResult(Order* pOrder)
{
    qDebug() << "DlgMain::onEntrustResult, " << pOrder->toQString().toUtf8().data();

    //更新成交明细
    if (pOrder->vecDealDetail.size() > 0)
    {
        updateDealList(pOrder);
    }

    //更新委托明细（已成、未成都要更新）
    updateEntrustList(pOrder);
}

bool DlgMain::isEntrustHidden(int iRow, bool bUndeal, bool bDeal, bool bWithdraw, bool bFail)
{
    bool bHide = true;

    QString strEntrustStatus = ui.tblEntrustList->item(iRow, m_tblEntrustListColMap[TBL_ENTRUSTLIST_COL::ENTRUST_STATUS])->text();

    //未成
    if ((strEntrustStatus == m_pContext->queryDict(DICT_ID::ENTRUST_STATUS, ENTRUST_STATUS::UNACK)      //未响应
        || strEntrustStatus == m_pContext->queryDict(DICT_ID::ENTRUST_STATUS, ENTRUST_STATUS::UNREPORT)  //未报
        || strEntrustStatus == m_pContext->queryDict(DICT_ID::ENTRUST_STATUS, ENTRUST_STATUS::TO_REPORT) //待报
        || strEntrustStatus == m_pContext->queryDict(DICT_ID::ENTRUST_STATUS, ENTRUST_STATUS::REPORTING) //正报
        || strEntrustStatus == m_pContext->queryDict(DICT_ID::ENTRUST_STATUS, ENTRUST_STATUS::REPORTED)  //已报
        || strEntrustStatus == m_pContext->queryDict(DICT_ID::ENTRUST_STATUS, ENTRUST_STATUS::TO_WITHDRAW) //待撤
        || strEntrustStatus == m_pContext->queryDict(DICT_ID::ENTRUST_STATUS, ENTRUST_STATUS::DEAL_PART) //部成
        || strEntrustStatus == m_pContext->queryDict(DICT_ID::ENTRUST_STATUS, ENTRUST_STATUS::UNAPPROVE)  //未审批
        ) && bUndeal)
    {
        bHide = false;
    }

    //已成
    if ((strEntrustStatus == m_pContext->queryDict(DICT_ID::ENTRUST_STATUS, ENTRUST_STATUS::DEAL) //已成
        || strEntrustStatus == m_pContext->queryDict(DICT_ID::ENTRUST_STATUS, ENTRUST_STATUS::WITHDRAW_PART) //部撤
        ) && bDeal)
    {
        bHide = false;
    }

    //已撤
    if ((strEntrustStatus == m_pContext->queryDict(DICT_ID::ENTRUST_STATUS, ENTRUST_STATUS::WITHDRAW) //已撤
        || strEntrustStatus == m_pContext->queryDict(DICT_ID::ENTRUST_STATUS, ENTRUST_STATUS::WITHDRAW_PART) //部撤
        || strEntrustStatus == m_pContext->queryDict(DICT_ID::ENTRUST_STATUS, ENTRUST_STATUS::UNAPPROVE_CANCEL) //未审批即撤销
        ) && bWithdraw)
    {
        bHide = false;
    }

    //废单
    if ((strEntrustStatus == m_pContext->queryDict(DICT_ID::ENTRUST_STATUS, ENTRUST_STATUS::FAIL) //废单
        || strEntrustStatus == m_pContext->queryDict(DICT_ID::ENTRUST_STATUS, ENTRUST_STATUS::APPROVE_REJECT) //审批拒绝
        ) && bFail)
    {
        bHide = false;
    }

    return bHide;
}

void DlgMain::onNewWithdraw(OrderList* pOrderList)
{
    for (int i = 0; i < pOrderList->size(); i++)
    {
        Order* pOrder = (*pOrderList)[i];

        //更新待处理委托列表
        qDebug() << "DlgMain::onNewWithdraw, " << pOrder->toQString().toUtf8().data();

        int row = 0, col = 0;
        ui.tblUntreatedList->insertRow(row);

        QTableWidgetItem *check = new QTableWidgetItem();
        check->setCheckState(Qt::Checked);
        ui.tblUntreatedList->setItem(row, col++, check); //插入复选框

        ui.tblUntreatedList->setItem(row, col++, new QTableWidgetItem(QString("%1").arg(pOrder->iInternalId)));
        ui.tblUntreatedList->setItem(row, col++, new QTableWidgetItem(pOrder->strAccountCode));
        ui.tblUntreatedList->setItem(row, col++, new QTableWidgetItem(m_pContext->queryDict(DICT_ID::ACCOUNT_CODE, pOrder->strAccountCode)));
        ui.tblUntreatedList->setItem(row, col++, new QTableWidgetItem(pOrder->strCombiNo));
        ui.tblUntreatedList->setItem(row, col++, new QTableWidgetItem(m_pContext->queryDict(DICT_ID::COMBI_NO, pOrder->strCombiNo)));
        ui.tblUntreatedList->setItem(row, col++, new QTableWidgetItem(m_pContext->queryDict(DICT_ID::MARKET_NO, pOrder->strMarketNo)));
        ui.tblUntreatedList->setItem(row, col++, new QTableWidgetItem(pOrder->strStockCode));
        ui.tblUntreatedList->setItem(row, col++, new QTableWidgetItem(pOrder->strStockName));
        ui.tblUntreatedList->setItem(row, col++, new QTableWidgetItem("-"));
        ui.tblUntreatedList->setItem(row, col++, new QTableWidgetItem("-"));
        ui.tblUntreatedList->setItem(row, col++, new QTableWidgetItem("-"));
        ui.tblUntreatedList->setItem(row, col++, new QTableWidgetItem("-"));
        ui.tblUntreatedList->setItem(row, col++, new QTableWidgetItem("-"));
        ui.tblUntreatedList->setItem(row, col++, new QTableWidgetItem(QString(QStringLiteral("撤单"))));
        ui.tblUntreatedList->setItem(row, col++, new QTableWidgetItem(QString("%1").arg(pOrder->iExtAccessSystemId)));
        ui.tblUntreatedList->setItem(row, col++, new QTableWidgetItem(QString("%1").arg(pOrder->iEntrustNo)));//原委托序号

        m_untreatedOrderMap.insert(pOrder->iInternalId, pOrder);
    }

    SAFE_DELETE(pOrderList);
}

//void DlgMain::onWithdrawResult(Order* pOrder)
//{
//
//}

void DlgMain::onClickCheckSelectEntrustList()
{
    bool bUndeal = ui.chkUndeal->isChecked();
    bool bDeal = ui.chkDeal->isChecked();
    bool bWithdraw = ui.chkWithdraw->isChecked();
    bool bFail = ui.chkFail->isChecked();

    for (int i = 0; i < ui.tblEntrustList->rowCount(); i++)
    {
        ui.tblEntrustList->setRowHidden(i, isEntrustHidden(i, bUndeal, bDeal, bWithdraw, bFail));
    }
}

void DlgMain::updateEntrustList(const Order* pOrder)
{
    int row = -1;
    int iColumnCount = ui.tblEntrustList->columnCount();
    for (int i = 0; i < ui.tblEntrustList->rowCount(); i++)
    {
        ////先根据 委托序号 进行匹配
        //if (pOrder->iEntrustNo > 0
        //    && ui.tblEntrustList->item(i, 2)->text().toInt() == pOrder->iEntrustNo)
        //{
        //    row = i;
        //    break;
        //}
        ////TODO 再根据 “发送句柄+请求序号” 进行匹配（篮子委托的发送句柄都相同，request_order不同）
        //else if (pOrder->iEntrustNo == 0
        //    && ui.tblEntrustList->item(i, iColumnCount - 1)->text().toInt() == pOrder->hSendHandle
        //    && ui.tblEntrustList->item(i, iColumnCount - 1)->text().toInt() == pOrder->iRequestOrder)
        //{
        //    row = i;
        //    break;
        //}

        if (pOrder->iInternalId == ui.tblEntrustList->item(i, m_tblEntrustListColMap[TBL_ENTRUSTLIST_COL::NO])->text().toInt())
        {
            row = i;
            break;
        }
    }

    if (row < 0) //未找到委托，新增
    {
        row = 0;
        ui.tblEntrustList->insertRow(row);
    }

    int col = 0;

    ui.tblEntrustList->setItem(row, col++, new QTableWidgetItem(QString("%1").arg(pOrder->iInternalId)));//主键
    if (pOrder->dtBusinessTime.toSecsSinceEpoch() > 0)
    {
        ui.tblEntrustList->setItem(row, col++, new QTableWidgetItem(pOrder->dtBusinessTime.toString("yyyy-MM-dd")));
        ui.tblEntrustList->setItem(row, col++, new QTableWidgetItem(pOrder->dtBusinessTime.toString("hh:mm:ss")));
    }
    else
    {
        ui.tblEntrustList->setItem(row, col++, new QTableWidgetItem(pOrder->dtEntrustTime.toString("yyyy-MM-dd")));
        ui.tblEntrustList->setItem(row, col++, new QTableWidgetItem(pOrder->dtEntrustTime.toString("hh:mm:ss")));
    }
    
    ui.tblEntrustList->setItem(row, col++, new QTableWidgetItem(QString("%1").arg(pOrder->iEntrustNo)));//key
    
    QTableWidgetItem* pItemEntrustStatus = new QTableWidgetItem(m_pContext->queryDict(DICT_ID::ENTRUST_STATUS, pOrder->strEntrustStatus));
    if (ENTRUST_STATUS::FAIL == pOrder->strEntrustStatus)
    {
        pItemEntrustStatus->setTextColor(Qt::red);
    }
    ui.tblEntrustList->setItem(row, col++, pItemEntrustStatus);
    
    ui.tblEntrustList->setItem(row, col++, new QTableWidgetItem(pOrder->strAccountCode));
    ui.tblEntrustList->setItem(row, col++, new QTableWidgetItem(m_pContext->queryDict(DICT_ID::ACCOUNT_CODE, pOrder->strAccountCode)));
    ui.tblEntrustList->setItem(row, col++, new QTableWidgetItem(pOrder->strCombiNo));
    ui.tblEntrustList->setItem(row, col++, new QTableWidgetItem(m_pContext->queryDict(DICT_ID::COMBI_NO, pOrder->strCombiNo)));
    ui.tblEntrustList->setItem(row, col++, new QTableWidgetItem(m_pContext->queryDict(DICT_ID::MARKET_NO, pOrder->strMarketNo)));
    ui.tblEntrustList->setItem(row, col++, new QTableWidgetItem(pOrder->strStockCode));
    ui.tblEntrustList->setItem(row, col++, new QTableWidgetItem(pOrder->strStockName));
    ui.tblEntrustList->setItem(row, col++, new QTableWidgetItem(m_pContext->queryDict(DICT_ID::ENTRUST_DIRECTION, pOrder->strEntrustDirection)));
    ui.tblEntrustList->setItem(row, col++, new QTableWidgetItem(m_pContext->queryDict(DICT_ID::PRICE_TYPE, pOrder->strPriceType)));
    ui.tblEntrustList->setItem(row, col++, new QTableWidgetItem(QString("%1").arg(pOrder->fEntrustPrice)));
    ui.tblEntrustList->setItem(row, col++, new QTableWidgetItem(QString("%1").arg(pOrder->fEntrustAmount)));
    ui.tblEntrustList->setItem(row, col++, new QTableWidgetItem(QString("%1").arg(pOrder->fEntrustPrice * pOrder->fEntrustAmount)));
    ui.tblEntrustList->setItem(row, col++, new QTableWidgetItem(QString("%1").arg(pOrder->fCancelDealAmount)));

    QTableWidgetItem* pItemFailCause = new QTableWidgetItem(pOrder->strFailCause);
    pItemFailCause->setToolTip(pOrder->strFailCause);
    pItemFailCause->setTextColor(Qt::red);
    ui.tblEntrustList->setItem(row, col++, pItemFailCause);
    //ui.tblEntrustList->resizeColumnToContents(col - 1);自适应列的宽度，但是如果文字超长，也会显示不全

    ui.tblEntrustList->setItem(row, col++, new QTableWidgetItem(QString("%1").arg(pOrder->iExtAccessSystemId)));
    ui.tblEntrustList->setItem(row, col++, new QTableWidgetItem(QString("%1").arg(pOrder->hSendHandle)));//key

    //是否显示/隐藏
    bool bUndeal = ui.chkUndeal->isChecked();
    bool bDeal = ui.chkDeal->isChecked();
    bool bWithdraw = ui.chkWithdraw->isChecked();
    bool bFail = ui.chkFail->isChecked();

    ui.tblEntrustList->setRowHidden(row, isEntrustHidden(row, bUndeal, bDeal, bWithdraw, bFail));
}

void DlgMain::updateDealList(const Order* pOrder)
{
    for (int i = 0; i < pOrder->vecDealDetail.size(); i++)
    {
        Deal* pDeal = pOrder->vecDealDetail[i];

        if (pDeal->bDisplay)
        {
            continue;
        }

        int col = 0, row = 0;
        ui.tblDealList->insertRow(row);

        ui.tblDealList->setItem(row, col++, new QTableWidgetItem(pDeal->dtDealTime.toString("yyyy-MM-dd")));
        ui.tblDealList->setItem(row, col++, new QTableWidgetItem(pDeal->dtDealTime.toString("hh:mm:ss")));
        ui.tblDealList->setItem(row, col++, new QTableWidgetItem(pDeal->strDealNo));
        ui.tblDealList->setItem(row, col++, new QTableWidgetItem(QString("%1").arg(pOrder->iEntrustNo)));//key
        ui.tblDealList->setItem(row, col++, new QTableWidgetItem(pOrder->strAccountCode));
        ui.tblDealList->setItem(row, col++, new QTableWidgetItem(m_pContext->queryDict(DICT_ID::ACCOUNT_CODE, pOrder->strAccountCode)));
        ui.tblDealList->setItem(row, col++, new QTableWidgetItem(pOrder->strCombiNo));
        ui.tblDealList->setItem(row, col++, new QTableWidgetItem(m_pContext->queryDict(DICT_ID::COMBI_NO, pOrder->strCombiNo)));
        ui.tblDealList->setItem(row, col++, new QTableWidgetItem(m_pContext->queryDict(DICT_ID::MARKET_NO, pOrder->strMarketNo)));
        ui.tblDealList->setItem(row, col++, new QTableWidgetItem(pOrder->strStockCode));
        ui.tblDealList->setItem(row, col++, new QTableWidgetItem(pOrder->strStockName));
        ui.tblDealList->setItem(row, col++, new QTableWidgetItem(m_pContext->queryDict(DICT_ID::ENTRUST_DIRECTION, pOrder->strEntrustDirection)));
        ui.tblDealList->setItem(row, col++, new QTableWidgetItem(QString("%1").arg(pDeal->fDealAmount)));
        ui.tblDealList->setItem(row, col++, new QTableWidgetItem(QString("%1").arg(pDeal->fDealPrice)));
        ui.tblDealList->setItem(row, col++, new QTableWidgetItem(QString("%1").arg(pDeal->fDealBalance)));
        ui.tblDealList->setItem(row, col++, new QTableWidgetItem(QString("%1").arg(pDeal->fDealfee)));
        ui.tblDealList->setItem(row, col++, new QTableWidgetItem(QString("%1").arg(pOrder->iExtAccessSystemId)));

        pDeal->bDisplay = true;
    }
}

void DlgMain::onRunLog(int type, QString message)
{
    qInfo() << QString("Run log:[%1]%2").arg(type).arg(message).toUtf8().data();

    QString str = QDateTime::currentDateTime().toString("hh:mm:ss.zzz") + " ";
    if (RunLog_Error == type)//red
    {
        //播放提醒音
        if (m_pMediaPlayerError)
        {
            m_pMediaPlayerError->play();
        }

        ui.chkQuickEntrust->setCheckState(Qt::Unchecked);//会自动触发槽函数

        str.append("<font color=\"#FF0000\">").append(message).append("</font>");
        str.append("<font color=\"#FF0000\">").append(QStringLiteral("     程序异常，已禁用快速下单！请检查错误后再开启")).append("</font>");
    }
    else if (RunLog_Important == type)//blue
    {
        //暂时不播放：有些不需要播放
        //if (m_pMediaPlayerImportant)//有可能还没初始化就有提示消息，比如启动时的：载入xx条委托记录
        //{
        //    m_pMediaPlayerImportant->play();
        //}
        str.append("<font color=\"#0000FF\">").append(message).append("</font>");
    }
    else
    {
        str.append(message);
    }

    ui.edtRunLog->append(str);

    QScrollBar* pScrollbar = ui.edtRunLog->verticalScrollBar();
    if (pScrollbar)
    {
        pScrollbar->setSliderPosition(pScrollbar->maximum());
    }
}

void DlgMain::onActionSetting()
{
    DlgMsgBox* pMsgBox = new DlgMsgBox;
    pMsgBox->SetMessage(DlgMsgBox::Info, QStringLiteral("提示"), QStringLiteral("暂未实现"));
    pMsgBox->exec();
}

void DlgMain::onActionAbout()
{
    DlgAbout* pDlg = new DlgAbout;
    pDlg->show();
}
