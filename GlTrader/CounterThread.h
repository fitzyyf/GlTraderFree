#pragma once

#include <QMAP>

#include "hundsun/t2sdk_interface.h"
#include "OrderMgr.h"
#include "Config.h"

class QTimer;
class Context;

class CMsgCallback;
class CPushCallback;

class CounterThread : public QThread
{
    Q_OBJECT

public:
    CounterThread(ConfigData configData, Context* pContext, QObject *parent = nullptr);
    virtual ~CounterThread();

    virtual void run() override;

signals:
    void sigLoginResult(int iRetCode, QString strErrMsg);
    void sigLogining(QString strMsg);
    void sigLogoutResult(int iRetCode, QString strErrMsg);

    void sigEntrustResult(Order* pOrder);

    void sigQueryOrderByEntrustNoResult(OrderList* pOrderList);

    void sigNewEntrustHandled(int iOrderInternalId);
    void sigNewWithdrawHandled(int iOrderInternalId);

    void sigRunLog(int type, QString message);

    //void sigKillReconnectTimer();

private slots:
    void onLogin(QString strUsername, QString strPassword);
    void onLogout();
    void sendHeartBeat();

    void onMsgCallback(CConnectionInterface *lpConnection, int hSend, IBizMessage* lpMsg);
    void onPushCallback(CSubscribeInterface* lpSub, int subscribeIndex, QByteArray buf);

    void onQueryFund();
    void onQueryCombi();

    //void onEntrust(Order* pOrder);
    void onEntrustCombi(OrderList* pOrderList);
    void onWithdraw(OrderList* pOrderList);

    void onQueryOrderByEntrustNo(OrderList* pOrderList);

    void onConnCreated(quint32 ptr);
    void onConnClosed(quint32 ptr);

    //void onKillReconnectTimer();

private:

    QString getErrMsg();
    void setErrMsg(const QString strErrMsg);

    int connectServer();
    void disconnectServer();

    void initConnConfig();
    void releaseConnConfig();

    int connectUfx();
    int connectMc();

    void disconnectUfx();
    void disconnectMc();

    int subscribe();
    int doLogin();

    void startHeartBeat();
    void stopHeartBeat();

private:
    ConfigData m_ConfigData;

    Context* m_pContext;
    QTimer* m_pHeartBeatTimer;
    QTimer* m_pExitTimer;

    //QTimer* m_pReconnectUfxTimer;
    //QTimer* m_pReconnectMcTimer;
    //QTimer* m_pReconnectTimer;

    CConfigInterface* m_lpConnConfig;
    CConnectionInterface* m_pConnMsg;
    CConnectionInterface* m_pConnPush;

    CSubscribeInterface*  m_pSubscribe;

    CMsgCallback* m_pMsgCallback;//异步发送消息后的回调
    CPushCallback* m_pPushCallback;//服务端主动推送消息

    QString m_strErrMsg;
    int m_iPacketId;
    QMap<int, CSubscribeParamInterface*> m_AllSubscribeParam;

    bool m_bRefreshAccountCodeDict; //是否刷新了AccountCode字典
    bool m_bRefreshCombiNoDict;     //是否刷新了CombiNo字典

    //bool m_bConnectedUfx;//是否已连接
    //bool m_bConnectedMc; //是否已连接
    //bool m_bConnected;   //是否已连接
    //bool m_bConnecting;  //是否正在建立连接

    bool m_bLogout;//用户是否主动退出

    bool m_bFirstLogin;//首次登录
};

