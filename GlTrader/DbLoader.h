#pragma once

class DbHelper;

class Context;

class DbLoader
{
public:
    DbLoader();
    ~DbLoader();

    static DbLoader* getInstance();
    static void destoryInstance();

    void setContext(Context* pContext);
    bool initialize(QString& strErrMsg);

    //QString queryDict(int iDictId, const QString& strItem);
    //QString queryRunConfig(const QString& strItem);

private:
    bool createDbDir();
    bool initCfgDb();
    bool initInDb();
    bool initOutDb();

    bool initDict();

    bool createInDbTable();
    bool createOutDbTable();

    bool loadOutDb();
    bool loadInDb();

private:
    static DbLoader* m_pInstance;

    Context* m_pContext;

    DbHelper* m_pCfgDbHelper;
    DbHelper* m_pInDbHelper;
    DbHelper* m_pOutDbHelper;

    //QMap<int, QMap<QString, QString>*> m_dictMap;
    //QMap<QString, QString> m_runConfigMap;
};

