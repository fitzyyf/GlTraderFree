#pragma once

#include "Constants.h"
#include "FuncHandler/FuncHandler.h"

class WithdrawHandler;
class Order;

typedef int (WithdrawHandler::*PtrPushMsgFunc)(int, IF2UnPacker*, Order*, QMap<QString, QVariant>&);

class WithdrawHandler : public FuncHandler
{
public:
    WithdrawHandler(int iFuncNo);
    WithdrawHandler(const QString& strMsgType);
    virtual ~WithdrawHandler();

protected:
    virtual void initPacketFieldDef() override;

    virtual void doPack(IBizMessage* lpBizMessage, IF2Packer* lpPacker, QMap<QString, QVariant>& params = QMap<QString, QVariant>()) override;
    
    //处理应答消息
    virtual int doUnpack(IBizMessage* lpBizMessage, IF2UnPacker* pUnPacker, QMap<QString, QVariant>& output = QMap<QString, QVariant>()) override;
    
    //处理推送消息总入口
    virtual int doUnpack(int iSubsIndex, const char* pMsgType, IF2UnPacker* lpUnPack, QMap<QString, QVariant>& output = QMap<QString, QVariant>());

    //处理各类推送消息
    //d 委托撤单
    virtual int doUnpackEntrustWithdrawPush(int iSubsIndex, IF2UnPacker* lpUnPack, Order* pOrder, QMap<QString, QVariant>& output = QMap<QString, QVariant>());
    //e 委托撤成
    virtual int doUnpackEntrustWithdrawDealPush(int iSubsIndex, IF2UnPacker* lpUnPack, Order* pOrder, QMap<QString, QVariant>& output = QMap<QString, QVariant>());
    //f 委托撤废
    virtual int doUnpackEntrustWithdrawFailPush(int iSubsIndex, IF2UnPacker* lpUnPack, Order* pOrder, QMap<QString, QVariant>& output = QMap<QString, QVariant>());

private:
    QMap<QString, PtrPushMsgFunc> m_pushMsgFuncMap;
};


REGISTER_FUNC_HANDLER(FUNC_NO::ENTRUST_WITHDRAW, WithdrawHandler);

REGISTER_PUSH_HANDLER(1, PUSH_MSG_TYPE::ENTRUST_WITHDRAW, WithdrawHandler);
REGISTER_PUSH_HANDLER(2, PUSH_MSG_TYPE::ENTRUST_WITHDRAW_DEAL, WithdrawHandler);
REGISTER_PUSH_HANDLER(3, PUSH_MSG_TYPE::ENTRUST_WITHDRAW_FAIL, WithdrawHandler);
