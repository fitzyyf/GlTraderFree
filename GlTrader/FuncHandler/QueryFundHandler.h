#pragma once

#include "Constants.h"
#include "FuncHandler/FuncHandler.h"

class QueryFundHandler : public FuncHandler
{
public:
    QueryFundHandler(int iFuncNo);
    virtual ~QueryFundHandler();

    virtual void initPacketFieldDef() override;

    virtual void doPack(IBizMessage* lpBizMessage, IF2Packer* lpPacker, QMap<QString, QVariant>& params = QMap<QString, QVariant>()) override;
    virtual int doUnpack(IBizMessage* lpBizMessage, IF2UnPacker* pUnPacker, QMap<QString, QVariant>& output = QMap<QString, QVariant>()) override;

};

REGISTER_FUNC_HANDLER(FUNC_NO::FUND_QUERY, QueryFundHandler);
