#pragma once

#include "Constants.h"
#include "FuncHandler/FuncHandler.h"

class EntrustCombiHandler : public FuncHandler
{
public:
    EntrustCombiHandler(int iFuncNo);
    virtual ~EntrustCombiHandler();

protected:
    virtual void initPacketFieldDef() override;

    virtual void doPack(IBizMessage* lpBizMessage, IF2Packer* lpPacker, QMap<QString, QVariant>& params = QMap<QString, QVariant>()) override;
    
    //处理应答消息
    virtual int doUnpack(IBizMessage* lpBizMessage, IF2UnPacker* pUnPacker, QMap<QString, QVariant>& output = QMap<QString, QVariant>()) override;    
};


REGISTER_FUNC_HANDLER(FUNC_NO::ENTRUST_COMBI, EntrustCombiHandler);
