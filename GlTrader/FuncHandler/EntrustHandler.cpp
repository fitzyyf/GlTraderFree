#include "stdafx.h"
#include "EntrustHandler.h"
#include "Context.h"
#include "Utils.h"
#include "OrderMgr.h"

EntrustHandler::EntrustHandler(int iFuncNo)
    : FuncHandler(iFuncNo)
{

}

EntrustHandler::EntrustHandler(const QString& strMsgType)
    : FuncHandler(strMsgType)
{
    m_pushMsgFuncMap.insert(PUSH_MSG_TYPE::ENTRUST,      &EntrustHandler::doUnpackEntrustPush);
    m_pushMsgFuncMap.insert(PUSH_MSG_TYPE::ENTRUST_CONFIRM, &EntrustHandler::doUnpackEntrustConfirmPush);
    m_pushMsgFuncMap.insert(PUSH_MSG_TYPE::ENTRUST_FAIL, &EntrustHandler::doUnpackEntrustFailPush);
    m_pushMsgFuncMap.insert(PUSH_MSG_TYPE::ENTRUST_DEAL, &EntrustHandler::doUnpackEntrustDealPush);
}

EntrustHandler::~EntrustHandler()
{
}

void EntrustHandler::initPacketFieldDef()
{
    m_vPacketFieldDef.push_back(new PacketFieldDef("user_token", 'S', 512, 0));
    m_vPacketFieldDef.push_back(new PacketFieldDef("batch_no", 'I', 8, 0));
    m_vPacketFieldDef.push_back(new PacketFieldDef("account_code", 'S', 32, 0));
    m_vPacketFieldDef.push_back(new PacketFieldDef("asset_no", 'S', 16, 0));
    m_vPacketFieldDef.push_back(new PacketFieldDef("combi_no", 'S', 16, 0));
    m_vPacketFieldDef.push_back(new PacketFieldDef("market_no", 'S', 3, 0));
    m_vPacketFieldDef.push_back(new PacketFieldDef("stock_code", 'S', 16, 0));
    m_vPacketFieldDef.push_back(new PacketFieldDef("entrust_direction", 'S', 1, 0));
    m_vPacketFieldDef.push_back(new PacketFieldDef("price_type", 'S', 1, 0));
    m_vPacketFieldDef.push_back(new PacketFieldDef("entrust_price", 'F', 9, 3));
    m_vPacketFieldDef.push_back(new PacketFieldDef("entrust_amount", 'F', 16, 2));
    m_vPacketFieldDef.push_back(new PacketFieldDef("extsystem_id", 'I', 8, 0));
}

void EntrustHandler::doPack(IBizMessage* lpBizMessage, IF2Packer* lpPacker, QMap<QString, QVariant>& params)
{
    lpBizMessage->SetFunction(FuncHandler::m_iFuncNo);
    lpBizMessage->SetPacketType(REQUEST_PACKET);
    lpBizMessage->SetSystemNo(CONSTANT::SYSTEM_NO);

    Order* pOrder = params[PARAMETER::ORDER_IN].value<Order*>();

    pOrder->iExtSystemId = Order::getAutoIncr();
    m_pContext->addOrder(pOrder);

    qDebug() << "New order request, " << pOrder->toQString().toUtf8().data();

    QVector<QVariant> valueList;
    valueList.push_back(m_pContext->getUserToken());
    valueList.push_back(pOrder->iBatchNo);
    valueList.push_back(pOrder->strAccountCode);
    valueList.push_back(pOrder->strAssetNo);
    valueList.push_back(pOrder->strCombiNo);
    valueList.push_back(pOrder->strMarketNo);
    valueList.push_back(pOrder->strStockCode);
    valueList.push_back(pOrder->strEntrustDirection);
    valueList.push_back(pOrder->strPriceType);
    valueList.push_back(pOrder->fEntrustPrice);
    valueList.push_back(pOrder->fEntrustAmount);
    valueList.push_back(pOrder->iExtSystemId);
    FuncHandler::makePacket(lpPacker, valueList);

    lpBizMessage->SetContent(lpPacker->GetPackBuf(), lpPacker->GetPackLen());
    lpBizMessage->SetPacketId(m_pContext->getNewPacketId());
}

 //处理应答消息
int EntrustHandler::doUnpack(IBizMessage* lpBizMessage, IF2UnPacker* pUnPacker, QMap<QString, QVariant>& output)
{
    if (pUnPacker->GetDatasetCount() <= 1)
    {
        qCritical() << "Unpack response failed: no data";
        return ERROR_CODE::FAIL;
    }

    //解析委托应答
    pUnPacker->SetCurrentDatasetByIndex(1);
    pUnPacker->First();

    //batch_no	N8	委托批号
    //entrust_no	N8	委托序号
    //extsystem_id	N8	第三方系统自定义号
    //entrust_fail_code	N8	委托失败代码		参见数据字典17。
    //fail_cause	C256	失败原因
    //risk_serial_no	N8	风控判断流水号		用于关联风控信息包中的风控信息条目

    int iExtSystemId = pUnPacker->GetInt("extsystem_id");

    Order* pOrder = m_pContext->getOrder(iExtSystemId);
    if (pOrder == nullptr)
    {
        qCritical() << "Can't find entrust, iExtSystemId = " << iExtSystemId;
        return ERROR_CODE::FAIL;
    }

    pOrder->iBatchNo = pUnPacker->GetInt("batch_no");
    pOrder->iEntrustNo = pUnPacker->GetInt("entrust_no");
    pOrder->iFailCode = pUnPacker->GetInt("entrust_fail_code");
    pOrder->strFailCause = Utils::toQString(pUnPacker->GetStr("fail_cause"));
    int iRiskSerialNo = pUnPacker->GetInt("risk_serial_no");

    qDebug() << "New order response, " << pOrder->toQString().toUtf8().data();

    //TODO 解析风控信息
    if (pUnPacker->GetDatasetCount() >= 3)
    {
        pUnPacker->SetCurrentDatasetByIndex(2);
        pUnPacker->First();
    }

    QVariant var;
    var.setValue(pOrder);
    output.insert(PARAMETER::ORDER_OUT, var);

    return ERROR_CODE::SUCCESS;
}

//处理推送消息
int EntrustHandler::doUnpack(int iSubsIndex, const char* pMsgType, IF2UnPacker* lpUnPack, QMap<QString, QVariant>& output)
{
    //根据extsystem_id查找订单
    int iExtSystemId = lpUnPack->GetInt("extsystem_id");
    Order* pOrder = m_pContext->getOrder(iExtSystemId);
    if (pOrder == nullptr)
    {
        qWarning() << "Can't find entrust, iExtSystemId = " << iExtSystemId << ", igonre it";
        return ERROR_CODE::FAIL;
    }

    //根据消息类型查找处理器
    auto it = m_pushMsgFuncMap.find(pMsgType);
    if (it == m_pushMsgFuncMap.end())
    {
        qWarning() << "Cant't find function to process push message: " << pMsgType;
        return ERROR_CODE::FAIL;
    }
    else
    {
        PtrPushMsgFunc func = it.value();
        return (this->*func)(iSubsIndex, lpUnPack, pOrder, output);
    }



    return ERROR_CODE::SUCCESS;
}

//a 委托下达
int EntrustHandler::doUnpackEntrustPush(int iSubsIndex, IF2UnPacker* lpUnPack, Order* pOrder, QMap<QString, QVariant>& output)
{     
    //注意：有可能先收到主推消息，再收到应答消息！！！！
    int iBusinessDate = lpUnPack->GetInt("business_date");//YYYYMMDD
    int iBusinessTime = lpUnPack->GetInt("business_time");//HHMMSS
    const char* pReportNo = lpUnPack->GetStr("report_no");
    const char* pEntrustStatus = lpUnPack->GetStr("entrust_status");
    int iEntrustNo = lpUnPack->GetInt("entrust_no");

    QString strDateTime = QString("%1%2").arg(iBusinessDate).arg(iBusinessTime, 6, 10, (QLatin1Char)('0'));
    pOrder->dtBusinessTime = QDateTime::fromString(strDateTime, "yyyyMMddhhmmss");
    pOrder->iEntrustNo = iEntrustNo;
    pOrder->strReportNo = Utils::toQString(pReportNo);

    //可能委托确认、成交消息、废单先到，委托下达消息后到
    if (pOrder->strEntrustStatus == QString(ENTRUST_STATUS::UNACK)
        || pOrder->strEntrustStatus == QString(ENTRUST_STATUS::UNREPORT)
        || pOrder->strEntrustStatus == QString(ENTRUST_STATUS::TO_REPORT)
        || pOrder->strEntrustStatus == QString(ENTRUST_STATUS::REPORTING)
        )
    {
        pOrder->strEntrustStatus = Utils::toQString(pEntrustStatus);//未报
    }
    else
    {
        qWarning() << QString("Invalid entrust status, old = %1(%2), new = %3(%4)")
            .arg(pOrder->strEntrustStatus)
            .arg(m_pContext->queryDict(DICT_ID::ENTRUST_STATUS, pOrder->strEntrustStatus))
            .arg(pEntrustStatus)
            .arg(m_pContext->queryDict(DICT_ID::ENTRUST_STATUS, pEntrustStatus));
    }

    qDebug() << "Entrust(a) push, " << pOrder->toQString().toUtf8().data();

    QVariant var;
    var.setValue(pOrder);
    output.insert(PARAMETER::ORDER_OUT, var);

    return ERROR_CODE::SUCCESS;
}

//b 委托确认
int EntrustHandler::doUnpackEntrustConfirmPush(int iSubsIndex, IF2UnPacker* lpUnPack, Order* pOrder, QMap<QString, QVariant>& output)
{
    const char* strConfirmNo = lpUnPack->GetStr("confirm_no");
    const char* pEntrustStatus = lpUnPack->GetStr("entrust_status");
    int iEntrustNo = lpUnPack->GetInt("entrust_no");

    pOrder->iEntrustNo = iEntrustNo;
    pOrder->strConfirmNo = Utils::toQString(strConfirmNo);

    //可能成交消息先到，委托确认消息后到
    if (pOrder->strEntrustStatus == QString(ENTRUST_STATUS::UNACK)
        || pOrder->strEntrustStatus == QString(ENTRUST_STATUS::UNREPORT)
        || pOrder->strEntrustStatus == QString(ENTRUST_STATUS::TO_REPORT)
        || pOrder->strEntrustStatus == QString(ENTRUST_STATUS::REPORTING)
        || pOrder->strEntrustStatus == QString(ENTRUST_STATUS::REPORTED)
        )
    {
        pOrder->strEntrustStatus = Utils::toQString(pEntrustStatus);//已报
    }
    else
    {
        qWarning() << QString("Invalid entrust status, old = %1(%2), new = %3(%4)")
            .arg(pOrder->strEntrustStatus)
            .arg(m_pContext->queryDict(DICT_ID::ENTRUST_STATUS, pOrder->strEntrustStatus))
            .arg(pEntrustStatus)
            .arg(m_pContext->queryDict(DICT_ID::ENTRUST_STATUS, pEntrustStatus));
    }

    qDebug() << "Entrust confirm(b) push, " << pOrder->toQString().toUtf8().data();

    QVariant var;
    var.setValue(pOrder);
    output.insert(PARAMETER::ORDER_OUT, var);

    return ERROR_CODE::SUCCESS;
}

//c 委托废单
int EntrustHandler::doUnpackEntrustFailPush(int iSubsIndex, IF2UnPacker* lpUnPack, Order* pOrder, QMap<QString, QVariant>& output)
{
    const char* pEntrustStatus = lpUnPack->GetStr("entrust_status");
    const char* pRevokeCause = lpUnPack->GetStr("revoke_cause");
    int iEntrustNo = lpUnPack->GetInt("entrust_no");

    pOrder->iEntrustNo = iEntrustNo;
    pOrder->strEntrustStatus = Utils::toQString(pEntrustStatus);
    pOrder->strFailCause = Utils::toQString(pRevokeCause);

    qDebug() << "Entrust fail(c) push, " << pOrder->toQString().toUtf8().data();

    QVariant var;
    var.setValue(pOrder);
    output.insert(PARAMETER::ORDER_OUT, var);

    return ERROR_CODE::SUCCESS;
}

//g 委托成交
int EntrustHandler::doUnpackEntrustDealPush(int iSubsIndex, IF2UnPacker* lpUnPack, Order* pOrder, QMap<QString, QVariant>& output)
{
    const char* pEntrustStatus = lpUnPack->GetStr("entrust_status");
    int iDealDate = lpUnPack->GetInt("deal_date");
    int iDealTime = lpUnPack->GetInt("deal_time");
    const char* pDealNo = lpUnPack->GetStr("deal_no");
    float fDealAmount = (float)lpUnPack->GetDouble("deal_amount");
    float fDealPrice = (float)lpUnPack->GetDouble("deal_price");
    float fDealBalance = (float)lpUnPack->GetDouble("deal_balance");
    float fDealFee = (float)lpUnPack->GetDouble("deal_fee");

    pOrder->strEntrustStatus = Utils::toQString(pEntrustStatus);

    //判断是不是重复的成交序号
    QString strDealNo = Utils::toQString(pDealNo);
    for (int i = 0; i < pOrder->vecDealDetail.size(); i++)
    {
        if (pOrder->vecDealDetail[i]->strDealNo == strDealNo)
        {
            qWarning() << "Receive duplicate deal push message, ignore, strDealNo =" << strDealNo;

            QVariant var;
            var.setValue(nullptr);
            output.insert(PARAMETER::ORDER_OUT, var);

            return ERROR_CODE::SUCCESS;
        }
    }

    Deal* pDeal = new Deal;
    QString strDateTime = QString("%1%2").arg(iDealDate).arg(iDealTime, 6, 10, (QLatin1Char)('0'));
    pDeal->dtDealTime = QDateTime::fromString(strDateTime, "yyyyMMddhhmmss");
    pDeal->strDealNo = strDealNo;
    pDeal->fDealAmount = fDealAmount;
    pDeal->fDealPrice = fDealPrice;
    pDeal->fDealBalance = fDealBalance;
    pDeal->fDealfee = fDealFee;

    pOrder->vecDealDetail.push_back(pDeal);

    qDebug() << "Entrust deal(g) push, " << pOrder->toQString().toUtf8().data();

    QVariant var;
    var.setValue(pOrder);
    output.insert(PARAMETER::ORDER_OUT, var);

    return ERROR_CODE::SUCCESS;
}
