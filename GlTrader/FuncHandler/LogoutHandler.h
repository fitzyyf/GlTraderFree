#pragma once

#include "Constants.h"
#include "FuncHandler/FuncHandler.h"

class LogoutHandler : public FuncHandler
{
public:
    LogoutHandler(int iFuncNo);
    virtual ~LogoutHandler();

    virtual void initPacketFieldDef() override;

    virtual void doPack(IBizMessage* lpBizMessage, IF2Packer* lpPacker, QMap<QString, QVariant>& params = QMap<QString, QVariant>()) override;
    virtual int doUnpack(IBizMessage* lpBizMessage, IF2UnPacker* pUnPacker, QMap<QString, QVariant>& output = QMap<QString, QVariant>()) override;

};

REGISTER_FUNC_HANDLER(FUNC_NO::LOGOUT, LogoutHandler);
