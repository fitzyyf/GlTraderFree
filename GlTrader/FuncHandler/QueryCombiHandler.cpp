#include "stdafx.h"
#include "QueryCombiHandler.h"
#include "Context.h"
#include "Utils.h"

QueryCombiHandler::QueryCombiHandler(int iFuncNo)
    : FuncHandler(iFuncNo)
{

}


QueryCombiHandler::~QueryCombiHandler()
{
}

void QueryCombiHandler::initPacketFieldDef()
{
    //user_token	C512	用户口令	Y
    //account_code	C32	账户编号	N
    m_vPacketFieldDef.push_back(new PacketFieldDef("user_token", 'S', 512, 0));
}

void QueryCombiHandler::doPack(IBizMessage* lpBizMessage, IF2Packer* lpPacker, QMap<QString, QVariant>& params)
{
    qDebug() << "QueryCombi request";

    lpBizMessage->SetFunction(FuncHandler::m_iFuncNo);
    lpBizMessage->SetPacketType(REQUEST_PACKET);
    lpBizMessage->SetSystemNo(CONSTANT::SYSTEM_NO);

    QVector<QVariant> valueList;
    valueList.push_back(m_pContext->getUserToken());
    FuncHandler::makePacket(lpPacker, valueList);

    lpBizMessage->SetContent(lpPacker->GetPackBuf(), lpPacker->GetPackLen());
    lpBizMessage->SetPacketId(m_pContext->getNewPacketId());
}

int QueryCombiHandler::doUnpack(IBizMessage* lpBizMessage, IF2UnPacker* pUnPacker, QMap<QString, QVariant>& output)
{
    if (pUnPacker->GetDatasetCount() <= 1)
    {
        qCritical() << "Unpack response failed: no data";
        return ERROR_CODE::FAIL;
    }

    //解析委托应答
    pUnPacker->SetCurrentDatasetByIndex(1);
    pUnPacker->First();

    //account_code	C32	账户编号
    //asset_no	C16	资产单元编号
    //combi_no	C16	组合编号
    //combi_name	C64	组合名称
    //market_no_list	C256	允许的交易市场		见数据词典4，为空表示可支持全部市场
    //futu_invest_type	C1	期货投资类型		参见数据字典5（期货部分），为空表示不能投资期货。
    //entrust_direction_list	C256	允许的委托方向		参见数据词典6。
    for (int i = 0; i < pUnPacker->GetRowCount(); i++)
    {
        pUnPacker->Go(i + 1); //取值范围[1, GetRowCount()]，fuck！！！
        const char* pCombiNo = pUnPacker->GetStr("combi_no");
        const char* pCombiName = pUnPacker->GetStr("combi_name");

        m_pContext->addDict(DICT_ID::COMBI_NO, pCombiNo, Utils::toQString(pCombiName));
    }

    return ERROR_CODE::SUCCESS;
}
