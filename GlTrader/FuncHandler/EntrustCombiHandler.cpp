#include "stdafx.h"
#include "EntrustCombiHandler.h"
#include "Context.h"
#include "Utils.h"
#include "OrderMgr.h"

EntrustCombiHandler::EntrustCombiHandler(int iFuncNo)
    : FuncHandler(iFuncNo)
{

}

EntrustCombiHandler::~EntrustCombiHandler()
{
}

void EntrustCombiHandler::initPacketFieldDef()
{
    m_vPacketFieldDef.push_back(new PacketFieldDef("user_token", 'S', 512, 0));
    m_vPacketFieldDef.push_back(new PacketFieldDef("batch_no", 'I', 8, 0));
    m_vPacketFieldDef.push_back(new PacketFieldDef("account_code", 'S', 32, 0));
    m_vPacketFieldDef.push_back(new PacketFieldDef("asset_no", 'S', 16, 0));
    m_vPacketFieldDef.push_back(new PacketFieldDef("combi_no", 'S', 16, 0));
    m_vPacketFieldDef.push_back(new PacketFieldDef("market_no", 'S', 3, 0));
    m_vPacketFieldDef.push_back(new PacketFieldDef("stock_code", 'S', 16, 0));
    m_vPacketFieldDef.push_back(new PacketFieldDef("entrust_direction", 'S', 1, 0));
    m_vPacketFieldDef.push_back(new PacketFieldDef("price_type", 'S', 1, 0));
    m_vPacketFieldDef.push_back(new PacketFieldDef("entrust_price", 'F', 9, 3));
    m_vPacketFieldDef.push_back(new PacketFieldDef("entrust_amount", 'F', 16, 2));
    m_vPacketFieldDef.push_back(new PacketFieldDef("extsystem_id", 'I', 8, 0));
}

void EntrustCombiHandler::doPack(IBizMessage* lpBizMessage, IF2Packer* lpPacker, QMap<QString, QVariant>& params)
{
    lpBizMessage->SetFunction(FuncHandler::m_iFuncNo);
    lpBizMessage->SetPacketType(REQUEST_PACKET);
    lpBizMessage->SetSystemNo(CONSTANT::SYSTEM_NO);

    QVector<QVariant> valueList;
    OrderList* pOrderList = params[PARAMETER::ORDER_LIST_IN].value<OrderList*>();
    for (int i = 0; i < pOrderList->size(); i++)
    {
        Order* pOrder = (*pOrderList)[i];

        pOrder->iExtSystemId = Order::getAutoIncr();
        pOrder->iRequestOrder = i + 1;
        m_pContext->addOrder(pOrder);

        qDebug() << "New order request, " << pOrder->toQString().toUtf8().data();

        valueList.push_back(m_pContext->getUserToken());
        valueList.push_back(pOrder->iBatchNo);
        valueList.push_back(pOrder->strAccountCode);
        valueList.push_back(pOrder->strAssetNo);
        valueList.push_back(pOrder->strCombiNo);
        valueList.push_back(pOrder->strMarketNo);
        valueList.push_back(pOrder->strStockCode);
        valueList.push_back(pOrder->strEntrustDirection);
        valueList.push_back(pOrder->strPriceType);
        valueList.push_back(pOrder->fEntrustPrice);
        valueList.push_back(pOrder->fEntrustAmount);
        valueList.push_back(pOrder->iExtSystemId);
    }
    FuncHandler::makePacket(lpPacker, valueList);

    lpBizMessage->SetContent(lpPacker->GetPackBuf(), lpPacker->GetPackLen());
    lpBizMessage->SetPacketId(m_pContext->getNewPacketId());
}

 //处理应答消息
int EntrustCombiHandler::doUnpack(IBizMessage* lpBizMessage, IF2UnPacker* pUnPacker, QMap<QString, QVariant>& output)
{
    if (pUnPacker->GetDatasetCount() <= 1)
    {
        qCritical() << "Unpack response failed: no data";
        return ERROR_CODE::FAIL;
    }

    //解析委托应答
    pUnPacker->SetCurrentDatasetByIndex(1);
    //pUnPacker->First();

    //batch_no	N8	委托批号
    //entrust_no	N8	委托序号
    //extsystem_id	N8	第三方系统自定义号
    //entrust_fail_code	N8	委托失败代码		参见数据字典17。
    //fail_cause	C256	失败原因
    //risk_serial_no	N8	风控判断流水号		用于关联风控信息包中的风控信息条目
    SAFE_NEW(OrderList, pOrderList);
    QMap<int, Order*> m_riskOrderMap;
    //OrderList* pOrderList = new OrderList;
    for (int i = 0; i < pUnPacker->GetRowCount(); i++)
    {
        pUnPacker->Go(i + 1); //取值范围[1, GetRowCount()]，fuck！！！

        int iExtSystemId = pUnPacker->GetInt("extsystem_id");

        Order* pOrder = m_pContext->getOrder(iExtSystemId);
        if (pOrder == nullptr)
        {
            qCritical() << "Can't find entrust, iExtSystemId = " << iExtSystemId;
            return ERROR_CODE::FAIL;
        }

        pOrder->iBatchNo = pUnPacker->GetInt("batch_no");
        pOrder->iEntrustNo = pUnPacker->GetInt("entrust_no");
        pOrder->iFailCode = pUnPacker->GetInt("entrust_fail_code");
        pOrder->strFailCause = Utils::toQString(pUnPacker->GetStr("fail_cause"));
        pOrder->iRiskSerialNo = pUnPacker->GetInt("risk_serial_no");

        if (pOrder->iRiskSerialNo > 0)
        {
            m_riskOrderMap.insert(pOrder->iRiskSerialNo, pOrder);
        }

        pOrderList->push_back(pOrder);
        qDebug() << "New order response, " << pOrder->toQString().toUtf8().data();
    }


    //TODO 解析风控信息
/*
"ErrorCode", "ErrorMsg", "MsgDetail", "DataCount",
"3001", "触犯风控", "", "3",

"batch_no", "entrust_no", "extsystem_id", "entrust_fail_code", "fail_cause", "risk_serial_no", "request_order",
"0", "0", "46570005", "1", "触犯风控", "272967", "1",
"0", "0", "46570006", "1", "触犯风控", "272968", "2",
"0", "0", "46570007", "1", "触犯风控", "272969", "3",

"risk_serial_no", "market_no", "stock_code", "entrust_direction", "futures_direction", "risk_no", "risk_type", "risk_summary", "risk_operation", "remark_short", "risk_threshold_value", "risk_value",
"272967", "2", "300001", "1", "", "3", "Z", "黑白名单", "2", "操作证券代码属于黑名单,无法操作", "0", "0",
"272968", "2", "300001", "1", "", "3", "Z", "黑白名单", "2", "操作证券代码属于黑名单,无法操作", "0", "0",
"272969", "2", "300001", "1", "", "3", "Z", "黑白名单", "2", "操作证券代码属于黑名单,无法操作", "0", "0",

触犯风控：[Z-黑白名单][禁止]操作证券代码属于黑名单,无法操作。风控阀值=%f，实际值=%f
*/
    if (pUnPacker->GetDatasetCount() >= 3)
    {
        pUnPacker->SetCurrentDatasetByIndex(2);
        for (int i = 0; i < pUnPacker->GetRowCount(); i++)
        {
            pUnPacker->Go(i + 1); //取值范围[1, GetRowCount()]，fuck！！！

            int iRiskSerialNo = pUnPacker->GetInt("risk_serial_no");
            auto it = m_riskOrderMap.find(iRiskSerialNo);
            if (it == m_riskOrderMap.end())
            {
                qCritical() << "Cant't find order, risk_serial_no =" << iRiskSerialNo;
            }
            else
            {
                Order* pOrder = it.value();
                pOrder->strFailCause =
                    QString("[%1][%2-%3][%4]%5")
                    .arg(pOrder->strFailCause)
                    .arg(pUnPacker->GetStr("risk_type"))
                    .arg(Utils::toQString(pUnPacker->GetStr("risk_summary")))
                    .arg(m_pContext->queryDict(DICT_ID::RISK_OPERATION, pUnPacker->GetStr("risk_operation")))
                    .arg(Utils::toQString(pUnPacker->GetStr("remark_short")));
            }
        }
    }

    QVariant var;
    var.setValue(pOrderList);
    output.insert(PARAMETER::ORDER_LIST_OUT, var);

    return ERROR_CODE::SUCCESS;
}
