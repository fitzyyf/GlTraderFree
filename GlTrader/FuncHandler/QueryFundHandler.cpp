#include "stdafx.h"
#include "QueryFundHandler.h"
#include "Context.h"
#include "Utils.h"

QueryFundHandler::QueryFundHandler(int iFuncNo)
    : FuncHandler(iFuncNo)
{

}


QueryFundHandler::~QueryFundHandler()
{
}

void QueryFundHandler::initPacketFieldDef()
{
    //user_token	C512	用户口令	Y
    m_vPacketFieldDef.push_back(new PacketFieldDef("user_token", 'S', 512, 0));
}

void QueryFundHandler::doPack(IBizMessage* lpBizMessage, IF2Packer* lpPacker, QMap<QString, QVariant>& params)
{
    qDebug() << "QueryFund request";

    lpBizMessage->SetFunction(FuncHandler::m_iFuncNo);
    lpBizMessage->SetPacketType(REQUEST_PACKET);
    lpBizMessage->SetSystemNo(CONSTANT::SYSTEM_NO);

    QVector<QVariant> valueList;
    valueList.push_back(m_pContext->getUserToken());
    FuncHandler::makePacket(lpPacker, valueList);

    lpBizMessage->SetContent(lpPacker->GetPackBuf(), lpPacker->GetPackLen());
    lpBizMessage->SetPacketId(m_pContext->getNewPacketId());
}

int QueryFundHandler::doUnpack(IBizMessage* lpBizMessage, IF2UnPacker* pUnPacker, QMap<QString, QVariant>& output)
{
    if (pUnPacker->GetDatasetCount() <= 1)
    {
        qCritical() << "Unpack response failed: no data";
        return ERROR_CODE::FAIL;
    }

    //解析委托应答
    pUnPacker->SetCurrentDatasetByIndex(1);
    pUnPacker->First();

    //account_code	C32	账户编号
    //account_name	C32	账户名称
    //account_type	C3	账户类型
    for (int i = 0; i < pUnPacker->GetRowCount(); i++)
    {
        pUnPacker->Go(i + 1); //取值范围[1, GetRowCount()]，fuck！！！
        const char* pAccountCode = pUnPacker->GetStr("account_code");
        const char* pAccountName = pUnPacker->GetStr("account_name");

        m_pContext->addDict(DICT_ID::ACCOUNT_CODE, pAccountCode, Utils::toQString(pAccountName));
    }

    return ERROR_CODE::SUCCESS;
}
