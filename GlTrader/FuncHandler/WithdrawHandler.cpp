#include "stdafx.h"
#include "WithdrawHandler.h"
#include "Context.h"
#include "Utils.h"
#include "OrderMgr.h"

WithdrawHandler::WithdrawHandler(int iFuncNo)
    : FuncHandler(iFuncNo)
{

}

WithdrawHandler::WithdrawHandler(const QString& strMsgType)
    : FuncHandler(strMsgType)
{
    m_pushMsgFuncMap.insert(PUSH_MSG_TYPE::ENTRUST_WITHDRAW, &WithdrawHandler::doUnpackEntrustWithdrawPush);
    m_pushMsgFuncMap.insert(PUSH_MSG_TYPE::ENTRUST_WITHDRAW_DEAL, &WithdrawHandler::doUnpackEntrustWithdrawDealPush);
    m_pushMsgFuncMap.insert(PUSH_MSG_TYPE::ENTRUST_WITHDRAW_FAIL, &WithdrawHandler::doUnpackEntrustWithdrawFailPush);
}

WithdrawHandler::~WithdrawHandler()
{
}

void WithdrawHandler::initPacketFieldDef()
{
    m_vPacketFieldDef.push_back(new PacketFieldDef("user_token", 'S', 512, 0));
    m_vPacketFieldDef.push_back(new PacketFieldDef("account_code", 'S', 32, 0));
    m_vPacketFieldDef.push_back(new PacketFieldDef("combi_no", 'S', 16, 0));
    m_vPacketFieldDef.push_back(new PacketFieldDef("entrust_no", 'I', 8, 0));
    m_vPacketFieldDef.push_back(new PacketFieldDef("extsystem_id", 'I', 8, 0));
}

void WithdrawHandler::doPack(IBizMessage* lpBizMessage, IF2Packer* lpPacker, QMap<QString, QVariant>& params)
{
    lpBizMessage->SetFunction(FuncHandler::m_iFuncNo);
    lpBizMessage->SetPacketType(REQUEST_PACKET);
    lpBizMessage->SetSystemNo(CONSTANT::SYSTEM_NO);

    QVector<QVariant> valueList;
    OrderList* pOrderList = params[PARAMETER::ORDER_LIST_IN].value<OrderList*>();
    for (int i = 0; i < pOrderList->size(); i++)
    {
        Order* pOrder = (*pOrderList)[i];
        pOrder->iExtSystemId = Order::getAutoIncr();
        //撤单委托不加到上下文中
        //m_pContext->addOrder(pOrder);

        qDebug() << "Withdraw request, " << pOrder->toQString().toUtf8().data();

        valueList.push_back(m_pContext->getUserToken());
        valueList.push_back(pOrder->strAccountCode);
        valueList.push_back(pOrder->strCombiNo);
        valueList.push_back(pOrder->iEntrustNo);
        valueList.push_back(pOrder->iExtSystemId);
    }

    FuncHandler::makePacket(lpPacker, valueList);

    lpBizMessage->SetContent(lpPacker->GetPackBuf(), lpPacker->GetPackLen());
    lpBizMessage->SetPacketId(m_pContext->getNewPacketId());
}

 //处理应答消息
int WithdrawHandler::doUnpack(IBizMessage* lpBizMessage, IF2UnPacker* pUnPacker, QMap<QString, QVariant>& output)
{
    if (pUnPacker->GetDatasetCount() <= 1)
    {
        qCritical() << "Unpack response failed: no data";
        return ERROR_CODE::FAIL;
    }

    //解析委托应答
    pUnPacker->SetCurrentDatasetByIndex(1);
    //pUnPacker->First();

    //entrust_no	N8	委托序号
    //market_no	C3	交易市场		参见数据字典4
    //stock_code	C16	证券代码
    //success_flag	C1	撤单成功标志		'1' - 成功 '2' - 失败
    //fail_cause	C256	失败原因

    SAFE_NEW(OrderList, pOrderList);
    for (int i = 0; i < pUnPacker->GetRowCount(); i++)
    {
        pUnPacker->Go(i + 1); //取值范围[1, GetRowCount()]，fuck！！！

        //处理撤单应答消息
        int iEntrustNo = pUnPacker->GetInt("entrust_no");

        Order* pOrder = m_pContext->getOrderByEntrustNo(iEntrustNo);
        if (pOrder == nullptr)
        {
            qCritical() << "Can't find entrust, iEntrustNo = " << iEntrustNo;
            return ERROR_CODE::FAIL;
        }

        pOrder->iCancelSuccessFlag = pUnPacker->GetInt("success_flag");
        pOrder->strFailCause = Utils::toQString(pUnPacker->GetStr("fail_cause"));

        qDebug() << "Withdraw response, " << pOrder->toQString().toUtf8().data();

        pOrderList->push_back(pOrder);
    }

    QVariant var;
    var.setValue(pOrderList);
    output.insert(PARAMETER::ORDER_LIST_OUT, var);

    return ERROR_CODE::SUCCESS;
}

//处理推送消息
int WithdrawHandler::doUnpack(int iSubsIndex, const char* pMsgType, IF2UnPacker* lpUnPack, QMap<QString, QVariant>& output)
{
    //根据extsystem_id查找订单
    int iExtSystemId = lpUnPack->GetInt("extsystem_id");
    Order* pOrder = m_pContext->getOrder(iExtSystemId);
    if (pOrder == nullptr)
    {
        qWarning() << "Can't find entrust, iExtSystemId = " << iExtSystemId << ", igonre it";
        return ERROR_CODE::FAIL;
    }

    //根据消息类型查找处理器
    auto it = m_pushMsgFuncMap.find(pMsgType);
    if (it == m_pushMsgFuncMap.end())
    {
        qWarning() << "Cant't find function to process push message: " << pMsgType;
        return ERROR_CODE::FAIL;
    }
    else
    {
        PtrPushMsgFunc func = it.value();
        return (this->*func)(iSubsIndex, lpUnPack, pOrder, output);
    }

    return ERROR_CODE::SUCCESS;
}



//d 委托撤单
int WithdrawHandler::doUnpackEntrustWithdrawPush(int iSubsIndex, IF2UnPacker* lpUnPack, Order* pOrder, QMap<QString, QVariant>& output)
{
    int iBusinessDate = lpUnPack->GetInt("business_date");//YYYYMMDD
    int iBusinessTime = lpUnPack->GetInt("business_time");//HHMMSS
    int iCancelEntrustNo = lpUnPack->GetInt("cancel_entrust_no");//被撤委托
    int iEntrustNo = lpUnPack->GetInt("entrust_no");//撤单委托
    const char* pEntrustStatus = lpUnPack->GetStr("entrust_status");

    QString strDateTime = QString("%1%2").arg(iBusinessDate).arg(iBusinessTime, 6, 10, (QLatin1Char)('0'));
    pOrder->dtWithdrawBusinessTime = QDateTime::fromString(strDateTime, "yyyyMMddhhmmss");
    pOrder->iCancelEntrustNo = iEntrustNo;

    //可能委托撤成、撤废先到，委托撤单消息后到
    if (pOrder->strEntrustStatus == QString(ENTRUST_STATUS::WITHDRAW_PART)
        || pOrder->strEntrustStatus == QString(ENTRUST_STATUS::WITHDRAW)
        )
    {
        //已经是撤成、撤废了，则不处理
        qWarning() << QString("Invalid entrust status, old = %1(%2), new = %3(%4)")
            .arg(pOrder->strEntrustStatus)
            .arg(m_pContext->queryDict(DICT_ID::ENTRUST_STATUS, pOrder->strEntrustStatus))
            .arg(pEntrustStatus)
            .arg(m_pContext->queryDict(DICT_ID::ENTRUST_STATUS, pEntrustStatus));
    }
    else
    {
        pOrder->strEntrustStatus = Utils::toQString(pEntrustStatus);//待撤
    }


    qDebug() << "Withdraw(d) push, " << pOrder->toQString().toUtf8().data();

    QVariant var;
    var.setValue(pOrder);
    output.insert(PARAMETER::ORDER_OUT, var);

    return ERROR_CODE::SUCCESS;
}

//e 委托撤成
int WithdrawHandler::doUnpackEntrustWithdrawDealPush(int iSubsIndex, IF2UnPacker* lpUnPack, Order* pOrder, QMap<QString, QVariant>& output)
{
    const char* pEntrustStatus = lpUnPack->GetStr("entrust_status");
    int iCancelAmount = lpUnPack->GetInt("cancel_amount");//撤销数量

    pOrder->strEntrustStatus = Utils::toQString(pEntrustStatus);
    pOrder->fCancelDealAmount = iCancelAmount;

    qDebug() << "Withdraw deal(e) push, " << pOrder->toQString().toUtf8().data();

    QVariant var;
    var.setValue(pOrder);
    output.insert(PARAMETER::ORDER_OUT, var);

    return ERROR_CODE::SUCCESS;
}

//f 委托撤废
int WithdrawHandler::doUnpackEntrustWithdrawFailPush(int iSubsIndex, IF2UnPacker* lpUnPack, Order* pOrder, QMap<QString, QVariant>& output)
{
    const char* pEntrustStatus = lpUnPack->GetStr("entrust_status");
    const char* pRevokeCause = lpUnPack->GetStr("revoke_cause");

    pOrder->strEntrustStatus = Utils::toQString(pEntrustStatus);
    pOrder->strFailCause = Utils::toQString(pRevokeCause);

    qDebug() << "Withdraw fail(f) push, " << pOrder->toQString().toUtf8().data();

    QVariant var;
    var.setValue(pOrder);
    output.insert(PARAMETER::ORDER_OUT, var);

    return ERROR_CODE::SUCCESS;
}
