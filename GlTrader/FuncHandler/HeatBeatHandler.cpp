#include "stdafx.h"
#include "HeatBeatHandler.h"
#include "Context.h"
#include "Utils.h"

HeatBeatHandler::HeatBeatHandler(int iFuncNo)
    : FuncHandler(iFuncNo)
{

}


HeatBeatHandler::~HeatBeatHandler()
{
}

void HeatBeatHandler::initPacketFieldDef()
{
    m_vPacketFieldDef.push_back(new PacketFieldDef("user_token", 'S', 512, 0));
}

void HeatBeatHandler::doPack(IBizMessage* lpBizMessage, IF2Packer* lpPacker, QMap<QString, QVariant>& params)
{
    //qDebug() << "HeatBeat request";

    lpBizMessage->SetFunction(FuncHandler::m_iFuncNo);
    lpBizMessage->SetPacketType(REQUEST_PACKET);
    lpBizMessage->SetSystemNo(CONSTANT::SYSTEM_NO);

    QVector<QVariant> valueList;
    valueList.push_back(m_pContext->getUserToken());
    FuncHandler::makePacket(lpPacker, valueList);

    lpBizMessage->SetContent(lpPacker->GetPackBuf(), lpPacker->GetPackLen());
    lpBizMessage->SetPacketId(m_pContext->getNewPacketId());
}

int HeatBeatHandler::doUnpack(IBizMessage* lpBizMessage, IF2UnPacker* pUnPacker, QMap<QString, QVariant>& output)
{
    return ERROR_CODE::SUCCESS;
}
