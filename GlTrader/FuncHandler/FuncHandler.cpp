#include "stdafx.h"
#include "FuncHandler/FuncHandler.h"
#include "CounterCallback.h"
#include "Utils.h"

FuncHandler::FuncHandler(int iFuncNo)
    : m_iFuncNo(iFuncNo)
    , m_pContext(nullptr)
{
    FuncHandlerMgr* p = FuncHandlerMgr::getInstance();
    p->regHandler(m_iFuncNo, this);
}

FuncHandler::FuncHandler(const QString& strMsgType)
    : m_strMsgType(strMsgType)
    , m_iFuncNo(0)
    , m_pContext(nullptr)
{
    FuncHandlerMgr* p = FuncHandlerMgr::getInstance();
    p->regHandler(m_strMsgType, this);
}

FuncHandler::~FuncHandler()
{
    for (int i = 0; i < m_vPacketFieldDef.size(); i++)
    {
        delete m_vPacketFieldDef[i];
    }
    m_vPacketFieldDef.clear();
}

void FuncHandler::setContext(Context* pContext)
{
    m_pContext = pContext;
}

int FuncHandler::packAndSend(CConnectionInterface* pConn, CommMode eMode, QMap<QString, QVariant>& params)
{
    IBizMessage* lpBizMessage = NewBizMessage();
    CAutoKnown ak(lpBizMessage);

    IF2Packer* lpPacker = NewPacker(CONSTANT::PACKER_VERSION);
    CAutoKnown ak2(lpPacker);

    doPack(lpBizMessage, lpPacker, params);

    if (lpBizMessage->GetFunction() != FUNC_NO::HEARTBEAT)
    {
        qDebug() << "Send bizMessage: " << Utils::toQString(lpBizMessage).toUtf8().data();
    }
    else
    {
        qDebug() << "Send heart beat";
    }

    int hSend = pConn->SendBizMsg(lpBizMessage, eMode);

    lpPacker->FreeMem(lpPacker->GetPackBuf());

    return hSend;
}

int FuncHandler::unpack(int hSend, IBizMessage* lpMsg, QMap<QString, QVariant>& output)
{
    //消息错误码
    int iFuncNo = lpMsg->GetFunction();
    int iPacketType = lpMsg->GetPacketType();
    int iPacketId = lpMsg->GetPacketId();

    if (iFuncNo != FUNC_NO::HEARTBEAT)
    {
        qDebug() << "Recv bizMessage: " << Utils::toQString(lpMsg).toUtf8().data();
    }

    int iReturnCode = lpMsg->GetReturnCode();
    if (iReturnCode != 0)
    {
        output.insert(PARAMETER::ERROR_INFO, Utils::toQString(lpMsg->GetErrorInfo()));
        qCritical() << "Error message: errorNo:" << lpMsg->GetErrorNo() << ",errorInfo:" << Utils::toQString(lpMsg->GetErrorInfo());
        return iReturnCode;
    }

    //解包
    int iLen = 0;
    const void* responseBuffer = lpMsg->GetContent(iLen);
    IF2UnPacker* pRespUnPacker = NewUnPacker((void *)responseBuffer, iLen);
    CAutoKnown ak3(pRespUnPacker);

    //业务处理结果
    pRespUnPacker->SetCurrentDatasetByIndex(0);  //第1个结果集是消息头,肯定存在
    pRespUnPacker->First();
    int iErrorCode = pRespUnPacker->GetInt("ErrorCode");     //接口调用结果,0表示成功,非0表示失败
    const char* pErrorMsg = pRespUnPacker->GetStr("ErrorMsg");    //失败时的错误信息,成功时为空
    const char* pMsgDetail = pRespUnPacker->GetStr("MsgDetail");  //详细错误信息,建议取这个字段,方便排查问题
    int iDatasetCount = pRespUnPacker->GetDatasetCount();     //表示有多少个结果集
    if (iErrorCode != 0)
    {
        //output.insert(PARAMETER::ERROR_INFO, 
        //    QString(QStringLiteral("[%1]无效委托，%2：%3")).arg(iErrorCode).arg(Utils::toQString(pErrorMsg)).arg(Utils::toQString(pMsgDetail)));
        
        if (pMsgDetail != nullptr && strlen(pMsgDetail) > 0)
        {
            output.insert(PARAMETER::ERROR_INFO,
                QString(QStringLiteral("%2：%3")).arg(Utils::toQString(pErrorMsg)).arg(Utils::toQString(pMsgDetail)));
        }
        else
        {
            output.insert(PARAMETER::ERROR_INFO, Utils::toQString(pErrorMsg));
        }

        qCritical() << QString::asprintf("Call function[%d] error, ErrorCode = %d, ErrorMsg = '%s', MsgDetail = '%s'",
            iFuncNo,
            iErrorCode,
            Utils::toQString(pErrorMsg).toUtf8().data(),
            Utils::toQString(pMsgDetail).toUtf8().data()).toUtf8().data();
        if (iDatasetCount > 1)
        {
            //对于篮子委托，有针对每条委托的错误信息
            doUnpack(lpMsg, pRespUnPacker, output);
        }

        return iErrorCode;
    }
    else
    {
        //业务解码
        return doUnpack(lpMsg, pRespUnPacker, output);
    }

}

int FuncHandler::unpack(int iSubsIndex, const char* pMsgType, IF2UnPacker* lpUnPack, QMap<QString, QVariant>& output)
{
    return doUnpack(iSubsIndex, pMsgType, lpUnPack, output);
}

int FuncHandler::doUnpack(int iSubsIndex, const char* pMsgType, IF2UnPacker* lpUnPack, QMap<QString, QVariant>& output)
{
    return ERROR_CODE::SUCCESS;
}

void FuncHandler::makePacket(IF2Packer* pPacker, const QVector<QVariant>& vValueList)
{
    if (m_vPacketFieldDef.size() == 0)
    {
        initPacketFieldDef();
    }

    assert(vValueList.size() % m_vPacketFieldDef.size() == 0);

    pPacker->BeginPack();

    //列名
    for (auto f : m_vPacketFieldDef)
    {
        pPacker->AddField(f->szFieldName, f->cFieldType, f->iFieldWidth, f->iFieldScale);
    }

    //值
    for (int i = 0; i < vValueList.size(); i++)
    {
        char type = m_vPacketFieldDef.at(i % m_vPacketFieldDef.size())->cFieldType;
        const QVariant& val = vValueList.at(i);

        switch (type)
        {
        case 'S':
            pPacker->AddStr(val.toString().toUtf8().constData());
            break;
        case 'C':
            pPacker->AddChar(val.toChar().unicode());
            break;
        case 'I':
            pPacker->AddInt(val.toInt());
            break;
        case 'F':
            pPacker->AddDouble(val.toDouble());
            break;
        case 'R':
        {
            QByteArray buf = val.toByteArray();
            pPacker->AddRaw(buf.data(), buf.length());
            break;
        }
        default:
            qCritical() << "Unknown field type: " << type << ", name = " << m_vPacketFieldDef.at(i % m_vPacketFieldDef.size())->szFieldName;
            break;
        }
    }

    pPacker->EndPack();
}

//***************************************************************
//
//  FuncHandlerMgr
//
//***************************************************************
FuncHandlerMgr* FuncHandlerMgr::m_pInstance = nullptr;

FuncHandlerMgr* FuncHandlerMgr::getInstance()
{
    if (m_pInstance == nullptr)
    {
        m_pInstance = new FuncHandlerMgr;
    }
    return m_pInstance;
}

void FuncHandlerMgr::destoryInstance()
{
    if (m_pInstance != nullptr)
    {
        delete m_pInstance;
        m_pInstance = nullptr;
    }
}

void FuncHandlerMgr::setContext(Context* pContext)
{
    for (auto it = m_funcHandlerPtrMap.begin(); it != m_funcHandlerPtrMap.end(); it++)
    {
        it.value()->setContext(pContext);
    }
    for (auto it = m_pushHandlerPtrMap.begin(); it != m_pushHandlerPtrMap.end(); it++)
    {
        it.value()->setContext(pContext);
    }

}

void FuncHandlerMgr::regHandler(int iFuncNo, FuncHandler* pHandler)
{
    auto it = m_funcHandlerPtrMap.find(iFuncNo);
    if (it == m_funcHandlerPtrMap.end())
    {
        m_funcHandlerPtrMap.insert(iFuncNo, pHandler);
    }
    else
    {
        //TODO 要将处理器改为列表
    }
}

void FuncHandlerMgr::unregHandler(int iFuncNo, FuncHandler* pHandler)
{
    m_funcHandlerPtrMap.remove(iFuncNo);
}

FuncHandler* FuncHandlerMgr::getHandler(int iFuncNo)
{
    return m_funcHandlerPtrMap.value(iFuncNo);
}

void FuncHandlerMgr::regHandler(const QString& strMsgType, FuncHandler* pHandler)
{
    auto it = m_pushHandlerPtrMap.find(strMsgType);
    if (it == m_pushHandlerPtrMap.end())
    {
        m_pushHandlerPtrMap.insert(strMsgType, pHandler);
    }
    else
    {
        //TODO 要将处理器改为列表
    }
}

void FuncHandlerMgr::unregHandler(const QString& strMsgType, FuncHandler* pHandler)
{
    m_pushHandlerPtrMap.remove(strMsgType);
}

FuncHandler* FuncHandlerMgr::getHandler(const QString& strMsgType)
{
    return m_pushHandlerPtrMap.value(strMsgType);
}
