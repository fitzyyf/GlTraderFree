#include "stdafx.h"
#include "LoginHandler.h"
#include "Context.h"
#include "Utils.h"

LoginHandler::LoginHandler(int iFuncNo)
    : FuncHandler(iFuncNo)
{

}


LoginHandler::~LoginHandler()
{
}

void LoginHandler::initPacketFieldDef()
{
    m_vPacketFieldDef.push_back(new PacketFieldDef("operator_no", 'S', 16, 0));
    m_vPacketFieldDef.push_back(new PacketFieldDef("password", 'S', 32, 0));
    m_vPacketFieldDef.push_back(new PacketFieldDef("mac_address", 'S', 255, 0));
    m_vPacketFieldDef.push_back(new PacketFieldDef("op_station", 'S', 255, 0));
    m_vPacketFieldDef.push_back(new PacketFieldDef("ip_address", 'S', 32, 0));
    m_vPacketFieldDef.push_back(new PacketFieldDef("hd_volserial", 'S', 128, 0));
    m_vPacketFieldDef.push_back(new PacketFieldDef("authorization_id", 'S', 64, 0));
    //m_vPacketFieldDef.push_back(new PacketFieldDef("init_check", 'S', 1, 0));
}

void LoginHandler::doPack(IBizMessage* lpBizMessage, IF2Packer* lpPacker, QMap<QString, QVariant>& params)
{
    qDebug() << "Login request: operator_no = " << m_pContext->getUsername();

    lpBizMessage->SetFunction(FuncHandler::m_iFuncNo);
    lpBizMessage->SetPacketType(REQUEST_PACKET);
    lpBizMessage->SetSystemNo(CONSTANT::SYSTEM_NO);

    QVector<QVariant> valueList;
    valueList.push_back(m_pContext->getUsername());
    valueList.push_back(m_pContext->getPassword());
    valueList.push_back(m_pContext->getMac());//地址，必须是12位
    valueList.push_back(Utils::getComputerName());//计算机名称
    valueList.push_back(m_pContext->getIp());//ip地址
    valueList.push_back(m_pContext->getHDSerialId());//硬盘序列号
    valueList.push_back("1");//TODO 开发者授权id 这是什么？？
    FuncHandler::makePacket(lpPacker, valueList);

    lpBizMessage->SetContent(lpPacker->GetPackBuf(), lpPacker->GetPackLen());
    lpBizMessage->SetPacketId(m_pContext->getNewPacketId());
}

int LoginHandler::doUnpack(IBizMessage* lpBizMessage, IF2UnPacker* pUnPacker, QMap<QString, QVariant>& output)
{
    if (pUnPacker->GetDatasetCount() <= 1)
    {
        qCritical() << "Unpack response failed: no data";
        return ERROR_CODE::FAIL;
    }
    pUnPacker->SetCurrentDatasetByIndex(1);
    pUnPacker->First();

    m_pContext->setUserToken(pUnPacker->GetStr("user_token"));
    const char* pVersionNo = pUnPacker->GetStr("version_no");

    qDebug() << "Login response: user_token=" << m_pContext->getUserToken() << ", version_no=" << Utils::toQString(pVersionNo);

    return ERROR_CODE::SUCCESS;
}
