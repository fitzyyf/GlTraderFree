#pragma once

#include "Constants.h"
#include "FuncHandler/FuncHandler.h"

class EntrustHandler;
class Order;

typedef int (EntrustHandler::*PtrPushMsgFunc)(int, IF2UnPacker*, Order*, QMap<QString, QVariant>&);

class EntrustHandler : public FuncHandler
{
public:
    EntrustHandler(int iFuncNo);
    EntrustHandler(const QString& strMsgType);
    virtual ~EntrustHandler();

protected:
    virtual void initPacketFieldDef() override;

    virtual void doPack(IBizMessage* lpBizMessage, IF2Packer* lpPacker, QMap<QString, QVariant>& params = QMap<QString, QVariant>()) override;
    
    //处理应答消息
    virtual int doUnpack(IBizMessage* lpBizMessage, IF2UnPacker* pUnPacker, QMap<QString, QVariant>& output = QMap<QString, QVariant>()) override;
    
    //处理推送消息总入口
    virtual int doUnpack(int iSubsIndex, const char* pMsgType, IF2UnPacker* lpUnPack, QMap<QString, QVariant>& output = QMap<QString, QVariant>());

    //处理各类推送消息
    //a 委托下达
    virtual int doUnpackEntrustPush(int iSubsIndex, IF2UnPacker* lpUnPack, Order* pOrder, QMap<QString, QVariant>& output = QMap<QString, QVariant>());
    //b 委托确认
    virtual int doUnpackEntrustConfirmPush(int iSubsIndex, IF2UnPacker* lpUnPack, Order* pOrder, QMap<QString, QVariant>& output = QMap<QString, QVariant>());
    //c 委托废单
    virtual int doUnpackEntrustFailPush(int iSubsIndex, IF2UnPacker* lpUnPack, Order* pOrder, QMap<QString, QVariant>& output = QMap<QString, QVariant>());
    //g 委托成交
    virtual int doUnpackEntrustDealPush(int iSubsIndex, IF2UnPacker* lpUnPack, Order* pOrder, QMap<QString, QVariant>& output = QMap<QString, QVariant>());

private:
    QMap<QString, PtrPushMsgFunc> m_pushMsgFuncMap;
};


REGISTER_FUNC_HANDLER(FUNC_NO::ENTRUST, EntrustHandler);

REGISTER_PUSH_HANDLER(1, PUSH_MSG_TYPE::ENTRUST, EntrustHandler);
REGISTER_PUSH_HANDLER(2, PUSH_MSG_TYPE::ENTRUST_CONFIRM, EntrustHandler);
REGISTER_PUSH_HANDLER(3, PUSH_MSG_TYPE::ENTRUST_FAIL, EntrustHandler);
REGISTER_PUSH_HANDLER(7, PUSH_MSG_TYPE::ENTRUST_DEAL, EntrustHandler);
