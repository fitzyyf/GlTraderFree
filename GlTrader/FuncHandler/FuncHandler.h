#pragma once

#include "hundsun/t2sdk_interface.h"
#include "Constants.h"

class Context;

struct PacketFieldDef
{
    const char *szFieldName;
    char cFieldType;
    int iFieldWidth;
    int iFieldScale;

    PacketFieldDef(const char* n, char t = 'S', int w = 255, int s = 4)
        : szFieldName(n), cFieldType(t), iFieldWidth(w), iFieldScale(s)
    {
    }
};

struct PacketField
{
    const char* field;
    char type;
    QVariant val;

    PacketField() {}
    PacketField(const char* f, char t, QVariant v) :field(f), type(t), val(v) {}
};

class FuncHandler
{
public:
    FuncHandler(int iFuncNo);
    FuncHandler(const QString& strMsgType);
    virtual ~FuncHandler();

    void setContext(Context* pContext);

    virtual int packAndSend(CConnectionInterface* pConn, CommMode eMode = CommMode::Asy, QMap<QString, QVariant>& params = QMap<QString, QVariant>());
    
    //异步应答消息解码
    virtual int unpack(int hSend, IBizMessage* lpMsg, QMap<QString, QVariant>& output = QMap<QString, QVariant>());
    
    //推送消息解码
    virtual int unpack(int iSubsIndex, const char* pMsgType, IF2UnPacker* lpUnPack, QMap<QString, QVariant>& output = QMap<QString, QVariant>());

protected:
    virtual void initPacketFieldDef() = 0;
    virtual void doPack(IBizMessage* pBizMessage, IF2Packer* pPacker, QMap<QString, QVariant>& params = QMap<QString, QVariant>()) = 0;
    virtual int doUnpack(IBizMessage* lpBizMessage, IF2UnPacker* pUnPacker, QMap<QString, QVariant>& output = QMap<QString, QVariant>()) = 0;
    virtual int doUnpack(int iSubsIndex, const char* pMsgType, IF2UnPacker* lpUnPack, QMap<QString, QVariant>& output = QMap<QString, QVariant>());

    void makePacket(IF2Packer* pPacker, const QVector<QVariant>& vValueList);

protected:
    Context* m_pContext;
    int m_iFuncNo;
    QString m_strMsgType;
    QVector<PacketFieldDef*> m_vPacketFieldDef;
};


class FuncHandlerMgr
{
public:
    static FuncHandlerMgr* getInstance();
    static void destoryInstance();

    void setContext(Context* pContext);

    void regHandler(int iFuncNo, FuncHandler* pHandler);
    void unregHandler(int iFuncNo, FuncHandler* pHandler);
    FuncHandler* getHandler(int iFuncNo);

    void regHandler(const QString& strMsgType, FuncHandler* pHandler);
    void unregHandler(const QString& strMsgType, FuncHandler* pHandler);
    FuncHandler* getHandler(const QString& strMsgType);

private:
    FuncHandlerMgr() {}
    FuncHandlerMgr(const FuncHandlerMgr&) = delete;
    FuncHandlerMgr(FuncHandlerMgr&&) = delete;
    FuncHandlerMgr& operator =(const FuncHandlerMgr&) = delete;

private:
    static FuncHandlerMgr* m_pInstance;
    QMap<int, FuncHandler*> m_funcHandlerPtrMap;
    QMap<QString, FuncHandler*> m_pushHandlerPtrMap;
};

//定义功能号处理器
#define REGISTER_FUNC_HANDLER(func_no, handler)  static handler _##handler##_func_instance(func_no)

//定义推送消息处理器
#define REGISTER_PUSH_HANDLER(id, msg_type, handler)  static handler _##handler##_push_##id##_instance(msg_type)
