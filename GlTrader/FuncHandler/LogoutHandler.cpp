#include "stdafx.h"
#include "LogoutHandler.h"
#include "Context.h"
#include "Utils.h"

LogoutHandler::LogoutHandler(int iFuncNo)
    : FuncHandler(iFuncNo)
{

}


LogoutHandler::~LogoutHandler()
{
}

void LogoutHandler::initPacketFieldDef()
{
    m_vPacketFieldDef.push_back(new PacketFieldDef("user_token", 'S', 512, 0));
}

void LogoutHandler::doPack(IBizMessage* lpBizMessage, IF2Packer* lpPacker, QMap<QString, QVariant>& params)
{
    qDebug() << "Logout request";

    lpBizMessage->SetFunction(FuncHandler::m_iFuncNo);
    lpBizMessage->SetPacketType(REQUEST_PACKET);
    lpBizMessage->SetSystemNo(CONSTANT::SYSTEM_NO);

    QVector<QVariant> valueList;
    valueList.push_back(m_pContext->getUserToken());
    FuncHandler::makePacket(lpPacker, valueList);

    lpBizMessage->SetContent(lpPacker->GetPackBuf(), lpPacker->GetPackLen());
    lpBizMessage->SetPacketId(m_pContext->getNewPacketId());
}

int LogoutHandler::doUnpack(IBizMessage* lpBizMessage, IF2UnPacker* pUnPacker, QMap<QString, QVariant>& output)
{
    return ERROR_CODE::SUCCESS;
}
