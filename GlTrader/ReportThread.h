#pragma once
#include <QThread>
#include "OrderMgr.h"
#include "Config.h"

class DbHelper;
class Order;

class ReportThread : public QThread
{
    Q_OBJECT

public:
    ReportThread(ConfigData configData, QObject *parent = nullptr);
    virtual ~ReportThread();

    virtual void run() override;
    void setMaxId(int iMaxEntrustId, int iMaxWithdrawId);

Q_SIGNALS:
    void sigRunLog(int type, QString message);

public slots:
    void onNewEntrustHandled(int iOrderInternalId);
    void onNewWithdrawHandled(int iOrderInternalId);

    void onEntrustResult(Order* pOrder);
    void onWithdrawResult(Order* pOrder);

    void updateEntrustList(const Order* pOrder);
    void updateDealList(const Order* pOrder);

private:
    ConfigData m_ConfigData;

    DbHelper* m_pOutDbHelper;

    int m_iMaxEntrustId;
    int m_iMaxWithdrawId;
};

