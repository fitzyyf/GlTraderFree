#pragma once

#include <QMap>
#include <QVector>
#include <QString>

#include "sqlite/sqlite3.h"
#include "DbTable.h"

using namespace std;

typedef QMap<QString, QString> DbKV;
typedef QMap<QString, QString>* DbKVPtr;
typedef QMap<QString, QString>::iterator DbKVIter;

typedef QVector<DbKVPtr> DbRecordSet;
typedef QVector<DbKVPtr>* DbRecordSetPtr;

enum EDbSort
{
	ASC  = 0,
	DESC = 1
};

class DbHelper
{
public:
    DbHelper();
    virtual ~DbHelper();

	bool open(const char* pDbFileName, bool bReadOnly = false);
	bool close();

	//根据某一个字段的值获取一条记录中的某个字段的值
	const QString getValueByKey(const char* pTableName, const char* pResultColumn,
		const char* pWhereColumn = NULL, const char* pWhereValue = NULL, const char* defaultValue = NULL);
	const QString getValueByKey(const char* pTableName, const char* pResultColumn,
		const char* pWhereColumn = NULL, int iWhereValue = -1, const char* defaultValue = NULL);

	int getValueByKeyToInt(const char* pTableName, const char* pResultColumn,
		const char* pWhereColumn = NULL, const char* pWhereValue = NULL, int defaultValue = 0);
	int getValueByKeyToInt(const char* pTableName, const char* pResultColumn,
		const char* pWhereColumn = NULL, int iWhereValue = -1, int defaultValue = 0);

	float getValueByKeyToFloat(const char* pTableName, const char* pResultColumn,
		const char* pWhereColumn = NULL, const char* pWhereValue = NULL, float defaultValue = 0.0f);
	float getValueByKeyToFloat(const char* pTableName, const char* pResultColumn,
		const char* pWhereColumn = NULL, int iWhereValue = -1, float defaultValue = 0.0f);

	int getRecordByKey(const char* pTableName, DbRecordSetPtr pResultSet,
		const char* pWhereColumn = NULL, int iWhereValue = -1,
		const char* pOrderColumn = NULL, EDbSort eSort = EDbSort::ASC);

	int getRecordByKey(const char* pTableName, DbRecordSetPtr pResultSet,
		const char* pWhereColumn = NULL, const char* pWhereValue = NULL,
		const char* pOrderColumn = NULL, EDbSort eSort = EDbSort::ASC);

	int getRecordById(const char* pTableName, DbRecordSetPtr pResultSet,
		int id,
		const char* orderColumn = NULL, EDbSort eSort = EDbSort::ASC);

	int getRecord(const char* pTableName, DbRecordSetPtr pResultSet,
		DbKVPtr pWhere = NULL, DbKVPtr pOrder = NULL);

	int getRecordBySql(const char* sql, DbRecordSetPtr pResultSet);

    bool insertOrReplaceRecord(const char* pTableName, DbKVPtr pRecord, QString& strErrMsg);
    bool insertRecord(const char* pTableName, DbKVPtr pRecord, QString& strErrMsg);
    bool updateRecord(const char* pTableName, DbKVPtr pUpdate, DbKVPtr pWhere, QString& strErrMsg);

    bool execSql(const char* sql, QString& strErrMsg);

	static void freeResultSet(DbRecordSetPtr pResultSet);

protected:
	sqlite3* m_pDb;
};