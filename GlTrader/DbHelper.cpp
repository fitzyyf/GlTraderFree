#include "stdafx.h"

#include "DbHelper.h"
#include "Utils.h"
#include "Constants.h"


DbHelper::DbHelper()
{
	m_pDb = nullptr;
}

DbHelper::~DbHelper()
{
}

bool DbHelper::open(const char* pDbFileName, bool bReadOnly)
{
	int ret = sqlite3_open(pDbFileName, &m_pDb);
	if (ret != SQLITE_OK) 
	{
		const char* err = sqlite3_errmsg(m_pDb);
        qCritical("sqlite open error: %s", err);
		sqlite3_close(m_pDb);
		m_pDb = nullptr;
		return false;
	}

    if (bReadOnly)
    {
        close();

        int ret = sqlite3_open_v2(pDbFileName, &m_pDb, SQLITE_OPEN_READONLY, NULL);
        if (ret != SQLITE_OK)
        {
            const char* err = sqlite3_errmsg(m_pDb);
            qCritical("sqlite open error: %s", err);
            sqlite3_close(m_pDb);
            m_pDb = nullptr;
            return false;
        }
        else
        {
            sqlite3_busy_timeout(m_pDb, 1000);
        }
    }

	return true;
}

bool DbHelper::close()
{
	//关闭并删除数据库
	if (m_pDb != nullptr)
	{
		sqlite3_close(m_pDb);
		m_pDb = nullptr;
	}

	return true;
}

const QString DbHelper::getValueByKey(const char* pTableName, const char* pResultColumn,
	const char* pWhereColumn, const char* pWhereValue, const char* defaultValue)
{
    SAFE_NEW(DbRecordSet, pRecordSet);
	int cnt = getRecordByKey(pTableName, pRecordSet, pWhereColumn, pWhereValue);
	if (cnt <= 0)
	{
        DbHelper::freeResultSet(pRecordSet);
		qDebug("GlobalData::initGlobal : no record");
		return defaultValue;
	}

	DbKVPtr pRecord = pRecordSet->at(0);
	DbKVIter it = pRecord->find(pResultColumn);
    QString value;
	if (it != pRecord->end())
	{
		value = it.value();
	}
	else
	{
        value = defaultValue;
	}

    DbHelper::freeResultSet(pRecordSet);

    return value;
}

const QString DbHelper::getValueByKey(const char* pTableName, const char* pResultColumn,
	const char* pWhereColumn, int iWhereValue, const char* defaultValue)
{
	char pWhereValue[16] = { 0 };
	snprintf(pWhereValue, 15, "%d", iWhereValue);

	return getValueByKey(pTableName, pResultColumn,
		pWhereColumn, pWhereValue, defaultValue);
}

int DbHelper::getValueByKeyToInt(const char* pTableName, const char* pResultColumn,
	const char* pWhereColumn, const char* pWhereValue, int defaultValue)
{
	char pDefaultValue[16] = { 0 };
	snprintf(pDefaultValue, 15, "%d", defaultValue);

	return getValueByKey(pTableName, pResultColumn,
		pWhereColumn, pWhereValue, pDefaultValue).toInt();
}

int DbHelper::getValueByKeyToInt(const char* pTableName, const char* pResultColumn,
	const char* pWhereColumn, int iWhereValue, int defaultValue)
{
	char pDefaultValue[16] = { 0 };
	snprintf(pDefaultValue, 15, "%d", defaultValue);

	return getValueByKey(pTableName, pResultColumn,
		pWhereColumn, iWhereValue, pDefaultValue).toInt();
}

float DbHelper::getValueByKeyToFloat(const char* pTableName, const char* pResultColumn,
	const char* pWhereColumn, const char* pWhereValue, float defaultValue)
{
	char pDefaultValue[32] = { 0 };
	snprintf(pDefaultValue, 21, "%f", defaultValue);

	return getValueByKey(pTableName, pResultColumn,
		pWhereColumn, pWhereValue, pDefaultValue).toFloat();
}

float DbHelper::getValueByKeyToFloat(const char* pTableName, const char* pResultColumn,
	const char* pWhereColumn, int iWhereValue, float defaultValue)
{
	char pDefaultValue[32] = { 0 };
	snprintf(pDefaultValue, 21, "%f", defaultValue);

	return getValueByKey(pTableName, pResultColumn,
		pWhereColumn, iWhereValue, pDefaultValue).toFloat();
}


int DbHelper::getRecordById(const char* pTableName, DbRecordSetPtr pResultSet, 
	int id,
	const char* pOrderColumn, EDbSort eSort)
{
	char sz[16] = { 0 };
	snprintf(sz, 15, "%d", id);
	return getRecordByKey(pTableName, pResultSet, "id", sz, pOrderColumn, eSort);
}

int DbHelper::getRecordByKey(const char* pTableName, DbRecordSetPtr pResultSet,
	const char* pWhereColumn, int iWhereValue,
	const char* pOrderColumn, EDbSort eSort)
{
	char pWhereValue[16] = { 0 };
	snprintf(pWhereValue, 15, "%d", iWhereValue);

	return getRecordByKey(pTableName, pResultSet,
		pWhereColumn, pWhereValue,
		pOrderColumn, eSort);
}

int DbHelper::getRecordByKey(const char* pTableName, DbRecordSetPtr pResultSet,
	const char* pWhereColumn, const char* pWhereValue,
	const char* pOrderColumn, EDbSort eSort)
{
	int max = 1023;
	int len = 0;
	char sql[1024] = { 0 };

	len += snprintf(sql + len, max - len, "select * from %s where 1 = 1 ", pTableName);
	if (pWhereColumn != NULL && pWhereValue != NULL)
	{
		len += snprintf(sql + len, max - len, " and %s = \'%s\' ", pWhereColumn, pWhereValue);
	}

	if (pOrderColumn != NULL && (eSort == EDbSort::ASC || eSort == EDbSort::DESC))
	{
		if (EDbSort::ASC == eSort)
		{
			len += snprintf(sql + len, max - len, " order by %s asc ", pOrderColumn);
		}
		else
		{
			len += snprintf(sql + len, max - len, " order by %s desc ", pOrderColumn);
		}
	}

	return getRecordBySql(sql, pResultSet);
}


int DbHelper::getRecord(const char* pTableName, DbRecordSetPtr pResultSet, DbKVPtr pWhere, DbKVPtr pOrder)
{
	int max = 1023;
	char sql[1024] = { 0 };
	int len = 0;
	len += snprintf(sql, max, "select * from %s where 1 = 1 ", pTableName);

	if (pWhere != NULL)
	{
		DbKV::iterator it = pWhere->begin();
		for (; it != pWhere->end(); it++)
		{
			len += snprintf(sql + len, max - len, " and %s = '%s' ", 
                it.key().toUtf8().data(), it.value().toUtf8().data());
		}
	}

	if (pOrder != NULL)
	{
		len += snprintf(sql + len, max - len, " order by 1 ");
		DbKV::iterator it2 = pOrder->begin();
		for (; it2 != pOrder->end(); it2++)
		{
			len += snprintf(sql + len, max - len, " , %s %s ", 
                it2.key().toUtf8().data(), it2.value().toUtf8().data());
		}
	}

	return getRecordBySql(sql, pResultSet);
}

void DbHelper::freeResultSet(DbRecordSetPtr pResultSet)
{
	if (!pResultSet)
	{
		return;
	}

	for (unsigned i = 0; i < pResultSet->size(); i++)
	{
		SAFE_DELETE((*pResultSet)[i]);
	}

	SAFE_DELETE(pResultSet);
}

int DbHelper::getRecordBySql(const char* sql, DbRecordSetPtr pResultSet)
{
	char** table; // 查询结果
	int row, col;     // 行数、列数

	int ret = sqlite3_get_table(m_pDb, sql, &table, &row, &col, nullptr);
	if (ret != SQLITE_OK)
	{
		qDebug("SQL: %s", sql);
		qCritical("sqlite query error: %s", sqlite3_errmsg(m_pDb));
		return -1;
	}
	//qDebug("RLT: %d\tSQL: %s", row, sql);

	if (0 == row)
	{
		sqlite3_free_table(table);
		return 0;
	}

	// 第0行（0 ~ c-1），为字段名
	// 第1行（c ~ 2*c-1），第一条记录
	// ......
	QVector<QString> colName;
	for (int j = 0; j < col; j++)
	{
		QString s(table[j]);
		//transform(s.begin(), s.end(), s.begin(), toupper);
		colName.push_back(s.toUpper());
	}

	for (int i = 1; i <= row; i++)
	{
		SAFE_NEW(DbKV, pRow);

		for (int j = 0; j < col; j++)
		{
			const char* value = table[i * col + j];
			if (!value)
			{
				value = "";
			}

            pRow->insert(colName.at(j), value);
		}

		pResultSet->push_back(pRow);
	}

	// 记得是否查询表
	sqlite3_free_table(table);

	return row;
}

bool DbHelper::execSql(const char* sql, QString& strErrMsg)
{
    char* pzErrMsg;
    int iRet = sqlite3_exec(m_pDb, sql, nullptr, nullptr, &pzErrMsg);
    if (SQLITE_OK == iRet)
    {
        strErrMsg.clear();
        return true;
    }
    else
    {
        strErrMsg = QString("[%1]%2").arg(iRet).arg(Utils::toQString(pzErrMsg));
        qCritical() <<"Execute sql error: " << strErrMsg << ", sql: " << sql;
        return false;
    }
}

bool DbHelper::insertOrReplaceRecord(const char* pTableName, DbKVPtr pRecord, QString& strErrMsg)
{
    QString cols;
    QString vals;

    for (auto it = pRecord->begin(); it != pRecord->end(); it++)
    {
        cols.append(it.key()).append(",");
        vals.append("'").append(it.value()).append("',");
    }
    cols.resize(cols.length() - 1);
    vals.resize(vals.length() - 1);

    QString sql = QString("insert or replace into %1(%2) values(%3)").arg(pTableName).arg(cols).arg(vals);

    return execSql(sql.toUtf8().data(), strErrMsg);
}

bool DbHelper::insertRecord(const char* pTableName, DbKVPtr pRecord, QString& strErrMsg)
{
    QString cols;
    QString vals;

    for (auto it = pRecord->begin(); it != pRecord->end(); it++)
    {
        cols.append(it.key()).append(",");
        vals.append("'").append(it.value()).append("',");
    }
    cols.resize(cols.length() - 1);
    vals.resize(vals.length() - 1);

    QString sql = QString("insert into %1(%2) values(%3)").arg(pTableName).arg(cols).arg(vals);

    return execSql(sql.toUtf8().data(), strErrMsg);
}

bool DbHelper::updateRecord(const char* pTableName, DbKVPtr pUpdate, DbKVPtr pWhere, QString& strErrMsg)
{
    QString vals;
    for (auto it = pUpdate->begin(); it != pUpdate->end(); it++)
    {
        vals.append(QString("%1 = '%2'").arg(it.key()).arg(it.value()));
    }

    QString wheres;
    for (auto it = pUpdate->begin(); it != pUpdate->end(); it++)
    {
        vals.append(QString("and %1 = '%2'").arg(it.key()).arg(it.value()));
    }

    QString sql = QString("update %1 set %2 where 1=1 %3").arg(pTableName).arg(vals).arg(wheres);

    return execSql(sql.toUtf8().data(), strErrMsg);
}
