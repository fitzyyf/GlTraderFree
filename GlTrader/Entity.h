#pragma once

struct TDict
{
    int dict_id;
    QString item;
    QString remark;
};

struct TRunConfig
{
    QString item;
    QString value;
};
