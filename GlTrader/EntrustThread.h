#pragma once

#include <QThread>
#include <QTimer>
#include "OrderMgr.h"
#include "Config.h"
#include "DbHelper.h"

class Context;

class EntrustThread : public QThread
{
    Q_OBJECT

public:
    EntrustThread(ConfigData configData, QObject *parent = nullptr);
    virtual ~EntrustThread();

    void setMonitorInterval(int iInterval);
    void setMaxId(int iMaxEntrustId, int iMaxWithdrawId);

    virtual void run() override;

    void startMonitor();
    void stopMonitor();

private:
    void startTimer();
    void stopTimer();

    void monitorEntrust();
    void monitorWithdraw();

Q_SIGNALS:
    void sigNewEntrust(OrderList* pOrderList);
    void sigNewWithdraw(OrderList* pOrderList);

    void sigQueryOrderByEntrustNo(OrderList* pOrderList);
    void sigRunLog(int type, QString message);

private slots:
    void onQueryOrderByEntrustNoResult(OrderList* pOrignOrderList);

private:
    ConfigData m_ConfigData;

    QTimer* m_pEntrustMonitorTimer;
    QTimer* m_pWithdrawMonitorTimer;

    int m_iMonitorInterval;

    int m_iMaxEntrustId;
    int m_iMaxWithdrawId;

    DbHelper* m_pInDbHelper;
    bool m_bRun;

    QMap<int, Order*> m_withdrawOrderMap;
    //bool m_bFirstMonitorEntrust;
    //bool m_bFirstMonitorWithdraw;

    DbRecordSet m_entrustRecordSet;
    DbRecordSet m_withdrawRecordSet;
};

