#include "stdafx.h"
#include "ReportThread.h"
#include "DbHelper.h"
#include "Utils.h"

ReportThread::ReportThread(ConfigData configData, QObject *parent)
    : QThread(parent)
    , m_ConfigData(configData)
    , m_pOutDbHelper(nullptr)
    , m_iMaxEntrustId(0)
    , m_iMaxWithdrawId(0)
{
    moveToThread(this);
}


ReportThread::~ReportThread()
{
    if (m_pOutDbHelper)
    {
        m_pOutDbHelper->close();
    }
    SAFE_DELETE(m_pOutDbHelper);

}

void ReportThread::run()
{
    if (m_pOutDbHelper == nullptr)
    {
        m_pOutDbHelper = new DbHelper;
    }

    if (!m_pOutDbHelper->open(Utils::getOutDbName().toUtf8().data()))
    {
        qCritical() << "Open database failed: " << Utils::getOutDbName();
        emit sigRunLog(RunLog_Error, QStringLiteral("打开数据库文件失败：%1").arg(Utils::getOutDbName()));

        SAFE_DELETE(m_pOutDbHelper);
        return;
    }

    QString strErrMsg;
    if (!m_pOutDbHelper->execSql("PRAGMA journal_mode=WAL;", strErrMsg))
    {
        qCritical() << "Set WAL mode failed: " << strErrMsg.toUtf8().data();
        emit sigRunLog(RunLog_Error, QStringLiteral("设置数据库WAL模式失败"));

        SAFE_DELETE(m_pOutDbHelper);
        return;
    }

    exec();
}

void ReportThread::setMaxId(int iMaxEntrustId, int iMaxWithdrawId)
{
    m_iMaxEntrustId = iMaxEntrustId;
    m_iMaxWithdrawId = iMaxWithdrawId;
}

void ReportThread::onNewEntrustHandled(int iOrderInternalId)
{
    if (iOrderInternalId <= m_iMaxEntrustId)
    {
        return;
    }

    m_iMaxEntrustId = iOrderInternalId;

    DbKV values;
    values.insert(DBTABLE::TRUNCONFIG::COL_ITEM, RUN_CONFIG::MAX_ENTRUST_ID);
    values.insert(DBTABLE::TRUNCONFIG::COL_VALUE, QString("%1").arg(m_iMaxEntrustId));

    QString strErrMsg;
    bool bRet = m_pOutDbHelper->insertOrReplaceRecord(DBTABLE::TRUNCONFIG::TABLE, &values, strErrMsg);
    if (!bRet)
    {
        qCritical() << "ReportThread::onNewEntrustHandled error: " << strErrMsg;
        emit sigRunLog(RunLog_Error, QStringLiteral("更新表 %1 失败：%2").arg(DBTABLE::TRUNCONFIG::TABLE).arg(strErrMsg));
    }
    else
    {
        qDebug() << "Update" << RUN_CONFIG::MAX_ENTRUST_ID << "to" << m_iMaxEntrustId;
    }

}

void ReportThread::onNewWithdrawHandled(int iOrderInternalId)
{
    if (iOrderInternalId <= m_iMaxWithdrawId)
    {
        return;
    }

    m_iMaxWithdrawId = iOrderInternalId;

    DbKV values;
    values.insert(DBTABLE::TRUNCONFIG::COL_ITEM, RUN_CONFIG::MAX_WITHDRAW_ID);
    values.insert(DBTABLE::TRUNCONFIG::COL_VALUE, QString("%1").arg(m_iMaxWithdrawId));

    QString strErrMsg;
    bool bRet = m_pOutDbHelper->insertOrReplaceRecord(DBTABLE::TRUNCONFIG::TABLE, &values, strErrMsg);
    if (!bRet)
    {
        emit sigRunLog(RunLog_Error, QStringLiteral("更新表 %1 失败：%2").arg(DBTABLE::TRUNCONFIG::TABLE).arg(strErrMsg));
    }
    else
    {
        qDebug() << "Update" << RUN_CONFIG::MAX_WITHDRAW_ID << "to" << m_iMaxWithdrawId;
    }
}

void ReportThread::onEntrustResult(Order* pOrder)
{
    qDebug() << "ReportThread::onEntrustResult, " << pOrder->toQString().toUtf8().data();

    //更新成交明细
    if (pOrder->strEntrustStatus == ENTRUST_STATUS::DEAL
        || pOrder->strEntrustStatus == ENTRUST_STATUS::DEAL_PART)
    {
        updateDealList(pOrder);
    }

    //更新委托明细（已成、未成都要更新）
    updateEntrustList(pOrder);
}

void ReportThread::onWithdrawResult(Order* pOrder)
{

}

void ReportThread::updateEntrustList(const Order* pOrder)
{
    //写tentrustlist表

    DbKV values;
    values.insert(DBTABLE::TENTRUSTLIST::COL_ID, QString("%1").arg(pOrder->iInternalId));

    if (pOrder->dtBusinessTime.toSecsSinceEpoch() > 0)
    {
        values.insert(DBTABLE::TENTRUSTLIST::COL_BUSINESS_TIME, pOrder->dtBusinessTime.toString("yyyy-MM-dd hh:mm:ss"));
    }
    else
    {
        values.insert(DBTABLE::TENTRUSTLIST::COL_BUSINESS_TIME, pOrder->dtEntrustTime.toString("yyyy-MM-dd hh:mm:ss"));
    }

    values.insert(DBTABLE::TENTRUSTLIST::COL_ENTRUST_NO, QString("%1").arg(pOrder->iEntrustNo));

    if (pOrder->strEntrustStatus.length() == 0 && pOrder->strFailCause.length() > 0)
    {
        values.insert(DBTABLE::TENTRUSTLIST::COL_ENTRUST_STATUS, ENTRUST_STATUS::FAIL);
    }
    else
    {
        values.insert(DBTABLE::TENTRUSTLIST::COL_ENTRUST_STATUS, pOrder->strEntrustStatus);
    }

    values.insert(DBTABLE::TENTRUSTLIST::COL_ACCOUNT_CODE, pOrder->strAccountCode);
    values.insert(DBTABLE::TENTRUSTLIST::COL_ASSET_NO, pOrder->strAssetNo);
    values.insert(DBTABLE::TENTRUSTLIST::COL_COMBI_NO, pOrder->strCombiNo);
    //values.insert(DBTABLE::TENTRUSTLIST::COL_STOCKHOLDER_ID, "");
    //values.insert(DBTABLE::TENTRUSTLIST::COL_REPORT_SEAT, "");
    values.insert(DBTABLE::TENTRUSTLIST::COL_MARKET_NO, pOrder->strMarketNo);
    values.insert(DBTABLE::TENTRUSTLIST::COL_STOCK_CODE, pOrder->strStockCode);
    values.insert(DBTABLE::TENTRUSTLIST::COL_ENTRUST_DIRECTION, pOrder->strEntrustDirection);
    values.insert(DBTABLE::TENTRUSTLIST::COL_PRICE_TYPE, pOrder->strPriceType);
    values.insert(DBTABLE::TENTRUSTLIST::COL_ENTRUST_PRICE, QString("%1").arg(pOrder->fEntrustPrice));
    values.insert(DBTABLE::TENTRUSTLIST::COL_ENTRUST_AMOUNT, QString("%1").arg(pOrder->fEntrustAmount));
    values.insert(DBTABLE::TENTRUSTLIST::COL_CANCEL_DEAL_AMOUNT, QString("%1").arg(pOrder->fCancelDealAmount));
    values.insert(DBTABLE::TENTRUSTLIST::COL_FAIL_CAUSE, pOrder->strFailCause);
    values.insert(DBTABLE::TENTRUSTLIST::COL_EXT_ACCESS_SYSTEM_ID, QString("%1").arg(pOrder->iExtAccessSystemId));
    values.insert(DBTABLE::TENTRUSTLIST::COL_EXT_SYSTEM_ID, QString("%1").arg(pOrder->iExtSystemId));

    QString strErrMsg;
    bool bRet = m_pOutDbHelper->insertOrReplaceRecord(DBTABLE::TENTRUSTLIST::TABLE, &values, strErrMsg);
    if (!bRet)
    {
        qCritical() << "Insert or replace record in table(" << DBTABLE::TENTRUSTLIST::TABLE  << ")failed: " << strErrMsg;
        emit sigRunLog(RunLog_Error, QStringLiteral("更新或插入表 %1 失败：%2").arg(DBTABLE::TENTRUSTLIST::TABLE).arg(strErrMsg));
    }
}

void ReportThread::updateDealList(const Order* pOrder)
{
    //写tdeallist表
    for (int i = 0; i < pOrder->vecDealDetail.size(); i++)
    {
        Deal* pDeal = pOrder->vecDealDetail[i];

        if (pDeal->bOutput)
        {
            continue;
        }

        DbKV values;
        values.insert(DBTABLE::TDEALLIST::COL_DEAL_TIME, pDeal->dtDealTime.toString("yyyy-MM-dd hh:mm:ss"));
        values.insert(DBTABLE::TDEALLIST::COL_DEAL_NO, pDeal->strDealNo);
        values.insert(DBTABLE::TDEALLIST::COL_ENTRUST_NO, QString("%1").arg(pOrder->iEntrustNo));
        values.insert(DBTABLE::TDEALLIST::COL_ENTRUST_STATUS, pOrder->strEntrustStatus);
        values.insert(DBTABLE::TDEALLIST::COL_ACCOUNT_CODE, pOrder->strAccountCode);
        values.insert(DBTABLE::TDEALLIST::COL_ASSET_NO, pOrder->strAssetNo);
        values.insert(DBTABLE::TDEALLIST::COL_COMBI_NO, pOrder->strCombiNo);
        //values.insert(DBTABLE::TDEALLIST::COL_STOCKHOLDER_ID, "");
        //values.insert(DBTABLE::TDEALLIST::COL_REPORT_SEAT, "");
        values.insert(DBTABLE::TDEALLIST::COL_MARKET_NO, pOrder->strMarketNo);
        values.insert(DBTABLE::TDEALLIST::COL_STOCK_CODE, pOrder->strStockCode);
        values.insert(DBTABLE::TDEALLIST::COL_ENTRUST_DIRECTION, pOrder->strEntrustDirection);
        values.insert(DBTABLE::TDEALLIST::COL_PRICE_TYPE, pOrder->strPriceType);
        values.insert(DBTABLE::TDEALLIST::COL_DEAL_AMOUNT, QString("%1").arg(pDeal->fDealAmount));
        values.insert(DBTABLE::TDEALLIST::COL_DEAL_PRICE, QString("%1").arg(pDeal->fDealPrice));
        values.insert(DBTABLE::TDEALLIST::COL_DEAL_BALANCE, QString("%1").arg(pDeal->fDealBalance));
        values.insert(DBTABLE::TDEALLIST::COL_DEAL_FEE, QString("%1").arg(pDeal->fDealfee));
        values.insert(DBTABLE::TDEALLIST::COL_EXT_ACCESS_SYSTEM_ID, QString("%1").arg(pOrder->iExtAccessSystemId));

        QString strErrMsg;
        bool bRet = m_pOutDbHelper->insertRecord(DBTABLE::TDEALLIST::TABLE, &values, strErrMsg);
        if (bRet)
        {
            pDeal->bOutput = true;
        }
        else
        {
            qCritical() << "Insert record in table(" << DBTABLE::TENTRUSTLIST::TABLE << ")failed: " << strErrMsg;
            emit sigRunLog(RunLog_Error, QStringLiteral("插入表 %1 失败：%2").arg(DBTABLE::TDEALLIST::TABLE).arg(strErrMsg));
        }
    }
}
