#pragma once

#include <QVector>
#include "Constants.h"

//**************************************************
//
// 成交明细
//
//**************************************************
struct Deal
{
    QDateTime dtDealTime;  //成交时间
    QString   strDealNo;   //成交编号
    float     fDealAmount; //成交数量
    float     fDealPrice;  //成交价格
    float     fDealBalance;//成交金额
    float     fDealfee;    //本次费用

    //系统数据
    bool      bDisplay;    //是否已经在成交列表中显示
    bool      bOutput;     //是否已经输出

    Deal();
    void reset();
    void clone(const Deal& o);
    Deal& operator =(const Deal& o);
    QString toQString() const;
};
Q_DECLARE_METATYPE(Deal);
Q_DECLARE_METATYPE(Deal*);
//Q_DECLARE_METATYPE(QVector<Deal*>);

//**************************************************
//
//   订单
//
//**************************************************
struct Order
{
    //内部数据
    EntrustType eEntrustType;   //委托=0，撤单=1
    int iInternalId;            //内部编号，对应tentrust/tentrustlist表中的id字段（主键）
    int hSendHandle;            //消息发送句柄

    //外部接入系统数据
    int iExtAccessSystemId;     //接入系统id

    //委托信息
    QDateTime dtEntrustTime;    //委托时间
    int     iBatchNo;           //委托批号
    QString strAccountCode;     //账户编号
    QString strAccountName;     //账户名称
    QString strAssetNo;         //资产单元编号
    QString strCombiNo;         //单元编码
    QString strCombiName;       //单元名称
    QString strMarketNo;        //交易市场
    QString strMarketName;      //交易市场
    QString strStockCode;       //证券代码
    QString strStockName;       //证券名称
    QString strEntrustDirection;//委托方向
    QString strPriceType;       //价格类型
    float   fEntrustPrice;      //委托价格
    float   fEntrustAmount;     //委托数量
    unsigned int iExtSystemId;  //外部系统id（恒生PB使用，本系统内唯一，8位数）
    int     iRequestOrder;      //篮子委托中的请求次序（暂时不同）

    //撤单信息
    QDateTime dtWithdrawTime;       //撤单时间
    QDateTime dtWithdrawBusinessTime;//撤单申报时间

    //委托结果
    QDateTime dtBusinessTime; //申报时间
    int     iEntrustNo;       //委托序号
    int iCancelEntrustNo;     //撤单委托序号
    QString strReportNo;      //申报编号
    QString strConfirmNo;     //确认编号
    QString strEntrustStatus; //委托状态
    float   fCancelDealAmount;//撤成数量
    int     iCancelSuccessFlag;//撤单结果
    int     iFailCode;        //废单代码
    QString strFailCause;     //废单原因
    int     iRiskSerialNo;    //风控判断流水号

    //成交结果（对于分笔成交来说，以下参数都可能不一样）
    QVector<Deal*> vecDealDetail;

    Order();
    virtual ~Order();
    void reset();
    void clone(const Order& o);
    Order& operator =(const Order& o);
    QString toQString() const ;

    static unsigned int getAutoIncr();

private:
    static unsigned int m_iAutoIncr;
};
Q_DECLARE_METATYPE(Order);
Q_DECLARE_METATYPE(Order*);

typedef QVector<Order*> OrderList;

Q_DECLARE_METATYPE(OrderList*);


//Q_DECLARE_METATYPE(OrderList);


//QT_BEGIN_NAMESPACE                                                 
//template <>                                                        
//struct QMetaTypeId< OrderList >                                         
//{                                                                   
//enum { Defined = 1 };                                          
//    static int qt_metatype_id()                                    
//{                                                          
//static QBasicAtomicInt metatype_id = Q_BASIC_ATOMIC_INITIALIZER(0); 
//if (const int id = metatype_id.loadAcquire())           
//return id;                                         
//const int newId = qRegisterMetaType< OrderList >("OrderList",
//    reinterpret_cast< OrderList *>(quintptr(-1)));
//    metatype_id.storeRelease(newId);                        
//    return newId;                                           
//}                                                          
//};                                                                  
//QT_END_NAMESPACE
