#include "stdafx.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <QDebug>

#include "Xxtea.h"
#include "utils.h"

#define CRYPT_ONE_BYTE

QByteArray Xxtea::encrypt(QByteArray src, QByteArray key)
{
    if (key.length() < 16)
    {
        return QByteArray();
    }

    TEACTX ctx;

    quint32 ulEnc = src.length() + 1024;
    quint8* bufEnc = new quint8[ulEnc];
    memset(bufEnc, 0, ulEnc);

    Encrypt(&ctx,
        (const quint8 *)src.constData(),
        src.length(),
        (const quint8 *)key.constData(),
        (quint8 *)bufEnc, &ulEnc);

    QByteArray dst;
    dst.resize(ulEnc);
    memcpy(dst.data(), bufEnc, ulEnc);

    delete[] bufEnc;

    //qDebug() << "XXTea encrypt:"
    //    << "\r\nsrc:" << src.length()
    //    << Utils::toQString(src).toLatin1().data()
    //    << "\r\nkey:" << key.length()
    //    << Utils::toQString(key).toLatin1().data()
    //    << "\r\ndst:" << dst.length()
    //    << Utils::toQString(dst).toLatin1().data();

    return dst;
}

QByteArray Xxtea::decrypt(QByteArray src, QByteArray key)
{
    if (key.length() < 16)
    {
        return QByteArray();
    }

    TEACTX ctx;

    quint32 ulDec = src.length() + 1024;
    quint8* bufDec = new quint8[ulDec];
    quint32 ulRet;

    memset(bufDec, 0, ulDec);

    ulRet = Decrypt(&ctx,
        (const quint8 *)src.constData(),
        src.length(),
        (const quint8 *)key.constData(),
        (quint8 *)bufDec, &ulDec);

    QByteArray dst;
    if (1== ulRet)
    {
        dst.resize(ulDec);
        memcpy(dst.data(), bufDec, ulDec);
    }

    delete[] bufDec;

    //qDebug() << "XXTea decrypt:"
    //    << "\r\nsrc:" << src.length()
    //    << Utils::toQString(src).toLatin1().data()
    //    << "\r\nkey:" << key.length()
    //    << Utils::toQString(key).toLatin1().data()
    //    << "\r\ndst:" << dst.length()
    //    << Utils::toQString(dst).toLatin1().data();

    //如果key不是16字节的0数组，则再用16字节的0数组解密一次
    //if (dst.length() == 0)
    //{
    //    bool zeroFlag = true;
    //    for (int i = 0; i < key.length(); i++)
    //    {
    //        if (key.at(i) != 0)
    //        {
    //            zeroFlag = false;
    //            break;
    //        }
    //    }

    //    if (!zeroFlag)
    //    {
    //        qDebug() << "Xxtea decrypt failed, try zero key";

    //        static QByteArray zeroKey;
    //        zeroKey.resize(16);
    //        memset(zeroKey.data(), 0, 16);
    //        return Xxtea::decrypt(src, zeroKey);
    //    }
    //}

    return dst;
}


/////////////////////////////////////////////////////////////
quint16 Host2NetShort(quint16 usHost)
{
    const quint16 us = 0x1234 ;
    return ((quint8 *)&us)[0] == 0x12 ? usHost : ((usHost>>8) | (usHost<<8)) ;
}

quint16 Net2HostShort(quint16 usNet)
{
    return Host2NetShort(usNet) ;
}

quint32 Host2NetLong(quint32 ulHost)
{
    const quint16 us = 0x1234 ;
    return ((quint8 *)&us)[0] == 0x12 ? ulHost : (((ulHost>>8) & 0xFF00) |
            ((ulHost<<8) & 0xFF0000) | (ulHost<<24) | (ulHost>>24)) ;
}

quint32 Net2HostLong(quint32 ulHost)
{
    return Host2NetLong(ulHost) ;
}

//TEA加密。v明文8字节。k密钥16字节。w密文输出8字节。
void EnCipher(const quint32 *const v, const quint32 *const k, quint32 *const w)
{
    register quint32
            y = Host2NetLong(v[0]),
            z = Host2NetLong(v[1]),
            a = Host2NetLong(k[0]),
            b = Host2NetLong(k[1]),
            c = Host2NetLong(k[2]),
            d = Host2NetLong(k[3]),
            n = 0x10, /* do encrypt 16 (0x10) times */
            sum = 0,
            delta = 0x9E3779B9; /* 0x9E3779B9 - 0x100000000 = -0x61C88647 */

    while (n-- > 0)
    {
        sum += delta;
        y += ((z << 4) + a) ^ (z + sum) ^ ((z >> 5) + b);
        z += ((y << 4) + c) ^ (y + sum) ^ ((y >> 5) + d);
    }

    w[0] = Net2HostLong(y);
    w[1] = Net2HostLong(z);
}

//TEA解密。v密文8字节。k密钥16字节。w明文输出8字节。
void DeCipher(const quint32 *const v, const quint32 *const k, quint32 *const w)
{
    register quint32
            y = Host2NetLong(v[0]),
            z = Host2NetLong(v[1]),
            a = Host2NetLong(k[0]),
            b = Host2NetLong(k[1]),
            c = Host2NetLong(k[2]),
            d = Host2NetLong(k[3]),
            n = 0x10,
            sum = 0xE3779B90,
            /* why this ? must be related with n value*/
            /* delta=(2^32)*黄金分割数0.618 */
            delta = 0x9E3779B9;

    /* sum = delta<<5, in general sum = delta * n */
    while (n-- > 0)
    {
        z -= ((y << 4) + c) ^ (y + sum) ^ ((y >> 5) + d);
        y -= ((z << 4) + a) ^ (z + sum) ^ ((z >> 5) + b);
        sum -= delta;
    }

    w[0] = Net2HostLong(y);
    w[1] = Net2HostLong(z);
}

quint32 Random(void)
{
    //伪随机，每次程序启动后的随机数都相同的
    quint32 r = (quint32)rand();
    //printf("rand = %d\n", r);
    return 0x08;
    //return 0xdead ;
}

//每次8字节加密
static void EncryptEach8Bytes(TEACTX *pCtx)
{
#ifdef CRYPT_ONE_BYTE
    quint32 i ;
    quint8 *pPlain8, *pPlainPre8, *pCrypt8, *pCryptPre8 ;
    pPlain8 = (quint8 *)pCtx->buf ;
    pPlainPre8 = (quint8 *)pCtx->bufPre ;
    pCrypt8 = (quint8 *)pCtx->pCrypt ;
    pCryptPre8 = (quint8 *)pCtx->pCryptPre ;
    //本轮明文与上一轮的密文异或
    for(i=0; i<8; i++)
        pPlain8[i] ^= pCryptPre8[i] ;
    //再对异或后的明文加密
    EnCipher((quint32 *)pPlain8, (quint32 *)pCtx->pKey, (quint32 *)pCrypt8) ;
    //将加密后的密文与上一轮的明文(其实是上一轮明文与上上轮密文异或结果)异或
    for(i=0; i<8; i++)
        pCrypt8[i] ^= pPlainPre8[i] ;
    //
    for(i=0; i<8; i++)
        pPlainPre8[i] = pPlain8[i] ;
#else
    quint32 *pPlain8, *pPlainPre8, *pCrypt8, *pCryptPre8 ;
    pPlain8 = (quint32 *)pCtx->buf ;
    pPlainPre8 = (quint32 *)pCtx->bufPre ;
    pCrypt8 = (quint32 *)pCtx->pCrypt ;
    pCryptPre8 = (quint32 *)pCtx->pCryptPre ;
    pPlain8[0] ^= pCryptPre8[0] ;
    pPlain8[1] ^= pCryptPre8[1] ;
    EnCipher(pPlain8, (const quint32 *)pCtx->pKey, pCrypt8) ;
    pCrypt8[0] ^= pPlainPre8[0] ;
    pCrypt8[1] ^= pPlainPre8[1] ;
    pPlainPre8[0] = pPlain8[0] ;
    pPlainPre8[1] = pPlain8[1] ;
#endif
    pCtx->pCryptPre = pCtx->pCrypt ;
    pCtx->pCrypt += 8 ;
}

//加密。pPlain指向待加密的明文。ulPlainLen明文长度。pKey密钥16字节。
//pOut指向密文输出缓冲区。pOutLen输入输出参数，指示输出缓冲区长度、密文长度。
quint32 Encrypt(TEACTX *pCtx, const quint8 *pPlain, quint32 ulPlainLen,
               const quint8 *pKey, quint8 *pOut, quint32 *pOutLen)
{
    quint32 ulToPaddingLen, ulPadding, ulOut ;
    const quint8 *p ;
    if(pPlain == NULL || ulPlainLen == 0 || pOutLen == NULL)
        return 0 ;
    //计算需要填充的字节数
    //整个加密流程下来，不管明文长度多少，填充10个字节是固定的，
    //然后再根据明文的长度计算还需要填充的字节数。
    ulToPaddingLen = (8 - ((ulPlainLen + 10) & 0x07)) & 0x07 ;  //先加10个字节，然后再凑足8的整数倍
    //计算加密后的长度
    ulOut = 1 + ulToPaddingLen + 2 + ulPlainLen + 7 ;
    if(*pOutLen < ulOut)
    {
        *pOutLen = ulOut ;
        return 0 ;
    }
    *pOutLen = ulOut ;

    memset(pCtx, 0, sizeof(TEACTX)) ;
    pCtx->pCrypt = pOut ;
    pCtx->pCryptPre = pCtx->bufPre ;
    pCtx->pKey = pKey ;

    //buf[0]的最低3bit位等于所填充的长度，即buf[0]的 0-2位是ulToPaddingLen，3-8是随机数
    pCtx->buf[0] = (quint8)((Random() & 0xF8) | ulToPaddingLen) ;
    
    //用随机数填充上面计算得到的填充长度(每个字节填充的内容是一样的)。
    //这里填充的起始位置是&buf[1]。
    memset(pCtx->buf+1, (quint8)Random(), ulToPaddingLen++) ;

    //至少再填充两字节
    for(ulPadding = 0; ulPadding < 2; ulPadding++)
    {
        //如果已经超过长度了，则不填充，直接进行加密
        if(ulToPaddingLen == 8)
        {
            EncryptEach8Bytes(pCtx) ;
            ulToPaddingLen = 0 ;
        }
        pCtx->buf[ulToPaddingLen++] = (quint8)Random() ;
    }

    p = pPlain ;
    while(ulPlainLen > 0)
    {
        if(ulToPaddingLen == 8)
        {
            EncryptEach8Bytes(pCtx) ;
            ulToPaddingLen = 0 ;
        }
        pCtx->buf[ulToPaddingLen++] = *(p++) ;
        ulPlainLen-- ;
    }
    //末尾再添加7字节0后加密，在解密过程的时候可以用来判断key是否正确。
    for(ulPadding=0; ulPadding<7; ulPadding++)
        pCtx->buf[ulToPaddingLen++] = 0x00 ;
    //
    EncryptEach8Bytes(pCtx) ;


    return ulOut ;
}

//每次8字节进行解密
static void DecryptEach8Bytes(TEACTX *pCtx)
{
#ifdef CRYPT_ONE_BYTE
    
    quint32 i ;
    quint8 bufTemp[8] ;
    quint8 *pBuf8, *pBufPre8, *pCrypt8, *pCryptPre8 ;
    pBuf8 = (quint8 *)pCtx->buf ;
    pBufPre8 = (quint8 *)pCtx->bufPre ;
    pCrypt8 = (quint8 *)pCtx->pCrypt ;
    pCryptPre8 = (quint8 *)pCtx->pCryptPre ;
    //当前的密文与前一轮明文(实际是前一轮明文与前前轮密文异或结果)异或
    for (i = 0; i < 8; i++)
    {
        bufTemp[i] = pCrypt8[i] ^ pBufPre8[i];
    }

    //异或后的结果再解密(解密后得到当前名文与前一轮密文异或的结果，并非真正明文)

    DeCipher((quint32 *)bufTemp, (quint32 *)pCtx->pKey, (quint32 *)pBufPre8) ;


    //解密后的结果与前一轮的密文异或，得到真正的明文
    for (i = 0; i < 8; i++)
    {
        pBuf8[i] = pBufPre8[i] ^ pCryptPre8[i];
    }

#else
    quint32 bufTemp[2] ;
    quint32 *pBuf8, *pBufPre8, *pCrypt8, *pCryptPre8 ;
    pBuf8 = (quint32 *)pCtx->buf ;
    pBufPre8 = (quint32 *)pCtx->bufPre ;
    pCrypt8 = (quint32 *)pCtx->pCrypt ;
    pCryptPre8 = (quint32 *)pCtx->pCryptPre ;
    bufTemp[0] = pCrypt8[0] ^ pBufPre8[0] ;
    bufTemp[1] = pCrypt8[1] ^ pBufPre8[1] ;
    DeCipher(bufTemp, (const quint32 *)pCtx->pKey, pBufPre8) ;
    pBuf8[0] = pBufPre8[0] ^ pCryptPre8[0] ;
    pBuf8[1] = pBufPre8[1] ^ pCryptPre8[1] ;
#endif
    pCtx->pCryptPre = pCtx->pCrypt ;
    pCtx->pCrypt += 8 ;
}

//解密。pCipher指向待解密密文。ulCipherLen密文长度。pKey密钥16字节。
//pOut指向明文输出缓冲区。pOutLen输入输出参数，指示输出缓冲区长度、明文长度。
quint32 Decrypt(TEACTX *pCtx, const quint8 *pCipher, quint32 ulCipherLen,
               const quint8 *pKey, quint8 *pOut, quint32 *pOutLen)
{

    quint32 ulPos, ulPadding, ulOut, ul ;

    // 待解密的数据长度最少16字节，并且长度满足是8的整数倍。
    if (pCipher == NULL || pOutLen == NULL || ulCipherLen < 16 || (ulCipherLen & 0x07) != 0)
    {
        return 0;
    }

    // 先解密头8字节，以便获取第一轮加密时填充的长度。
    pCtx->pKey = pKey;
    DeCipher((const quint32 *)pCipher, (const quint32 *)pKey, (quint32 *)pCtx->bufPre) ;
    for (ul = 0; ul < 8; ul++)
    {
        pCtx->buf[ul] = pCtx->bufPre[ul];
    }

    ulPos = pCtx->buf[0] & 0x07 ; //第一轮加密时填充的长度

    //TODO 不知道为何要加这一段？？？？？？？对接收到的消息进行解密的时候会在这里返回！！！！！
    //if(ulPos > 1)
    //{
    //    for(ulOut=2; ulOut<=ulPos; ulOut++)
    //    {
    //        if(pCtx->buf[1] != pCtx->buf[ulOut])
    //        {
    //            *pOutLen = 0 ;
    //            return 0 ; //解密失败
    //        }
    //    }
    //}

    ulOut = ulCipherLen - ulPos - 10 ;
    if (ulPos + 10 > ulCipherLen || *pOutLen < ulOut)
    {
        return 0;
    }

    pCtx->pCryptPre = (quint8 *)pCipher ;
    pCtx->pCrypt = (quint8 *)pCipher + 8 ;
    ulPos++ ;

    for(ulPadding = 0; ulPadding < 2; ulPadding++)
    {
        if(ulPos == 8)
        {
            DecryptEach8Bytes(pCtx) ;
            ulPos = 0 ;
        }
        ulPos++ ;
    }
    //
    for(ul = 0; ul < ulOut; ul++)
    {
        if(ulPos == 8)
        {
            DecryptEach8Bytes(pCtx) ;
            ulPos = 0 ;
        }
        pOut[ul] = pCtx->buf[ulPos] ;
        ulPos++ ;
    }
    //
    for(ulPadding = 0; ulPadding < 7; ulPadding++)
    {
        if(ulPos < 8)
        {
            if(pCtx->buf[ulPos] != 0x00)
            {
                *pOutLen = 0 ;
                return 0 ;
            }
        }
        ulPos++ ;
    }
    *pOutLen = ulOut ;
    return 1 ;
}

void PrintBuffer(const quint8 *buf, quint32 ulLen)
{
    quint32 i ;
    for(i=0; i<ulLen; i++)
    {
        printf("%.2X ", buf[i]) ;
        if((i+1) % 16 == 0)
            putchar('\n') ;
    }
    if((ulLen & 0x0F) != 0)
        putchar('\n') ;
}

//int main(void)
//{
//    const char *pPK[][2] =
//    {
//        //明文--密钥
//        {"tea",     "123456789abcdef1"},
//        {"tea",     "123456789abcdef1"},
//        {"tea",     "123456789abcdef1"},
//        {"123456",  "password1234567"},
//        {"AABBCCD", "aabbccddeeffggh"},
//        {"Hello World 你好世界！", "aabbccddeeffggh"}
//    } ;
//    TEACTX ctx ;
//    quint8 bufEnc[512], bufDec[512] ;
//    quint32 ulEnc, ulDec, ulRet ;
//    int i ;
//    int len = sizeof(pPK)/sizeof(pPK[0]);
//    for(i=0; i<len; i++)
//    {
//        printf("明文：%s\n密钥：%s\n", pPK[i][0], pPK[i][1]) ;
//        ulEnc = sizeof(bufEnc) ;
//        Encrypt(&ctx, (const quint8 *)pPK[i][0], strlen(pPK[i][0])+1,
//                (const quint8 *)pPK[i][1], (quint8 *)bufEnc, &ulEnc) ;
//        printf("密文：\n") ;
//        PrintBuffer(bufEnc, ulEnc) ;
//        ulDec = sizeof(bufDec) ;
//        ulRet = Decrypt(&ctx, bufEnc, ulEnc, (const quint8 *)pPK[i][1],
//                (quint8 *)bufDec, &ulDec) ;
//        if(ulRet != 0)
//            printf("解密后明文：%s\n", bufDec) ;
//        else
//            printf("解密失败！\n") ;
//        putchar('\n') ;
//    }
//    return 0 ;
//}
