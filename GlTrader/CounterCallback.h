#pragma once

#include "hundsun/t2sdk_interface.h"

class Context;

class CAutoKnown
{
public:
    CAutoKnown(IKnown* ptr)
        : m_pPtr(ptr)
    {
        if (m_pPtr != nullptr)
        {
            m_pPtr->AddRef();
        }
    }

    virtual ~CAutoKnown()
    {
        m_pPtr->Release();
    }

private:
    IKnown* m_pPtr;
};

class CMsgCallback : public QObject, public CCallbackInterface
{
    Q_OBJECT

public:
    CMsgCallback(Context* pContext);

    // 因为CCallbackInterface的最终纯虚基类是IKnown，所以需要实现一下这3个方法
    unsigned long  FUNCTION_CALL_MODE QueryInterface(const char *iid, IKnown **ppv);
    unsigned long  FUNCTION_CALL_MODE AddRef();
    unsigned long  FUNCTION_CALL_MODE Release();

    // 各种事件发生时的回调方法，实际使用时可以根据需要来选择实现，对于不需要的事件回调方法，可直接return
    // Reserved?为保留方法，为以后扩展做准备，实现时可直接return或return 0。
    void FUNCTION_CALL_MODE OnConnect(CConnectionInterface *lpConnection);
    void FUNCTION_CALL_MODE OnSafeConnect(CConnectionInterface *lpConnection);
    void FUNCTION_CALL_MODE OnRegister(CConnectionInterface *lpConnection);
    void FUNCTION_CALL_MODE OnClose(CConnectionInterface *lpConnection);
    void FUNCTION_CALL_MODE OnSent(CConnectionInterface *lpConnection, int hSend, void *reserved1, void *reserved2, int nQueuingData);
    void FUNCTION_CALL_MODE Reserved1(void *a, void *b, void *c, void *d);
    void FUNCTION_CALL_MODE Reserved2(void *a, void *b, void *c, void *d);
    int  FUNCTION_CALL_MODE Reserved3();
    void FUNCTION_CALL_MODE Reserved4();
    void FUNCTION_CALL_MODE Reserved5();
    void FUNCTION_CALL_MODE Reserved6();
    void FUNCTION_CALL_MODE Reserved7();
    void FUNCTION_CALL_MODE OnReceivedBiz(CConnectionInterface *lpConnection, int hSend, const void *lpUnPackerOrStr, int nResult);
    void FUNCTION_CALL_MODE OnReceivedBizEx(CConnectionInterface *lpConnection, int hSend, LPRET_DATA lpRetData, const void *lpUnpackerOrStr, int nResult);
    void FUNCTION_CALL_MODE OnReceivedBizMsg(CConnectionInterface *lpConnection, int hSend, IBizMessage* lpMsg);

signals:
    void sigMsgCallback(CConnectionInterface* lpConnection, int hSend, IBizMessage* lpMsg);
    void sigConnCreated(quint32 ptr);
    void sigConnClosed(quint32 ptr);

private:
    Context* m_pContext;
};

Q_DECLARE_METATYPE(LPSUBSCRIBE_RECVDATA);

class CPushCallback : public QObject, public CSubCallbackInterface
{
    Q_OBJECT

public:
    CPushCallback(Context* pContext);

    unsigned long  FUNCTION_CALL_MODE QueryInterface(const char *iid, IKnown **ppv);
    unsigned long  FUNCTION_CALL_MODE AddRef();
    unsigned long  FUNCTION_CALL_MODE Release();

    void FUNCTION_CALL_MODE OnReceived(CSubscribeInterface *lpSub, int subscribeIndex, const void *lpData, int nLength, LPSUBSCRIBE_RECVDATA lpRecvData);
    void FUNCTION_CALL_MODE OnRecvTickMsg(CSubscribeInterface *lpSub, int subscribeIndex, const char* TickMsgInfo);

signals:
    void sigPushCallback(CSubscribeInterface* lpSub, int subscribeIndex, QByteArray buf);

private:
    Context* m_pContext;
};
