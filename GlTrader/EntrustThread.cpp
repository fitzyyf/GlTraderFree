#include "stdafx.h"
#include "EntrustThread.h"
#include "Constants.h"
#include "Utils.h"
#include "DbHelper.h"
#include "Context.h"

EntrustThread::EntrustThread(ConfigData configData, QObject *parent)
    : QThread(parent)
    , m_ConfigData(configData)
    , m_pEntrustMonitorTimer(nullptr)
    , m_pWithdrawMonitorTimer(nullptr)
    , m_iMonitorInterval(configData.iInDbMonitorInterval)
    , m_iMaxEntrustId(0)
    , m_iMaxWithdrawId(0)
    , m_pInDbHelper(nullptr)
    , m_bRun(false)
    //, m_bFirstMonitorEntrust(true)
    //, m_bFirstMonitorWithdraw(true)
{
    moveToThread(this);
}


EntrustThread::~EntrustThread()
{
    this->stopMonitor();

    SAFE_DELETE(m_pEntrustMonitorTimer);
    SAFE_DELETE(m_pWithdrawMonitorTimer);

    if (m_pInDbHelper)
    {
        m_pInDbHelper->close();
    }
    SAFE_DELETE(m_pInDbHelper);
}

void EntrustThread::run()
{
    exec();
}

void EntrustThread::setMonitorInterval(int iInterval)
{
    if (m_bRun && m_iMonitorInterval != iInterval)
    {
        stopTimer();
        startTimer();
    }
}

void EntrustThread::setMaxId(int iMaxEntrustId, int iMaxWithdrawId)
{
    m_iMaxEntrustId = iMaxEntrustId;
    m_iMaxWithdrawId = iMaxWithdrawId;
}

void EntrustThread::startMonitor()
{
    if (m_pInDbHelper == nullptr)
    {
        m_pInDbHelper = new DbHelper;
    }

    if (!m_pInDbHelper->open(Utils::getInDbName().toUtf8().data(), true))
    {
        qCritical() << "Open database failed: " << Utils::getInDbName();
        emit sigRunLog(RunLog_Error, QStringLiteral("无法打开数据库文件：%1").arg(Utils::getInDbName()));

        SAFE_DELETE(m_pInDbHelper);
        return;
    }

    ////程序启动后第一次载入委托和撤单，要在定时器启动前
    //if (m_bFirstMonitorEntrust)
    //{
    //    this->monitorEntrust();
    //    m_bFirstMonitorEntrust = false;
    //}

    //if (m_bFirstMonitorWithdraw)
    //{
    //    this->monitorWithdraw();
    //    m_bFirstMonitorWithdraw = false;
    //}

    this->startTimer();

    m_bRun = true;
}

void EntrustThread::stopMonitor()
{
    m_bRun = false;

    this->stopTimer();

    if (m_pInDbHelper)
    {
        m_pInDbHelper->close();
    }
    SAFE_DELETE(m_pInDbHelper);
}

void EntrustThread::startTimer()
{
    //委托监控
    {
        if (m_pEntrustMonitorTimer == nullptr)
        {
            m_pEntrustMonitorTimer = new QTimer;
            QObject::connect(m_pEntrustMonitorTimer, &QTimer::timeout, this, &EntrustThread::monitorEntrust);
        }

        if (m_pEntrustMonitorTimer->isActive())
        {
            qDebug() << "Monitor entrust timer has been actived";
            return;
        }

        qDebug() << "Start monitor entrust, interval = " << m_iMonitorInterval << " ms";

        //再设置定时器
        m_pEntrustMonitorTimer->start(m_iMonitorInterval);
    }

    //撤单监控
    {
        if (m_pWithdrawMonitorTimer == nullptr)
        {
            m_pWithdrawMonitorTimer = new QTimer;
            QObject::connect(m_pWithdrawMonitorTimer, &QTimer::timeout, this, &EntrustThread::monitorWithdraw);
        }

        if (m_pWithdrawMonitorTimer->isActive())
        {
            qDebug() << "Monitor withdraw timer has been actived";
            return;
        }

        qDebug() << "Start monitor withdraw, interval = " << m_iMonitorInterval << " ms";

        //再设置定时器
        m_pWithdrawMonitorTimer->start(m_iMonitorInterval);
    }

}

void EntrustThread::stopTimer()
{
    {
        if (m_pEntrustMonitorTimer == nullptr)
        {
            qWarning() << "Monitor entrust timer has been stopped";
            return;
        }

        qDebug() << "Stop monitor entrust";

        m_pEntrustMonitorTimer->stop();
        SAFE_DELETE(m_pEntrustMonitorTimer);
    }

    {
        if (m_pWithdrawMonitorTimer == nullptr)
        {
            qWarning() << "Monitor withdraw timer has been stopped";
            return;
        }

        qDebug() << "Stop monitor withdraw";

        m_pWithdrawMonitorTimer->stop();
        SAFE_DELETE(m_pWithdrawMonitorTimer);
    }

}

void EntrustThread::monitorEntrust()
{
    //查询最新的订单，篮子委托不超过500条
    //select * from tentrust where id > m_iMaxEntrustId
    QString sql = QString("select * from %1 where %2 > %3 order by %4 asc limit 500")
        .arg(DBTABLE::TENTRUST::TABLE)
        .arg(DBTABLE::TENTRUST::COL_ID)
        .arg(m_iMaxEntrustId)
        .arg(DBTABLE::TENTRUST::COL_ID);
    int cnt = m_pInDbHelper->getRecordBySql(sql.toUtf8().data(), &m_entrustRecordSet);
    if (cnt < 0)
    {
        qCritical() << "Query table error: " << DBTABLE::TENTRUST::TABLE;
        emit sigRunLog(RunLog_Error, QStringLiteral("查询表失败：%1").arg(DBTABLE::TENTRUST::TABLE));
        return;
    }
    else if (cnt == 0)
    {
        return;
    }

    qDebug() << "Found " << cnt << " new orders";
    //if (m_bFirstMonitorEntrust)
    //{
    //    emit sigRunLog(RunLog_Info, QString(QStringLiteral("载入 %1 条待处理新订单委托").arg(cnt)));
    //}

    SAFE_NEW(OrderList, pOrderList);
    for (unsigned i = 0; i < m_entrustRecordSet.size(); i++)
    {
        DbKVPtr pRecord = m_entrustRecordSet.at(i);
        SAFE_NEW(Order, pOrder);

        pOrder->eEntrustType = EntrustType::Entrust;
        pOrder->iInternalId = (*pRecord)[DBTABLE::TENTRUST::COL_ID].toInt();
        pOrder->dtEntrustTime = QDateTime::fromString((*pRecord)[DBTABLE::TENTRUST::COL_ENTRUST_TIME], "yyyy-MM-dd hh:mm:ss");
        pOrder->iBatchNo = (*pRecord)[DBTABLE::TENTRUST::COL_BATCH_NO].toInt();
        pOrder->strAccountCode = (*pRecord)[DBTABLE::TENTRUST::COL_ACCOUNT_CODE];
        pOrder->strAssetNo = (*pRecord)[DBTABLE::TENTRUST::COL_ASSET_NO];
        pOrder->strCombiNo = (*pRecord)[DBTABLE::TENTRUST::COL_COMBI_NO];
        pOrder->strMarketNo = (*pRecord)[DBTABLE::TENTRUST::COL_MARKET_NO];
        pOrder->strStockCode = (*pRecord)[DBTABLE::TENTRUST::COL_STOCK_CODE];
        pOrder->strEntrustDirection = (*pRecord)[DBTABLE::TENTRUST::COL_ENTRUST_DIRECTION];
        pOrder->strPriceType = (*pRecord)[DBTABLE::TENTRUST::COL_PRICE_TYPE];
        pOrder->fEntrustPrice = (*pRecord)[DBTABLE::TENTRUST::COL_ENTRUST_PRICE].toFloat();
        pOrder->fEntrustAmount = (*pRecord)[DBTABLE::TENTRUST::COL_ENTRUST_AMOUNT].toFloat();
        pOrder->iExtAccessSystemId = (*pRecord)[DBTABLE::TENTRUST::COL_EXT_ACCESS_SYSTEM_ID].toInt();
        pOrder->strEntrustStatus = ENTRUST_STATUS::UNACK;

        pOrderList->push_back(pOrder);

        if (pOrder->iInternalId > m_iMaxEntrustId)
        {
            m_iMaxEntrustId = pOrder->iInternalId;
        }
    }
    for (unsigned i = 0; i < m_entrustRecordSet.size(); i++)
    {
        SAFE_DELETE(m_entrustRecordSet[i]);
    }
    m_entrustRecordSet.clear();


    if (pOrderList->size() > 0)
    {
        emit sigNewEntrust(pOrderList);
    }
}

void EntrustThread::monitorWithdraw()
{
    //查询最新的撤单申请
    //select * from twithdraw where id > m_iMaxWithdrawId
    QString sql = QString("select * from %1 where %2 > %3  order by %4 asc limit 500")
        .arg(DBTABLE::TWITHDRAW::TABLE)
        .arg(DBTABLE::TWITHDRAW::COL_ID)
        .arg(m_iMaxWithdrawId)
        .arg(DBTABLE::TWITHDRAW::COL_ID);
    int cnt = m_pInDbHelper->getRecordBySql(sql.toUtf8().data(), &m_withdrawRecordSet);
    if (cnt < 0)
    {
        qCritical() << "Query table error: " << DBTABLE::TWITHDRAW::TABLE;
        emit sigRunLog(RunLog_Error, QStringLiteral("查询表失败：%1").arg(DBTABLE::TWITHDRAW::TABLE));
        return;
    }
    else if (cnt == 0)
    {
        return;
    }

    qDebug() << "Found " << cnt << " new withdraw orders";
    //if (m_bFirstMonitorWithdraw)
    //{
    //    emit sigRunLog(RunLog_Info, QString(QStringLiteral("载入 %1 条待处理撤单委托").arg(cnt)));
    //}

    SAFE_NEW(OrderList, pOrderList);
    for (unsigned i = 0; i < m_withdrawRecordSet.size(); i++)
    {
        DbKVPtr pRecord = m_withdrawRecordSet.at(i);
        SAFE_NEW(Order, pOrder);

        pOrder->eEntrustType = EntrustType::Withdraw;
        pOrder->iInternalId = (*pRecord)[DBTABLE::TWITHDRAW::COL_ID].toInt();
        pOrder->dtWithdrawTime = QDateTime::fromString((*pRecord)[DBTABLE::TWITHDRAW::COL_ENTRUST_TIME], "yyyy-MM-dd hh:mm:ss");
        pOrder->iEntrustNo = (*pRecord)[DBTABLE::TWITHDRAW::COL_ENTRUST_NO].toInt();

        pOrderList->push_back(pOrder);
        m_withdrawOrderMap.insert(pOrder->iEntrustNo, pOrder);

        if (pOrder->iInternalId > m_iMaxWithdrawId)
        {
            m_iMaxWithdrawId = pOrder->iInternalId;
        }

        //根据此iEntrustNo到CounterThread中查找原始订单，然后再撤单
        //emit sigQueryOrderByEntrustNo(pOrder);
    }

    for (unsigned i = 0; i < m_withdrawRecordSet.size(); i++)
    {
        SAFE_DELETE(m_withdrawRecordSet[i]);
    }
    m_withdrawRecordSet.clear();

    if (pOrderList->size() > 0)
    {
        emit sigQueryOrderByEntrustNo(pOrderList);
    }
}

void EntrustThread::onQueryOrderByEntrustNoResult(OrderList* pOrignOrderList)
{
    if (nullptr == pOrignOrderList)
    {
        return;
    }

    SAFE_NEW(OrderList, pOrderList);
    for (int i = 0; i < pOrignOrderList->size(); i++)
    {
        Order* pOrignOrder = (*pOrignOrderList)[i];
        if (nullptr == pOrignOrder)
        {
            continue;
        }

        auto it = m_withdrawOrderMap.find(pOrignOrder->iEntrustNo);
        if (it != m_withdrawOrderMap.end())
        {
            //根据原委托复制一份撤单委托
            Order* pOrder = it.value();

            int iInternalId = pOrder->iInternalId;
            pOrignOrder->dtWithdrawTime = pOrder->dtWithdrawTime;

            pOrder->clone(*pOrignOrder);
            pOrder->iInternalId = iInternalId;
            pOrder->eEntrustType = EntrustType::Withdraw;
            pOrder->strEntrustStatus = ENTRUST_STATUS::UNACK;

            m_withdrawOrderMap.erase(it);

            pOrderList->push_back(pOrder);
        }
        else
        {
            qCritical() << "Can't find withdraw order: iEntrustNo = " << pOrignOrder->iEntrustNo;
            emit sigRunLog(RunLog_Error, QStringLiteral("查询撤单原委托序号失败（委托序号：%1）").arg(pOrignOrder->iEntrustNo));
        }

    }

    SAFE_DELETE(pOrignOrderList);

    if (pOrderList->size() > 0)
    {
        emit sigNewWithdraw(pOrderList);
    }
    else
    {
        SAFE_DELETE(pOrderList);
    }

}