#pragma once

#include <QtWidgets/QDialog>
#include "ui_About.h"

class DlgAbout : public QDialog
{
    Q_OBJECT

public:
    DlgAbout(QWidget *parent = Q_NULLPTR);

protected:
    void mousePressEvent(QMouseEvent *e);

private:
    Ui::DlgAbout ui;
};
