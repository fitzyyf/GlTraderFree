#include "stdafx.h"
#include "Context.h"
#include "OrderMgr.h"
#include "Utils.h"

Context::Context()
{
    reset();
}


Context::~Context()
{
    //m_orderMap
    for (auto it = m_orderMap.begin(); it != m_orderMap.end(); it++)
    {
        SAFE_DELETE(it.value());
    }
    m_orderMap.clear();

    //m_dictMap
    for (auto it = m_dictMap.begin(); it != m_dictMap.end(); it++)
    {
        SAFE_DELETE(it.value());
    }
    m_dictMap.clear();
}

void Context::reset()
{
    m_strUsername.clear();
    m_strPassword.clear();
    m_strUserToken.clear();

    m_strIp.clear();
    m_strMac.clear();

    m_strAccountCode.clear();
    m_strCombNo.clear();
    m_iPacketId = 0;

    m_orderMap.clear();
    m_orderList.clear();
    m_orderMapEntrustNo.clear();
    m_unackOrderMap.clear();

    m_dictMap.clear();
    m_runConfigMap.clear();
}

QString Context::getUsername() const
{
    return m_strUsername;
}

void Context::setUsername(const QString& strUsername)
{
    m_strUsername = strUsername;
}

QString Context::getPassword() const
{
    return m_strPassword;
}

void Context::setPassword(const QString& strPassword)
{
    m_strPassword = strPassword;
}

QString Context::getUserToken() const
{
    return m_strUserToken;
}

void Context::setUserToken(const QString& strUserToken)
{
    m_strUserToken = strUserToken;
}

QString Context::getIp() const
{
    return m_strIp;
}

void Context::setIp(const QString& strIp)
{
    m_strIp = strIp;
}

QString Context::getMac() const
{
    return m_strMac;
}

void Context::setMac(const QString& strMac)
{
    m_strMac = strMac;
}

QString Context::getHDSerialId() const
{
    return m_strHDSerialId;
}

void Context::setHdSerialNo(const QString& strHdSerialNo)
{
    m_strHDSerialId = strHdSerialNo;
}

QString Context::getT2Password() const
{
    return m_strT2Password;
}

void Context::setT2Password(const QString& strT2Password)
{
    m_strT2Password = strT2Password;
}

QString Context::getAccountCode() const
{
    return m_strAccountCode;
}

void Context::setAccountCode(const QString& strAccountCode)
{
    m_strAccountCode = strAccountCode;
}

QString Context::getCombNo() const
{
    return m_strCombNo;
}

void Context::setCombNo(const QString& strCombNo)
{
    m_strCombNo = strCombNo;
}

int Context::getNewPacketId()
{
    return ++m_iPacketId;
}

//QVector<Order*>& Context::getOrderList()
//{
//    return m_orderList;
//}

Order* Context::getOrder(int iExtSystemId)
{
    auto it = m_orderMap.find(iExtSystemId);
    if (it == m_orderMap.end())
    {
        return nullptr;
    }
    else
    {
        return it.value();
    }
}

Order* Context::getOrderByEntrustNo(int iEntrustNo)
{
    auto it = m_orderMapEntrustNo.find(iEntrustNo);
    if (it == m_orderMapEntrustNo.end())
    {
        return nullptr;
    }
    else
    {
        return it.value();
    }
}

void Context::addOrder(Order* pOrder)
{
    m_orderMap.insert(pOrder->iExtSystemId, pOrder);
    m_orderList.push_back(pOrder);
}

void Context::addOrderByEntrustNo(Order* pOrder)
{
    if (pOrder->iEntrustNo > 0)
    {
        m_orderMapEntrustNo.insert(pOrder->iEntrustNo, pOrder);
    }
}

OrderList* Context::getUnackOrderList(int hSendHandle)
{
    auto it = m_unackOrderMap.find(hSendHandle);
    if (it == m_unackOrderMap.end())
    {
        return nullptr;
    }
    else
    {
        return it.value();
    }
}

void Context::setOrderAcked(int hSendHandle)
{
    auto it = m_unackOrderMap.find(hSendHandle);
    if (it != m_unackOrderMap.end())
    {
        delete it.value();
        m_unackOrderMap.remove(hSendHandle);
    }
}

void Context::addUnackOrder(Order* pOrder)
{
    auto it = m_unackOrderMap.find(pOrder->hSendHandle);
    if (it == m_unackOrderMap.end())
    {
        //SAFE_NEW(QVector<Order*>, pVec);
        QVector<Order*>* pVec = new QVector<Order*>;
        pVec->push_back(pOrder);
        m_unackOrderMap.insert(pOrder->hSendHandle, pVec);

    }
    else
    {
        it.value()->push_back(pOrder);
    }
}

QVector<Order*>& Context::getUntreatedOrderList() 
{ 
    return m_untreatedOrderList; 
}

void Context::addUntreatedOrder(Order* pOrder)
{
    m_untreatedOrderList.push_back(pOrder);
}

void Context::clearUntreatedOrders()
{
    m_untreatedOrderList.clear();
}

QString Context::queryDict(int iDictId, const QString& strItem)
{
    auto it = m_dictMap.find(iDictId);
    if (it == m_dictMap.end())
    {
        return strItem;
    }
    else
    {
        auto it2 = it.value()->find(strItem);
        if (it2 == it.value()->end())
        {
            return strItem;
        }
        else
        {
            return it2.value();
        }
    }

    return strItem;
}

QMap<QString, QString>* Context::queryDict(int iDictId)
{
    auto it = m_dictMap.find(iDictId);
    if (it == m_dictMap.end())
    {
        auto pMap = new QMap<QString, QString>;
        m_dictMap.insert(iDictId, pMap);

        return pMap;
    }
    else
    {
        return it.value();
    }
}

void Context::addDict(int iDictId, const QString& strItem, const QString& strRemark)
{
    auto it = m_dictMap.find(iDictId);
    if (it == m_dictMap.end())
    {
        auto pMap = new QMap<QString, QString>;
        m_dictMap.insert(iDictId, pMap);

        pMap->insert(strItem, strRemark);
    }
    else
    {
        it.value()->insert(strItem, strRemark);
    }
}

QString Context::queryRunConfig(const QString& strItem)
{
    auto it = m_runConfigMap.find(strItem);
    if (it != m_runConfigMap.end())
    {
        return it.value();
    }
    else
    {
        return "";
    }
}

void Context::addRunConfig(const QString& strItem, const QString& strValue)
{
    m_runConfigMap.insert(strItem, strValue);
}
