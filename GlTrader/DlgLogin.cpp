#include "stdafx.h"

#include "DlgLogin.h"
#include "DlgMain.h"
#include "Utils.h"
#include "CounterThread.h"
#include "ReportThread.h"
#include "Constants.h"
#include "Config.h"
#include "Xxtea.h"
#include "DbLoader.h"
#include "Context.h"
#include "IconHelper.h"
#include "DlgMsgBox.h"

DlgLogin::DlgLogin(QWidget *parent)
    : QDialog(parent)
{
    m_bReleaseCounterThread = true;
    ConfigMgr::newInstance(CONSTANT::CONFIG_FILE_NAME);
    m_pDlgMain = nullptr;
    m_pContext = new Context;

    //ConfigData是值传递，所以要提前load
    ConfigMgr::getInstance()->load();

    m_pReportThread = new ReportThread(ConfigMgr::getData());
    m_pCounterThread = new CounterThread(ConfigMgr::getData(), m_pContext);

    ui.setupUi(this);

    Utils::initLog();
    Utils::setLogLevel((LogLevel)ConfigMgr::getData().iLogLevel);
    qInfo() << "Program start, build time:" << Utils::getBuildTime().toString("yyyy-MM-dd hh:mm:ss");

    this->initUi();
    this->initSignalSlot();
    this->initData();
    this->initThread();
}

DlgLogin::~DlgLogin()
{
    DbLoader::destoryInstance();

    SAFE_QUIT_THREAD(m_pCounterThread);
    SAFE_QUIT_THREAD(m_pReportThread);

    ConfigMgr::destoryInstance();

    SAFE_DELETE(m_pContext);
}

void DlgLogin::initUi()
{
    //设置窗体标题栏隐藏
    this->setWindowFlags(Qt::FramelessWindowHint | Qt::WindowSystemMenuHint | Qt::WindowMinMaxButtonsHint);
    m_recWinLocation = this->geometry();
    m_bWinMax = false;
    m_bMousePressed = false;

    IconHelper::Instance()->SetIcon(ui.btnClose, QChar(0xf00d), 10);

    //版本号
    ui.lblVersion->setText(QStringLiteral("版本：") + Utils::toQString(CONSTANT::APP_VERSION) + " " + Utils::toQString(CONSTANT::VERSION_TYPE));

    //用户名密码
    QString strUsername = ConfigMgr::getData().strUsername;
    ui.edtUsername->setText(strUsername);

    if (ConfigMgr::getData().bRememberPassword)
    {
        QByteArray aPassword;
        aPassword = ConfigMgr::getData().arrPassword;
        ui.edtPassword->setText(QString(Xxtea::decrypt(aPassword)));
    }

    //登录
    ui.btnLogin->setEnabled(false);

    ui.chkRememberPassword->setChecked(ConfigMgr::getData().bRememberPassword);
}

void DlgLogin::initSignalSlot()
{
    //ui
    connect(ui.btnLogin, SIGNAL(clicked()), this, SLOT(login()));
    connect(ui.btnClose, SIGNAL(clicked()), this, SLOT(close()));
    connect(ui.lblVerfyCode, SIGNAL(sigVerifyCodeUpdated(QString)), this, SLOT(onVerifyCodeUpdated(QString)));

    //ui->柜台线程
    connect(this, SIGNAL(sigLogin(QString, QString)), m_pCounterThread, SLOT(onLogin(QString, QString)));
    //connect(this, SIGNAL(sigLogout()), m_pCounterThread, SLOT(onLogout()));

    //柜台线程->ui
    connect(m_pCounterThread, SIGNAL(sigLoginResult(int, QString)), this, SLOT(onLoginResult(int, QString)));
    connect(m_pCounterThread, SIGNAL(sigLogining(QString)), this, SLOT(onLogining(QString)));

    //柜台线程->回报线程（必须要在订阅主题之前调用）
    //包括委托和撤单
    connect(m_pCounterThread, SIGNAL(sigEntrustResult(Order*)), m_pReportThread, SLOT(onEntrustResult(Order*)));
}

void DlgLogin::initThread()
{
    m_pReportThread->start();
    m_pCounterThread->start();
}

void DlgLogin::initData()
{
    //TODO 改为在后台线程执行
    DbLoader::getInstance()->setContext(m_pContext);
    QString strErrMsg;
    bool ret = DbLoader::getInstance()->initialize(strErrMsg);
    if (!ret)
    {
        ui.lblStatusMessage->setText(strErrMsg);
        ui.btnLogin->setEnabled(false);
    }
    else
    {
        ui.lblStatusMessage->setText(QStringLiteral("XX证券股份有限公司"));
        ui.btnLogin->setEnabled(true);
    }

    if (ConfigMgr::getData().strServers.length() == 0)
    {
        this->onLoginResult(ERROR_CODE::FAIL, QStringLiteral("无法获取UFX服务端地址，请检查") + CONSTANT::CONFIG_FILE_NAME);
        return;
    }
}

void DlgLogin::login()
{
    QString strUsername = ui.edtUsername->text();
    QString strPassword = ui.edtPassword->text();
    QString strVerify = ui.edtVerifyCode->text().trimmed();
    
    if (strVerify.length() == 0)
    {
        DlgMsgBox* pMsgBox = new DlgMsgBox;
        pMsgBox->SetMessage(DlgMsgBox::Info, QStringLiteral("提示"), QStringLiteral("请输入验证码"));
        pMsgBox->exec();

        ui.lblVerfyCode->repaint();
        ui.edtVerifyCode->setText("");

        return;
    }

    if (strVerify.toLower() != m_strVerifyCode.toLower())
    {
        DlgMsgBox* pMsgBox = new DlgMsgBox;
        pMsgBox->SetMessage(DlgMsgBox::Error, QStringLiteral("警告"), QStringLiteral("验证码错误"));
        pMsgBox->exec();

        ui.lblVerfyCode->repaint();
        ui.edtVerifyCode->setText("");

        return;
    }

    if(strUsername.length() == 0 || strPassword.length() == 0)
    {
        DlgMsgBox* pMsgBox = new DlgMsgBox;
        pMsgBox->SetMessage(DlgMsgBox::Info, QStringLiteral("提示"), QStringLiteral("请输入用户名和密码"));
        pMsgBox->exec();

        ui.lblVerfyCode->repaint();
        ui.edtVerifyCode->setText("");
        return;
    }

    ui.btnLogin->setEnabled(false);

    emit sigLogin(strUsername, strPassword);
}

void DlgLogin::mouseMoveEvent(QMouseEvent *e)
{
    if (m_bMousePressed && (e->buttons() && Qt::LeftButton) && !m_bWinMax) 
    {
        this->move(e->globalPos() - m_ptMousePoint);
        e->accept();
    }
}

void DlgLogin::mousePressEvent(QMouseEvent *e)
{
    if (e->button() == Qt::LeftButton) 
    {
        m_bMousePressed = true;
        m_ptMousePoint = e->globalPos() - this->pos();
        e->accept();
    }
}

void DlgLogin::mouseReleaseEvent(QMouseEvent *)
{
    m_bMousePressed = false;
}

void DlgLogin::onVerifyCodeUpdated(QString strCode)
{
    m_strVerifyCode = strCode;
}

void DlgLogin::onLoginResult(int iRetCode, QString strErrMsg)
{
    ui.btnLogin->setEnabled(true);

    if (ERROR_CODE::SUCCESS == iRetCode)
    {
        enterDlgMain();
    }
    else if (PB_ERROR_CODE::NOT_INIT == iRetCode)//未初始化
    {
        DlgMsgBox* pMsgBox = new DlgMsgBox;
        pMsgBox->SetMessage(DlgMsgBox::Question, QStringLiteral("警告"), QStringLiteral("系统尚未初始化，无法进行交易，是否强制登录？"));
        int iRet = pMsgBox->exec();
        if (iRet == QDialog::Accepted)//确认
        {
            enterDlgMain();
        }
        else
        {
            ui.lblStatusMessage->setText(QString(QStringLiteral("登录失败：[%2]%3")).arg(iRetCode).arg(strErrMsg));
            ui.lblVerfyCode->repaint();
            ui.edtVerifyCode->setText("");
        }
    }
    else
    {
        ui.lblStatusMessage->setText(QString(QStringLiteral("登录失败：[%2]%3")).arg(iRetCode).arg(strErrMsg));
        ui.lblVerfyCode->repaint();
        ui.edtVerifyCode->setText("");
    }
}

void DlgLogin::onLogining(QString strMsg)
{
    ui.lblStatusMessage->setText(strMsg);
}

void DlgLogin::enterDlgMain()
{
    ui.lblStatusMessage->setText(QStringLiteral("登录成功，正在初始化界面..."));

    //保存用户名密码
    ConfigMgr::getData().strUsername = ui.edtUsername->text();
    ConfigMgr::getData().bRememberPassword = ui.chkRememberPassword->isChecked();
    if (ConfigMgr::getInstance()->getData().bRememberPassword)
    {
        QByteArray arrPassword = QByteArray(ui.edtPassword->text().toUtf8().data());
        ConfigMgr::getData().arrPassword = Xxtea::encrypt(arrPassword);
    }
    ConfigMgr::getInstance()->save();

    //进入主界面
    m_pDlgMain = new DlgMain(m_pCounterThread, m_pReportThread, m_pContext);
    m_pDlgMain->show();

    m_bReleaseCounterThread = false;

    this->close();
}
