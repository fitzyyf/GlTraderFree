#pragma once

#include <QtWidgets/QDialog>
#include "ui_Login.h"

class CounterThread;
class ReportThread;
class DlgMain;
class Context;

class DlgLogin : public QDialog
{
    Q_OBJECT

public:
    DlgLogin(QWidget *parent = Q_NULLPTR);
    virtual ~DlgLogin();

protected:
    void mouseMoveEvent(QMouseEvent *e);
    void mousePressEvent(QMouseEvent *e);
    void mouseReleaseEvent(QMouseEvent *);

signals:
    void sigLogin(QString strUsername, QString strPassword);
    //void sigLogout();
    
public slots:
    void login();
    //void logout();

    void onLoginResult(int iRetCode, QString strErrMsg);
    void onLogining(QString strMsg);

    void onVerifyCodeUpdated(QString strCode);

private:
    void initUi();
    void initSignalSlot();
    void initThread();
    void initData();
    void enterDlgMain();

private:
    Ui::DlgLogin ui;
    QString m_strVerifyCode;

    QPoint m_ptMousePoint;
    bool m_bMousePressed;
    bool m_bWinMax;
    QRect m_recWinLocation;

    Context* m_pContext;
    CounterThread* m_pCounterThread;
    ReportThread* m_pReportThread;
    bool m_bReleaseCounterThread;

    DlgMain* m_pDlgMain;
};
