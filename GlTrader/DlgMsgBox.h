#pragma once

#include <QDialog>

namespace Ui {
class DlgMsgBox;
}

class DlgMsgBox : public QDialog
{
    Q_OBJECT
public:
    enum
    {
        Info,
        Question,
        Error,
    };

public:
    explicit DlgMsgBox(QWidget *parent = 0);
    ~DlgMsgBox();

    void SetMessage(int type, const QString& title, const QString& msg);

protected:
    void mouseMoveEvent(QMouseEvent *e);
    void mousePressEvent(QMouseEvent *e);
    void mouseReleaseEvent(QMouseEvent *);

private slots:
    void on_btnOk_clicked();

private:
    Ui::DlgMsgBox *ui;

    QPoint mousePoint;              //鼠标拖动自定义标题栏时的坐标
    bool mousePressed;              //鼠标是否按下
};

