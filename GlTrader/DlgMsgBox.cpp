#include "stdafx.h"

#include "DlgMsgBox.h"
#include "ui_MsgBox.h"
#include "IconHelper.h"
//#include "myhelper.h"

DlgMsgBox::DlgMsgBox(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DlgMsgBox)
{
    ui->setupUi(this);

    this->mousePressed = false;
    //设置窗体标题栏隐藏
    this->setWindowFlags(Qt::FramelessWindowHint | Qt::SubWindow);
    //设置窗体关闭时自动释放内存
    this->setAttribute(Qt::WA_DeleteOnClose);
    //设置图形字体
    IconHelper::Instance()->SetIcon(ui->btnClose, QChar(0xf00d), 10);
    //关联关闭按钮
    connect(ui->btnClose, SIGNAL(clicked()), this, SLOT(close()));
    connect(ui->btnCancel, SIGNAL(clicked()), this, SLOT(close()));
    //窗体居中显示
    //窗体显示在屏幕中间位置
    QDesktopWidget *deskdop = QApplication::desktop();
    move((deskdop->width() - this->width()) / 2, (deskdop->height() - this->height()) / 2);
}

DlgMsgBox::~DlgMsgBox()
{
    delete ui;
}

void DlgMsgBox::SetMessage(int type, const QString& title, const QString& msg)
{
    if (type == DlgMsgBox::Info)
    {
        ui->labIcoMain->setStyleSheet("border-image: url(:/image/info.png);");
        ui->btnCancel->setVisible(false);
        ui->lblTitle->setText(title);
    } 
    else if (type == DlgMsgBox::Question)
    {
        ui->labIcoMain->setStyleSheet("border-image: url(:/image/question.png);");
        ui->lblTitle->setText(title);
    } 
    else if (type == DlgMsgBox::Error)
    {
        ui->labIcoMain->setStyleSheet("border-image: url(:/image/error.png);");
        ui->btnCancel->setVisible(false);
        ui->lblTitle->setText(title);
    }

    ui->labInfo->setText(msg);
}

void DlgMsgBox::on_btnOk_clicked()
{
    done(1);
    this->close();
}

void DlgMsgBox::mouseMoveEvent(QMouseEvent *e)
{
    if (mousePressed && (e->buttons() && Qt::LeftButton)) {
        this->move(e->globalPos() - mousePoint);
        e->accept();
    }
}

void DlgMsgBox::mousePressEvent(QMouseEvent *e)
{
    if (e->button() == Qt::LeftButton) {
        mousePressed = true;
        mousePoint = e->globalPos() - this->pos();
        e->accept();
    }
}

void DlgMsgBox::mouseReleaseEvent(QMouseEvent *)
{
    mousePressed = false;
}
