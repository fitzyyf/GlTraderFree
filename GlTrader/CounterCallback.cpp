#include "stdafx.h"

#include <string>

#include "CounterCallback.h"
#include "Utils.h"
#include "Context.h"

using namespace std;

//*******************************************************************************
//
//  CMsgCallback  业务消息回调
//
//*******************************************************************************

CMsgCallback::CMsgCallback(Context* pContext)
    : m_pContext(pContext)
{

}

unsigned long CMsgCallback::QueryInterface(const char *iid, IKnown **ppv)
{
    //未使用
    return 0;
}

unsigned long CMsgCallback::AddRef()
{
    //未使用
    return 0;
}

unsigned long CMsgCallback::Release()
{
    //未使用
    return 0;
}

void CMsgCallback::OnConnect(CConnectionInterface *lpConnection)
{
    qDebug() << "OnConnect: successfully connected." << lpConnection;
}

void CMsgCallback::OnSafeConnect(CConnectionInterface *lpConnection)
{
    qDebug() << "OnSafeConnect: successfully connected." << lpConnection;
}

void CMsgCallback::OnRegister(CConnectionInterface *lpConnection)
{
    //在注册成功之后，回调这个函数，这个回调里面可以通知其他线程已经可以开始干活了
    //连接已经建立，可以发包收包做业务操作了。
    qDebug() << "OnRegister: successfully registered." << lpConnection;
    emit sigConnCreated((quintptr)lpConnection);
}

void CMsgCallback::OnClose(CConnectionInterface *lpConnection)
{
    qDebug() << "OnClose: sdk connection closed." << lpConnection;
    emit sigConnClosed((quintptr)lpConnection);
}

void CMsgCallback::OnSent(CConnectionInterface *lpConnection, int hSend, void *reserved1, void *reserved2, int nQueuingData)
{
    qDebug() << "OnSend: hSend(" << hSend << ") send successed, queuingData(" << nQueuingData << ")." << lpConnection;
}

void CMsgCallback::Reserved1(void *a, void *b, void *c, void *d)
{
    //未使用
}

void CMsgCallback::Reserved2(void *a, void *b, void *c, void *d)
{
    //未使用
}

int  CMsgCallback::Reserved3()
{
    //未使用
    return 0;
}

void CMsgCallback::Reserved4()
{
    //未使用
}

void CMsgCallback::Reserved5()
{
    //未使用
}

void CMsgCallback::Reserved6()
{
    //未使用
}

void CMsgCallback::Reserved7()
{
    //未使用
}

void CMsgCallback::OnReceivedBiz(CConnectionInterface *lpConnection, int hSend, const void *lpUnPackerOrStr, int nResult)
{
    //未使用
}

void CMsgCallback::OnReceivedBizEx(CConnectionInterface *lpConnection, int hSend, LPRET_DATA lpRetData, const void *lpUnpackerOrStr, int nResult)
{
    //未使用
}

void CMsgCallback::OnReceivedBizMsg(CConnectionInterface *lpConnection, int hSend, IBizMessage* lpMsg)
{
    //消息为空
    if (lpMsg == nullptr)
    {
        qWarning() << "Receive empty biz message, hSend = " << hSend << ", ignore";
        return;
    }

    IBizMessage* lpNewBizMessage = Utils::cloneIBizMessage(lpMsg);
    lpNewBizMessage->AddRef();

    emit sigMsgCallback(lpConnection, hSend, lpNewBizMessage);
}

//*******************************************************************************
//
//  CPushCallback  主推消息
//
//*******************************************************************************

CPushCallback::CPushCallback(Context* pContext)
    : m_pContext(pContext)
{

}

unsigned long CPushCallback::QueryInterface(const char *iid, IKnown **ppv)
{
    //未使用
    return 0;
}

unsigned long CPushCallback::AddRef()
{
    //未使用
    return 0;
}

unsigned long CPushCallback::Release()
{
    //未使用
    return 0;
}

void CPushCallback::OnReceived(CSubscribeInterface *lpSub, int subscribeIndex, const void *lpData, int nLength, LPSUBSCRIBE_RECVDATA lpRecvData)
{
    //消息为空
    if (lpData == nullptr)
    {
        qWarning() << "Recv push message error, lpData is null, subscribeIndex = " << subscribeIndex << ", ignore";
        return;
    }

    if (lpRecvData == nullptr)
    {
        qCritical() << "Recv push message error: lpRecvData is null";
    }

    //IF2UnPacker* lpUnPack = NewUnPacker((void*)lpData, nLength);
    //qDebug() << "CPushCallback::OnReceived: " << Utils::toQString(lpUnPack).toUtf8().data();

    QByteArray buf;
    buf.resize(nLength);
    memcpy(buf.data(), lpData, nLength);

    emit sigPushCallback(lpSub, subscribeIndex, buf);
}

void CPushCallback::OnRecvTickMsg(CSubscribeInterface *lpSub, int subscribeIndex, const char* TickMsgInfo)
{
}
