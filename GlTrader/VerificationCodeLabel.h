#pragma once

#include <QLabel>
#include <QPaintEvent>
#include <QPainter>
#include <QTime>

class VerificationCodeLabel : public QLabel
{
    Q_OBJECT

public:
    VerificationCodeLabel(QWidget *parent = 0);
    virtual ~VerificationCodeLabel();


signals:
    void sigVerifyCodeUpdated(QString strCode);

protected:
    //重写绘制事件,以此来生成验证码
    void paintEvent(QPaintEvent *event);
    void mousePressEvent(QMouseEvent *ev);

private:
    //生成验证码
    void produceVerificationCode();
    //产生随机的字符
    QChar produceRandomLetter();
    //产生随机的颜色
    void produceRandomColor();

private:
    int m_iLetterCnt;//产生字符的数量
    int m_iNoicePointCnt;//噪点的数量

    QChar* m_pVerificationCodeArray;
    QColor* m_pColorArray;
};
