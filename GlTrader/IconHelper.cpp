#include "stdafx.h"
#include "IconHelper.h"

IconHelper* IconHelper::m_pInstance = 0;
IconHelper::IconHelper(QObject*):
    QObject(qApp)
{
    int fontId = QFontDatabase::addApplicationFont(":/image/fontawesome-webfont.ttf");
    QString fontName = QFontDatabase::applicationFontFamilies(fontId).at(0);
    m_iconFont = QFont(fontName);
}

void IconHelper::SetIcon(QLabel* lab, QChar c, int size)
{
    m_iconFont.setPointSize(size);
    lab->setFont(m_iconFont);
    lab->setText(c);
}

void IconHelper::SetIcon(QPushButton* btn, QChar c, int size)
{
    m_iconFont.setPointSize(size);
    btn->setFont(m_iconFont);
    btn->setText(c);
}
