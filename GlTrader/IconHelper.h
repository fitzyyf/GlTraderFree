#ifndef ICONHELPER_H
#define ICONHELPER_H

#include <QObject>
#include <QFont>
#include <QFontDatabase>
#include <QMutex>
#include <QLabel>
#include <QPushButton>
#include <QApplication>

class IconHelper : public QObject
{
private:
    explicit IconHelper(QObject *parent = 0);
    QFont m_iconFont;
    static IconHelper* m_pInstance;

public:
    static IconHelper* Instance()
    {
        static QMutex mutex;
        if (!m_pInstance) {
            QMutexLocker locker(&mutex);
            if (!m_pInstance) {
                m_pInstance = new IconHelper;
            }
        }
        return m_pInstance;
    }

    void SetIcon(QLabel* lab, QChar c, int size = 10);
    void SetIcon(QPushButton* btn, QChar c, int size = 10);

};

#endif // ICONHELPER_H
