#include "stdafx.h"
#include "OrderMgr.h"

//**************************************************
//
// 成交明细
//
//**************************************************
Deal::Deal()
{
    fDealAmount = 0;  //成交数量
    fDealPrice = 0;  //成交价格
    fDealBalance = 0;//成交金额
    fDealfee = 0;   //总费用

    bDisplay = false;
    bOutput = false;
}

void Deal::reset()
{
    dtDealTime.setSecsSinceEpoch(0);
    strDealNo.clear();
    fDealAmount = 0;
    fDealPrice = 0;
    fDealBalance = 0;
    fDealfee = 0;

    bDisplay = false;
    bOutput = false;
}

void Deal::clone(const Deal& o)
{
    this->dtDealTime = o.dtDealTime;
    this->strDealNo = o.strDealNo;
    this->fDealAmount = o.fDealAmount;
    this->fDealPrice = o.fDealPrice;
    this->fDealBalance = o.fDealBalance;
    this->fDealfee = o.fDealfee;

    this->bDisplay = o.bDisplay;
    this->bOutput = o.bOutput;
}

Deal& Deal::operator = (const Deal& o)
{
    this->clone(o);

    return *this;
}

QString Deal::toQString() const
{
    QString buf;

    buf += QString("%1 = %2, ").arg("dtDealTime").arg(dtDealTime.toString("yyyy-MM-dd hh:mm:ss.zzz"));
    buf += QString("%1 = %2, ").arg("strDealNo").arg(strDealNo);
    buf += QString("%1 = %2, ").arg("fDealAmout").arg(fDealAmount);
    buf += QString("%1 = %2, ").arg("fDealPrice").arg(fDealPrice);
    buf += QString("%1 = %2, ").arg("fDealBalance").arg(fDealBalance);
    buf += QString("%1 = %2, ").arg("fTotalfee").arg(fDealfee);
    buf += QString("%1 = %2, ").arg("bDisplay").arg(bDisplay);
    buf += QString("%1 = %2, ").arg("bOutput").arg(bOutput);

    return buf;
}


//**************************************************
//
//   订单
//
//**************************************************

//TODO 还可以改进：iExtSystemId一共只能有8位数，前4位是时分秒除以9999的余数，后4位不断加1
unsigned int Order::m_iAutoIncr = (QDateTime::currentDateTime().toString("hhmmss").toInt() % (9999)) * 10000;

Order::Order()
{
    eEntrustType = EntrustType::Entrust;
    iInternalId = 0;
    hSendHandle = 0;

    iExtAccessSystemId = 0;  //接入系统序号

    iBatchNo = 0;           //委托批号
    fEntrustPrice = 0;      //委托价格
    fEntrustAmount = 0;     //委托数量
    iExtSystemId = 0;       //外部系统id
    iRequestOrder = 0;

    iEntrustNo = 0;       //委托序号
    iCancelEntrustNo = 0;
    iCancelSuccessFlag = 0;//撤单结果
    fCancelDealAmount = 0;//撤成数量
    iFailCode = 0;   //废单代码
    iRiskSerialNo = 0;
}

Order::~Order()
{
    for (int i = 0; i < vecDealDetail.size(); i++)
    {
        delete vecDealDetail[i];
    }
    vecDealDetail.clear();
}

void Order::reset()
{
    eEntrustType = EntrustType::Entrust;
    iInternalId = 0;
    hSendHandle = 0;

    iExtAccessSystemId = 0;

    dtEntrustTime.setSecsSinceEpoch(0);
    iBatchNo = 0;
    strAccountCode.clear();
    strAccountName.clear();
    strAssetNo.clear();
    strCombiNo.clear();
    strCombiName.clear();
    strMarketNo.clear();
    strMarketName.clear();
    strStockCode.clear();
    strStockName.clear();
    strEntrustDirection.clear();
    strPriceType.clear();
    fEntrustPrice = 0;
    fEntrustAmount = 0;
    iExtSystemId = 0;
    iRequestOrder = 0;

    dtWithdrawTime.setSecsSinceEpoch(0);
    dtWithdrawBusinessTime.setSecsSinceEpoch(0);

    dtBusinessTime.setSecsSinceEpoch(0);
    iEntrustNo = 0;
    iCancelEntrustNo = 0;
    strReportNo.clear();
    strConfirmNo.clear();
    strEntrustStatus.clear();
    fCancelDealAmount = 0;
    iCancelSuccessFlag = 0;
    iFailCode = 0;
    strFailCause.clear();
    iRiskSerialNo = 0;
}

void Order::clone(const Order& o)
{
    this->eEntrustType = o.eEntrustType;
    this->iInternalId = o.iInternalId;
    this->hSendHandle = o.hSendHandle;

    this->iExtAccessSystemId = o.iExtAccessSystemId;

    this->dtEntrustTime = o.dtEntrustTime;
    this->iBatchNo = o.iBatchNo;
    this->strAccountCode = o.strAccountCode;
    this->strAccountName = o.strAccountName;
    this->strAssetNo = o.strAssetNo;
    this->strCombiNo = o.strCombiNo;
    this->strCombiName = o.strCombiName;
    this->strMarketNo = o.strMarketNo;
    this->strMarketName = o.strMarketName;
    this->strStockCode = o.strStockCode;
    this->strStockName = o.strStockName;
    this->strEntrustDirection = o.strEntrustDirection;
    this->strPriceType = o.strPriceType;
    this->fEntrustPrice = o.fEntrustPrice;
    this->fEntrustAmount = o.fEntrustAmount;
    this->iExtSystemId = o.iExtSystemId;
    this->iRequestOrder = o.iRequestOrder;

    this->dtWithdrawTime = o.dtWithdrawTime;
    this->dtWithdrawBusinessTime = o.dtWithdrawBusinessTime;

    this->dtBusinessTime = o.dtBusinessTime;
    this->iEntrustNo = o.iEntrustNo;
    this->iCancelEntrustNo = o.iCancelEntrustNo;
    this->strReportNo = o.strReportNo;
    this->strConfirmNo = o.strConfirmNo;
    this->strEntrustStatus = o.strEntrustStatus;
    this->fCancelDealAmount = o.fCancelDealAmount;
    this->iCancelSuccessFlag = o.iCancelSuccessFlag;
    this->iFailCode = o.iFailCode;
    this->strFailCause = o.strFailCause;
    this->iRiskSerialNo = o.iRiskSerialNo;
}

Order& Order::operator =(const Order& o)
{
    this->clone(o);

    return *this;
}

QString Order::toQString() const 
{
    QString buf("Order: ");

    buf.append("\r\n[Intnal data] ");
    buf += QString("%1 = %2, ").arg("eEntrustType").arg(eEntrustType);
    buf += QString("%1 = %2, ").arg("iInternalId").arg(iInternalId);
    buf += QString("%1 = %2, ").arg("hSendHandle").arg(hSendHandle);

    buf.append("\r\n[Access data] ");
    buf += QString("%1 = %2, ").arg("iExtAccessSystemId").arg(iExtAccessSystemId);

    buf.append("\r\n[Entrust data]");
    buf += QString("%1 = %2, ").arg("dtEntrustTime").arg(dtEntrustTime.toString("yyyy-MM-dd hh:mm:ss.zzz"));
    buf += QString("%1 = %2, ").arg("iBatchNo").arg(iBatchNo);

    buf += QString("%1 = %2, ").arg("strAccountCode").arg(strAccountCode);
    buf += QString("%1 = %2, ").arg("strAccountName").arg(strAccountName);
    buf += QString("%1 = %2, ").arg("strAssetNo").arg(strAssetNo);
    buf += QString("%1 = %2, ").arg("strCombiNo").arg(strCombiNo);
    buf += QString("%1 = %2, ").arg("strCombiName").arg(strCombiName);
    buf += QString("%1 = %2, ").arg("strMarketNo").arg(strMarketNo);
    buf += QString("%1 = %2, ").arg("strMarketName").arg(strMarketName);
    buf += QString("%1 = %2, ").arg("strStockCode").arg(strStockCode);
    buf += QString("%1 = %2, ").arg("strStockName").arg(strStockName);
    buf += QString("%1 = %2, ").arg("strEntrustDirection").arg(strEntrustDirection);
    buf += QString("%1 = %2, ").arg("strPriceType").arg(strPriceType);
    buf += QString("%1 = %2, ").arg("fEntrustPrice").arg(fEntrustPrice);
    buf += QString("%1 = %2, ").arg("fEntrustAmount").arg(fEntrustAmount);
    buf += QString("%1 = %2, ").arg("iExtSystemId").arg(iExtSystemId);
    buf += QString("%1 = %2, ").arg("iRequestOrder").arg(iRequestOrder);

    buf += QString("%1 = %2, ").arg("dtWithdrawTime").arg(dtWithdrawTime.toString("yyyy-MM-dd hh:mm:ss.zzz"));
    buf += QString("%1 = %2, ").arg("dtWithdrawEntrustTime").arg(dtWithdrawBusinessTime.toString("yyyy-MM-dd hh:mm:ss.zzz"));

    buf.append("\r\n[Report data] ");
    buf += QString("%1 = %2, ").arg("dtBusinessTime").arg(dtBusinessTime.toString("yyyy-MM-dd hh:mm:ss.zzz"));
    buf += QString("%1 = %2, ").arg("iEntrustNo").arg(iEntrustNo);
    buf += QString("%1 = %2, ").arg("iCancelEntrustNo").arg(iCancelEntrustNo);
    buf += QString("%1 = %2, ").arg("strReportNo").arg(strReportNo);
    buf += QString("%1 = %2, ").arg("strConfirmNo").arg(strConfirmNo);
    buf += QString("%1 = %2, ").arg("strEntrustStatus").arg(strEntrustStatus);
    buf += QString("%1 = %2, ").arg("fCancelDealAmount").arg(fCancelDealAmount);
    buf += QString("%1 = %2, ").arg("iCancelSuccessFlag").arg(iCancelSuccessFlag);
    buf += QString("%1 = %2, ").arg("iFailCode").arg(iFailCode);
    buf += QString("%1 = %2, ").arg("strFailCause").arg(strFailCause);
    buf += QString("%1 = %2, ").arg("iRiskSerialNo").arg(iRiskSerialNo);

    buf.append("\r\n[Deal data]  ");
    for (int i = 0; i < vecDealDetail.size(); i++)
    {
        buf.append("\r\n");
        buf.append(vecDealDetail[i]->toQString());
    }

    return buf;
}

unsigned int Order::getAutoIncr()
{
    QMutex m;
    QMutexLocker locker(&m);

    //需保证当天唯一
    return ++m_iAutoIncr;
}
