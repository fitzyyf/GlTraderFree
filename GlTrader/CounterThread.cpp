#include "stdafx.h"

#include <QTimer>
#include <string>

#include "hundsun/t2sdk_interface.h"
#include "CounterThread.h"
#include "Utils.h"
#include "CounterCallback.h"
#include "Constants.h"
#include "Context.h"
#include "FuncHandler/FuncHandler.h"

CounterThread::CounterThread(ConfigData configData, Context* pContext, QObject *parent)
    : QThread(parent)
    , m_ConfigData(configData)
    , m_pContext(pContext)
    , m_pHeartBeatTimer(nullptr)
    , m_pExitTimer(nullptr)
    , m_lpConnConfig(nullptr)
    , m_pConnMsg(nullptr)
    , m_pConnPush(nullptr)
    , m_pSubscribe(nullptr)
    , m_pMsgCallback(nullptr)
    , m_pPushCallback(nullptr)
    , m_iPacketId(0)
    , m_bRefreshAccountCodeDict(false)
    , m_bRefreshCombiNoDict(false)
    //, m_bConnectedUfx(false)
    //, m_bConnectedMc(false)
    //, m_bConnected(false)
    //, m_bConnecting(false)
    , m_bLogout(false)
    //, m_pReconnectUfxTimer(nullptr)
    //, m_pReconnectMcTimer(nullptr)
    //, m_pReconnectTimer(nullptr)
    , m_bFirstLogin(true)
{
    moveToThread(this);
}


CounterThread::~CounterThread()
{
    if (m_pExitTimer)
    {
        if (m_pExitTimer->isActive())
        {
            m_pExitTimer->stop();
        }
        SAFE_DELETE(m_pExitTimer);
    }

    this->releaseConnConfig();

    SAFE_RELEASE(m_pConnMsg);
    SAFE_RELEASE(m_pConnPush);
    SAFE_RELEASE(m_pSubscribe);

    SAFE_DELETE(m_pMsgCallback);
    SAFE_DELETE(m_pPushCallback);
}

void CounterThread::run()
{
    this->initConnConfig();

    exec();

    FuncHandlerMgr::destoryInstance();
}

QString CounterThread::getErrMsg()
{
    return m_strErrMsg;
}

void CounterThread::setErrMsg(const QString strErrMsg)
{
    m_strErrMsg = strErrMsg;
}

void CounterThread::onLogin(QString strUsername, QString strPassword)
{
    m_pContext->setUsername(strUsername);
    m_pContext->setPassword(strPassword);

    FuncHandlerMgr::getInstance()->setContext(m_pContext);

    //先释放一下资源，防止前一次login失败未释放
    this->disconnectServer();

    //连接服务器
    int iRet = connectServer();
    if (iRet != 0)
    {
        this->disconnectServer();

        emit sigLoginResult(iRet, this->getErrMsg());
        return;
    }

    //登录
    iRet = doLogin();
    if (iRet != 0)
    {
        this->disconnectServer();

        emit sigLoginResult(iRet, this->getErrMsg());
        return;
    }

    //m_bConnected = true;
}

void CounterThread::onLogout()
{
    emit sigRunLog(RunLog_Info, QStringLiteral("正在退出..."));

    m_bLogout = true;

    this->stopHeartBeat();

    //网络未连接，直接退出
    //if (!m_bConnected)
    //{
    //    this->disconnectServer();
    //    emit sigLogoutResult(ERROR_CODE::SUCCESS, this->getErrMsg());
    //    return;
    //}

    int iFuncNo = FUNC_NO::LOGOUT;
    FuncHandler* pHandler = FuncHandlerMgr::getInstance()->getHandler(iFuncNo);
    if (pHandler == nullptr)
    {
        qCritical() << "Can't find func handler: " << iFuncNo;
    }

    int iRet = pHandler->packAndSend(m_pConnMsg, CommMode::Asy);
    if (iRet < 0)
    {
        this->setErrMsg(Utils::toQString(m_pConnMsg->GetErrorMsg(iRet)));
        qCritical() << "Send logout failed: " << this->getErrMsg();

        //发送失败，直接退出
        this->disconnectServer();
        emit sigLogoutResult(ERROR_CODE::SUCCESS, this->getErrMsg());
    }
    else
    {
        //设置一个定时器，如果超时，自动退出
        if (m_pExitTimer == nullptr)
        {
            m_pExitTimer = new QTimer;
        }
        connect(m_pExitTimer, &QTimer::timeout, this, [this] {
            
            qDebug() << "Recv logout response failed, force to exit";
            //delete pTimer;//不能在其他线程中操作定时器

            this->disconnectServer();
            emit sigLogoutResult(ERROR_CODE::SUCCESS, this->getErrMsg());
        });
        m_pExitTimer->setSingleShot(true);//一次性
        m_pExitTimer->start(1000);
    }
}

void CounterThread::startHeartBeat()
{
    if (m_pHeartBeatTimer == nullptr)
    {
        m_pHeartBeatTimer = new QTimer;
        QObject::connect(m_pHeartBeatTimer, &QTimer::timeout, this, &CounterThread::sendHeartBeat);
    }

    if (m_pHeartBeatTimer->isActive())
    {
        qDebug() << "Heart beat timer has been actived";
        return;
    }

    qDebug() << "Start heart beat, interval = " << CONSTANT::HEARTBEAT_INTERVAL << " ms";

    //再设置定时器
    m_pHeartBeatTimer->start(CONSTANT::HEARTBEAT_INTERVAL);

}

void CounterThread::stopHeartBeat()
{
    if (m_pHeartBeatTimer == nullptr)
    {
        return;
    }

    qDebug() << "Stop heart beat";

    m_pHeartBeatTimer->stop();
    SAFE_DELETE(m_pHeartBeatTimer);
}

int CounterThread::connectServer()
{
    emit sigLogining(QStringLiteral("连接服务器..."));
    qDebug() << QStringLiteral("Connect to server").toUtf8().data() << m_ConfigData.strServers;

    int ret = this->connectUfx();
    if (ret != 0)
    {
        qCritical() << "Connect ufx error: " << this->getErrMsg().toUtf8().data();

        return ret;
    }

    ret = this->connectMc();
    if (ret != 0)
    {
        qCritical() << "Connect mc error: " << this->getErrMsg().toUtf8().data();

        return ret;
    }

    return ret;
}

void CounterThread::disconnectServer()
{
    //必须先断mc，再断ufx
    this->disconnectMc();
    this->disconnectUfx();
}

void CounterThread::initConnConfig()
{
    //CConfigInterface、CConnectionInterface的使用可以参考《T2SDK 外部版开发指南.docx》
    //创建T2SDK配置对象，并设置UFX服务器地址以及License文件
    m_lpConnConfig = NewConfig();
    m_lpConnConfig->AddRef();

    m_lpConnConfig->SetString("t2sdk", CONFIG_ITEM::SERVERS, m_ConfigData.strServers.toUtf8().data());
    m_lpConnConfig->SetString("t2sdk", "license_file", CONSTANT::LICENSE_FILE);
    m_lpConnConfig->SetString("t2sdk", "login_name", CONSTANT::LOGIN_NAME);

    //m_lpConnConfig->SetInt("t2sdk", CONFIG_ITEM::HEARTBEAT_TIME, m_ConfigData.iHeartbeatTime);
    //m_lpConnConfig->SetInt("t2sdk", CONFIG_ITEM::IF_ERROR_LOG, m_ConfigData.iIfErrorLog);
    //m_lpConnConfig->SetString("t2sdk", CONFIG_ITEM::LOGDIR, m_ConfigData.strLogDir.toUtf8().data());
    //m_lpConnConfig->SetInt("t2sdk", CONFIG_ITEM::IF_SENDRECV_LOG, m_ConfigData.iIfSendRecvLog);
    //m_lpConnConfig->SetInt("t2sdk", CONFIG_ITEM::MAX_LOG_FILE_SIZE, m_ConfigData.iMaxLogFileSize);
    //m_lpConnConfig->SetInt("t2sdk", CONFIG_ITEM::MAX_LOG_FILE_COUNT, m_ConfigData.iMaxLogFileCount);

    //m_lpConnConfig->SetInt("t2sdk", CONFIG_ITEM::WRITEDATA, m_ConfigData.iWriteData);
    //m_lpConnConfig->SetInt("t2sdk", CONFIG_ITEM::IF_REQINFO_CLIIPPING, m_ConfigData.iIfReqInfoClipping);

    //m_lpConnConfig->SetInt("t2sdk", CONFIG_ITEM::AUTO_RECONNECT, m_ConfigData.iAutoReconnect);
    //m_lpConnConfig->SetInt("t2sdk", CONFIG_ITEM::RECONNECT_TIMEOUT, m_ConfigData.iReconnectTimeout);

    //ssl加密
    //m_lpConnConfig->SetString("safe", "safe_level", "ssl");
    //m_lpConnConfig->SetString("safe", "version", "TLSv1.2");
    //m_lpConnConfig->SetString("safe", "cert_file", "glscpb1.pfx");
    //m_lpConnConfig->SetString("safe", "cert_pwd", "jiangqi");

    //密码加密
    //m_lpConnConfig->SetString("safe", "safe_level", CONSTANT::SAFE_LEVEL);
    //m_lpConnConfig->SetString("safe", "comm_pwd", m_pContext->getT2Password().toUtf8().data());
}

void CounterThread::releaseConnConfig()
{
    SAFE_RELEASE(m_lpConnConfig);
}

int CounterThread::connectUfx()
{
    emit sigLogining(QStringLiteral("连接UFX服务器..."));
    qDebug() << QStringLiteral("Connect to ufx server").toUtf8().data() << m_ConfigData.strServers;

    //消息回调
    m_pMsgCallback = new CMsgCallback(m_pContext);
    connect(m_pMsgCallback, SIGNAL(sigConnCreated(quint32)), this, SLOT(onConnCreated(quint32)));
    connect(m_pMsgCallback, SIGNAL(sigMsgCallback(CConnectionInterface*, int, IBizMessage*)),
        this, SLOT(onMsgCallback(CConnectionInterface*, int, IBizMessage*)));
    connect(m_pMsgCallback, SIGNAL(sigConnClosed(quint32)), this, SLOT(onConnClosed(quint32)));

    //创建连接对象，并注册回调
    m_pConnMsg = NewConnection(m_lpConnConfig);
    m_pConnMsg->AddRef();

    int ret = m_pConnMsg->Create2BizMsg(m_pMsgCallback);
    if (ret != 0)
    {
        qCritical() << "Create2BizMsg error: " << Utils::toQString(m_pConnMsg->GetErrorMsg(ret));
        this->setErrMsg(Utils::toQString(m_pConnMsg->GetErrorMsg(ret)));

        return ret;
    }

    //连接as_ufx，参数3000为超时参数，单位毫秒
    ret = m_pConnMsg->Connect(3000);
    if (ret != 0)
    {
        qCritical() << "Connect error: " << Utils::toQString(m_pConnMsg->GetErrorMsg(ret));
        this->setErrMsg(Utils::toQString(m_pConnMsg->GetErrorMsg(ret)));

        return ret;
    }

    //获取的地址为：10.193.13.197:38928
    QString ip = Utils::toQString(m_pConnMsg->GetSelfAddress());
    QStringList strList = ip.split(":");
    if (strList.size() >= 1)
    {
        m_pContext->setIp(strList.at(0));
    }
    m_pContext->setMac(Utils::toQString(m_pConnMsg->GetSelfMac()));

    //hard disk serial id
    if (m_pContext->getHDSerialId().length() == 0)
    {
        m_pContext->setHdSerialNo(Utils::getHDSerialId());
    }

    qDebug() << "Local IP address: " << m_pContext->getIp() << ", mac: " << m_pContext->getMac();

    qDebug() << "Connect to ufx server successful, local address: " << m_pConnMsg->GetSelfAddress();

    return ERROR_CODE::SUCCESS;
}

void CounterThread::disconnectUfx()
{
    qDebug() << "Disconnect ufx, status =" << (m_pConnMsg ? m_pConnMsg->GetStatus() : -1);

    if (m_pConnMsg != nullptr)
    {
        m_pConnMsg->Close();
        SAFE_RELEASE(m_pConnMsg);
    }

    SAFE_DELETE(m_pMsgCallback);
}

int CounterThread::connectMc()
{
    emit sigLogining(QStringLiteral("连接消息中心..."));
    qDebug() << QStringLiteral("Connect to mc server").toUtf8().data() << m_ConfigData.strServers;

    //创建连接对象
    m_pConnPush = NewConnection(m_lpConnConfig);
    m_pConnPush->AddRef();

    //初始化连接对象
    int iRet = m_pConnPush->Create2BizMsg(m_pMsgCallback);
    if (iRet != 0)
    {
        qCritical() << "Create2BizMsg error: " << Utils::toQString(m_pConnPush->GetErrorMsg(iRet));
        this->setErrMsg(Utils::toQString(m_pConnPush->GetErrorMsg(iRet)));

        return iRet;
    }

    //连接ar_mc，参数3000为超时参数，单位毫秒
    iRet = m_pConnPush->Connect(3000);
    if (iRet != 0)
    {
        qCritical() << "Connect error: " << Utils::toQString(m_pConnPush->GetErrorMsg(iRet));
        this->setErrMsg(Utils::toQString(m_pConnPush->GetErrorMsg(iRet)));

        return iRet;
    }

    //订阅消息
    iRet = subscribe();
    if (iRet != 0)
    {
        this->disconnectServer();

        emit sigLoginResult(iRet, this->getErrMsg());
        return iRet;
    }

    qDebug() << "Connect to mc server successful, local address: " << m_pConnPush->GetSelfAddress();
    return ERROR_CODE::SUCCESS;
}

void CounterThread::disconnectMc()
{
    qDebug() << "Disconnect mc, status =" << (m_pConnPush ? m_pConnPush->GetStatus() : -1);

    SAFE_RELEASE(m_pSubscribe);

    if (m_pConnPush != nullptr)
    {
        m_pConnPush->Close();
        SAFE_RELEASE(m_pConnPush);
    }

    SAFE_DELETE(m_pPushCallback);
}

int CounterThread::subscribe()
{
    emit sigLogining(QStringLiteral("订阅主题消息..."));
    qDebug() << QStringLiteral("Subscribe topic").toUtf8().data();

    m_pPushCallback = new CPushCallback(m_pContext);
    //创建订阅对象
    m_pSubscribe = m_pConnPush->NewSubscriber(m_pPushCallback, CONSTANT::BIZ_NAME, 5000);
    if (m_pSubscribe == nullptr)
    {
        qCritical() << "NewSubscribe error: " << Utils::toQString(m_pConnPush->GetMCLastError());
        this->setErrMsg(Utils::toQString(m_pConnPush->GetMCLastError()));
        SAFE_DELETE(m_pPushCallback);
        return -1;
    }
    m_pSubscribe->AddRef();

    //注意：如果设置了补缺，那么订阅消息后（无需登录）就会收到之前客户端不在线期间主题发布的所有消息
    //所以，在此之前要先重建当天的历史委托和成交信息，并设置好信号和槽
    QObject::connect(m_pPushCallback, SIGNAL(sigPushCallback(CSubscribeInterface*, int, QByteArray)),
        this, SLOT(onPushCallback(CSubscribeInterface*, int, QByteArray)));

    /****************************** 获取订阅参数 ******************************/
    CSubscribeParamInterface* lpSubscribeParam = NewSubscribeParam();
    CAutoKnown ak2(lpSubscribeParam);

    lpSubscribeParam->SetTopicName(CONSTANT::TOPIC_NAME);
    lpSubscribeParam->SetFromNow(CONSTANT::IS_REBUILD);
    lpSubscribeParam->SetReplace(CONSTANT::IS_REPLACE);

    //const char* p = "fuck";
    //lpSubscribeParam->SetAppData(p, strlen(p));//添加附加数据

    //添加过滤字段
    lpSubscribeParam->SetFilter(CONSTANT::FILTER_OPERATOR_NO, m_pContext->getUsername().toUtf8().data());

    //添加发送频率
    lpSubscribeParam->SetSendInterval(CONSTANT::SEND_INTERVAL);

    //创建一个业务包
    IF2Packer* pack = NewPacker(2);
    CAutoKnown ak3(pack);

    pack->BeginPack();
    pack->AddField("login_operator_no");
    pack->AddField("password");
    pack->AddStr(m_pContext->getUsername().toUtf8().data());//这里填你的操作员
    pack->AddStr(m_pContext->getPassword().toUtf8().data());//这里填你的操作员密码
    pack->EndPack();
    IF2UnPacker* lpBack = NULL;

    int iRet = m_pSubscribe->SubscribeTopic(lpSubscribeParam, 5000, &lpBack, pack);
    if (iRet <= 0)
    {
        qDebug() << "Subscribe topic error:[" << iRet << "] " << Utils::toQString(m_pConnPush->GetErrorMsg(iRet));
        this->setErrMsg(Utils::toQString(m_pConnPush->GetErrorMsg(iRet)));

        SAFE_RELEASE(lpBack);
        pack->FreeMem(pack->GetPackBuf());

        SAFE_RELEASE(m_pSubscribe);
        SAFE_DELETE(m_pPushCallback);

        return iRet;
    }
    int subscribeIndex = iRet;
    qDebug() << "SubscribeTopic info:[" << iRet << "] success";
    m_AllSubscribeParam[subscribeIndex] = lpSubscribeParam;//保存到map中，用于以后的取消订阅

    pack->FreeMem(pack->GetPackBuf());

    return 0;
}

void CounterThread::onMsgCallback(CConnectionInterface* lpConnection, int hSend, IBizMessage* lpMsg)
{
    int iFuncNo = lpMsg->GetFunction();

    //调用业务处理器
    FuncHandler* pHandler = FuncHandlerMgr::getInstance()->getHandler(iFuncNo);
    if (pHandler == nullptr)
    {
        qCritical() << "Can't find function handler, iFuncNo = " << iFuncNo;
        emit sigRunLog(RunLog_Error, QStringLiteral("无法处理未知的功能号：%1").arg(iFuncNo));
        return;
    }

    //业务解码
    QMap<QString, QVariant> output;
    int iRet = pHandler->unpack(hSend, lpMsg, output);
    if (iRet != ERROR_CODE::SUCCESS)
    {
        qCritical() << "Unpack response failed: " << iRet;

        QString strErrMsg = output[PARAMETER::ERROR_INFO].toString();
        emit sigRunLog(RunLog_Error, QStringLiteral("应答消息解码失败：%1").arg(strErrMsg));
        this->setErrMsg(strErrMsg);

        if (PB_ERROR_CODE::INVALID_USER_TOKEN == iRet)//token失效
        {
            emit sigRunLog(RunLog_Error, QStringLiteral("用户Token失效，请重启客户端重新登录"));
        }
    }
    
    //登录应答
    if (FUNC_NO::LOGIN == iFuncNo)
    {
        if (ERROR_CODE::SUCCESS == iRet)
        {
            //如果是首次登录，需要刷新用户信息。否则，只需获取user_token即可
            if (m_bFirstLogin)
            {
                emit sigLogining(QStringLiteral("刷新用户信息..."));

                this->startHeartBeat();

                //刷新account、asset、combi
                this->onQueryFund();
                this->onQueryCombi();

                m_bFirstLogin = false;
            }
        }
        else
        {
            emit sigLoginResult(iRet, this->getErrMsg());
        }
    }
    else if (FUNC_NO::LOGOUT == iFuncNo)
    {
        if (m_pExitTimer)
        {
            if (m_pExitTimer->isActive())
            {
                m_pExitTimer->stop();
            }
            SAFE_DELETE(m_pExitTimer);
        }

        this->disconnectServer();

        emit sigLogoutResult(iRet, this->getErrMsg());
    }
    //else if (FUNC_NO::ENTRUST == iFuncNo)
    //{
    //    if (iRet == ERROR_CODE::SUCCESS)
    //    {
    //        Order* pOrder = output[PARAMETER::ORDER_OUT].value<Order*>();
    //        m_pContext->setOrderAcked(pOrder->hSendHandle);
    //        m_pContext->addOrderByEntrustNo(pOrder);
    //        emit sigEntrustResult(pOrder);
    //    }
    //    else
    //    {
    //        OrderList* pOrderList = m_pContext->getUnackOrderList(hSend);
    //        for (int i = 0; i < pOrderList->size(); i++)
    //        {
    //            Order* pOrder = (*pOrderList)[i];

    //            pOrder->strEntrustStatus = ENTRUST_STATUS::FAIL;
    //            pOrder->strFailCause = this->getErrMsg();
    //            m_pContext->setOrderAcked(pOrder->hSendHandle);
    //            emit sigEntrustResult(pOrder);
    //        }
    //    }
    //}
    else if (FUNC_NO::ENTRUST_COMBI == iFuncNo)
    {
        if (iRet == ERROR_CODE::SUCCESS)
        {
            OrderList* pOrderList = output[PARAMETER::ORDER_LIST_OUT].value<OrderList*>();
            for(int i = 0; i < pOrderList->size(); i++)
            {
                Order* pOrder = (*pOrderList)[i];
                m_pContext->setOrderAcked(pOrder->hSendHandle);
                m_pContext->addOrderByEntrustNo(pOrder);
                emit sigEntrustResult(pOrder);
            }
            SAFE_DELETE(pOrderList);
        }
        else
        {
            OrderList* pOrderList = m_pContext->getUnackOrderList(hSend);
            Order* pOrder = nullptr;
            for (int i = 0; i < pOrderList->size(); i++)
            {
                pOrder = (*pOrderList)[i];

                //篮子委托的应答消息中有每条委托的错误信息，这里可以不用设置
                pOrder->strEntrustStatus = ENTRUST_STATUS::FAIL;
                if (pOrder->strFailCause.length() == 0)
                {
                    pOrder->strFailCause = this->getErrMsg();
                }
                emit sigEntrustResult(pOrder);
            }

            if (pOrder)
            {
                m_pContext->setOrderAcked(pOrder->hSendHandle);
            }

        }
    }
    else if (FUNC_NO::ENTRUST_WITHDRAW == iFuncNo)
    {
        if (iRet == ERROR_CODE::SUCCESS)//处理成功
        {
            OrderList* pOrderList = output[PARAMETER::ORDER_LIST_OUT].value<OrderList*>();
            for (int i = 0; i < pOrderList->size(); i++)
            {
                Order* pOrder = (*pOrderList)[i];
                m_pContext->setOrderAcked(pOrder->hSendHandle);
                m_pContext->addOrderByEntrustNo(pOrder);
                emit sigEntrustResult(pOrder);
            }
            SAFE_DELETE(pOrderList);
        }
        else
        {
            OrderList* pOrderList = m_pContext->getUnackOrderList(hSend);
            if (pOrderList != nullptr)
            {
                for (int i = 0; i < pOrderList->size(); i++)
                {
                    Order* pOrder = (*pOrderList)[i];

                    pOrder->strEntrustStatus = ENTRUST_STATUS::FAIL;
                    pOrder->strFailCause = this->getErrMsg();
                    m_pContext->setOrderAcked(pOrder->hSendHandle);
                    emit sigEntrustResult(pOrder);
                }
            }
        }
    }
    else if (FUNC_NO::FUND_QUERY == iFuncNo)
    {
        if (ERROR_CODE::SUCCESS == iRet)
        {
            m_bRefreshAccountCodeDict = true;

            if (m_bRefreshAccountCodeDict && m_bRefreshCombiNoDict)
            {
                emit sigLoginResult(iRet, this->getErrMsg());
            }
        }
    }
    else if (FUNC_NO::COMBI_QUERY == iFuncNo)
    {
        if (ERROR_CODE::SUCCESS == iRet)
        {
            m_bRefreshCombiNoDict = true;

            if (m_bRefreshAccountCodeDict && m_bRefreshCombiNoDict)
            {
                emit sigLoginResult(iRet, this->getErrMsg());
            }
        }
    }


    //在CMsgCallback::OnReceivedBizMsg中AddRef()
    lpMsg->Release();
    lpMsg = nullptr;
}

void CounterThread::onPushCallback(CSubscribeInterface* lpSub, int iSubsIndex, QByteArray buf)
{
    IF2UnPacker* lpUnPack = NewUnPacker((void*)buf.data(), buf.length());

    qDebug() << "Recv push message: " << Utils::toQString(lpUnPack).toUtf8().data();

    for (int i = 0; i < lpUnPack->GetDatasetCount(); i++)
    {
        lpUnPack->SetCurrentDatasetByIndex(i);

        for (int j = 0; j < lpUnPack->GetRowCount(); j++)
        {
            lpUnPack->Go(j + 1); //取值范围[1, GetRowCount()]，fuck！！！

            //主推消息类型
            const char* pMsgType = lpUnPack->GetStr("msgtype");

            //调用业务处理器
            FuncHandler* pHandler = FuncHandlerMgr::getInstance()->getHandler(pMsgType);
            if (pHandler == nullptr)
            {
                qCritical() << "Can't find push message handler, MsgType = " << pMsgType;
                emit sigRunLog(RunLog_Error, QStringLiteral("无法处理未知的推送消息，消息类型为：%1").arg(pMsgType));
                return;
            }

            //业务解码
            QMap<QString, QVariant> output;
            int iRet = pHandler->unpack(iSubsIndex, pMsgType, lpUnPack, output);
            if (iRet != ERROR_CODE::SUCCESS)
            {
                qCritical() << "Unpack push message failed: " << iRet;

                QString strErrMsg = output[PARAMETER::ERROR_INFO].toString();
                emit sigRunLog(RunLog_Error, QStringLiteral("推送消息解码失败：%1").arg(strErrMsg));
                this->setErrMsg(strErrMsg);
            }

            Order* pOrder = output[PARAMETER::ORDER_OUT].value<Order*>();
            if (nullptr != pOrder)
            {
                emit sigEntrustResult(pOrder);
            }

        }

    }

}

//链接建立完成，可以开始工作了
void CounterThread::onConnCreated(quint32 ptr)
{

}

//链接断开
void CounterThread::onConnClosed(quint32 ptr)
{
    //用户主动退出，忽略
    if (m_bLogout)
    {
        return;
    }

    //是否已经登录成功过（即是否已经进入了主界面）。第一次登录成功后会置为false
    if (m_bFirstLogin)
    {
        return;
    }

    sigRunLog(RunLog_Error, QStringLiteral("与服务器的链接断开，请检查网络，并重启客户端"));

    return;
}

int CounterThread::doLogin()
{
    int iFuncNo = FUNC_NO::LOGIN;

    emit sigLogining(QStringLiteral("账号登录..."));
    FuncHandler* pHandler = FuncHandlerMgr::getInstance()->getHandler(iFuncNo);
    if (pHandler == nullptr)
    {
        qCritical() << "Can't find func handler: " << iFuncNo;
        return ERROR_CODE::FAIL;
    }

    int iRet = pHandler->packAndSend(m_pConnMsg, CommMode::Asy);
    if (iRet < 0)
    {
        this->setErrMsg(Utils::toQString(m_pConnMsg->GetErrorMsg(iRet)));
    }
    else
    {
        iRet = ERROR_CODE::SUCCESS;
    }
    return iRet;
}

void CounterThread::sendHeartBeat()
{
    int iFuncNo = FUNC_NO::HEARTBEAT;

    FuncHandler* pHandler = FuncHandlerMgr::getInstance()->getHandler(iFuncNo);
    if (pHandler == nullptr)
    {
        qCritical() << "Can't find func handler: " << iFuncNo;
        emit sigRunLog(RunLog_Error, QStringLiteral("无法处理未知的功能号：%1").arg(iFuncNo));

        return;
    }

    int iRet = pHandler->packAndSend(m_pConnMsg, CommMode::Asy);
    if (iRet < 0)
    {
        QString strErrMsg = Utils::toQString(m_pConnMsg->GetErrorMsg(iRet));
        
        this->setErrMsg(strErrMsg);
        qCritical() << "Send heart beat failed: " << strErrMsg;
        emit sigRunLog(RunLog_Error, QStringLiteral("发送心跳消息失败：%1").arg(strErrMsg));

        return;
    }
}

void CounterThread::onQueryFund()
{
    int iFuncNo = FUNC_NO::FUND_QUERY;

    FuncHandler* pHandler = FuncHandlerMgr::getInstance()->getHandler(iFuncNo);
    if (pHandler == nullptr)
    {
        qCritical() << "Can't find func handler: " << iFuncNo;
        emit sigRunLog(RunLog_Error, QStringLiteral("无法处理未知的功能号：%1").arg(iFuncNo));

        return;
    }

    int iRet = pHandler->packAndSend(m_pConnMsg, CommMode::Asy);
    if (iRet < 0)
    {
        QString strErrMsg = Utils::toQString(m_pConnMsg->GetErrorMsg(iRet));

        this->setErrMsg(strErrMsg);
        qCritical() << "Send fund query failed: " << strErrMsg;
        emit sigRunLog(RunLog_Error, QStringLiteral("发送产品查询消息失败：%1").arg(strErrMsg));
    }
}

void CounterThread::onQueryCombi()
{
    int iFuncNo = FUNC_NO::COMBI_QUERY;

    FuncHandler* pHandler = FuncHandlerMgr::getInstance()->getHandler(iFuncNo);
    if (pHandler == nullptr)
    {
        qCritical() << "Can't find func handler: " << iFuncNo;
        emit sigRunLog(RunLog_Error, QStringLiteral("无法处理未知的功能号：%1").arg(iFuncNo));

        return;
    }

    int iRet = pHandler->packAndSend(m_pConnMsg, CommMode::Asy);
    if (iRet < 0)
    {
        QString strErrMsg = Utils::toQString(m_pConnMsg->GetErrorMsg(iRet));

        this->setErrMsg(strErrMsg);
        qCritical() << "Send combi query failed: " << strErrMsg;
        emit sigRunLog(RunLog_Error, QStringLiteral("发送组合查询消息失败：%1").arg(strErrMsg));
    }
}

void CounterThread::onEntrustCombi(OrderList* pOrderList)
{
    for (int i = 0; i < pOrderList->size(); i++)
    {
        Order* pOrder = (*pOrderList)[i];
        emit sigEntrustResult(pOrder);
    }

    int iFuncNo = FUNC_NO::ENTRUST_COMBI;

    FuncHandler* pHandler = FuncHandlerMgr::getInstance()->getHandler(iFuncNo);
    if (pHandler == nullptr)
    {
        qCritical() << "Can't find func handler: " << iFuncNo;
        SAFE_DELETE(pOrderList);
        emit sigRunLog(RunLog_Error, QStringLiteral("无法处理未知的功能号：%1").arg(iFuncNo));
        return;
    }

    QMap<QString, QVariant> params;
    QVariant var;
    var.setValue(pOrderList);
    params.insert(PARAMETER::ORDER_LIST_IN, var);
    int hSend = pHandler->packAndSend(m_pConnMsg, CommMode::Asy, params);
    if (hSend < 0)
    {
        QString strErrMsg = Utils::toQString(m_pConnMsg->GetErrorMsg(hSend));

        this->setErrMsg(strErrMsg);
        qCritical() << "Send entrust combi failed: " << strErrMsg;
        emit sigRunLog(RunLog_Error, QStringLiteral("发送篮子委托消息失败：%1").arg(strErrMsg));
    }
    else
    {
        if (pOrderList->size() > 0)
        {
            emit sigRunLog(RunLog_Info, QString(QStringLiteral("已下达 %1 笔新订单委托").arg(pOrderList->size())));
        }

        for (int i = 0; i < pOrderList->size(); i++)
        {
            Order* pOrder = (*pOrderList)[i];
            pOrder->hSendHandle = hSend;
            emit sigNewEntrustHandled(pOrder->iInternalId);
            m_pContext->addUnackOrder(pOrder);
        }
    }

    SAFE_DELETE(pOrderList);
}

void CounterThread::onWithdraw(OrderList* pOrderList)
{
    int iFuncNo = FUNC_NO::ENTRUST_WITHDRAW;

    FuncHandler* pHandler = FuncHandlerMgr::getInstance()->getHandler(iFuncNo);
    if (pHandler == nullptr)
    {
        qCritical() << "Can't find func handler: " << iFuncNo;
        emit sigRunLog(RunLog_Error, QStringLiteral("无法处理未知的功能号：%1").arg(iFuncNo));
        return;
    }

    SAFE_NEW(OrderList, pOrignOrderList);
    for (int i = 0; i < pOrderList->size(); i++)
    {
        Order* pOrder = (*pOrderList)[i];

        Order* pOrignOrder = m_pContext->getOrderByEntrustNo(pOrder->iEntrustNo);
        if (pOrignOrder == nullptr)
        {
            //如果程序重启过，可能会找不到原委托，这时也要执行撤单
            qWarning() << "Can't find orign order: entrust_no = " << pOrder->iEntrustNo;
            //pOrignOrder = pOrder;
            continue;
        }
        else
        {
            pOrignOrderList->push_back(pOrignOrder);
        }
    }

    QMap<QString, QVariant> params;
    QVariant var;
    var.setValue(pOrderList);
    params.insert(PARAMETER::ORDER_LIST_IN, var);
    int hSend = pHandler->packAndSend(m_pConnMsg, CommMode::Asy, params);
    if (hSend < 0)
    {
        QString strErrMsg = Utils::toQString(m_pConnMsg->GetErrorMsg(hSend));

        this->setErrMsg(strErrMsg);
        qCritical() << "Send withdraw failed: " << strErrMsg;
        emit sigRunLog(RunLog_Error, QStringLiteral("发送撤单消息失败：%1").arg(strErrMsg));

        SAFE_DELETE_LIST(pOrderList);
    }
    else
    {
        if (pOrderList->size() > 0)
        {
            emit sigRunLog(RunLog_Info, QString(QStringLiteral("已下达 %1 笔撤单委托").arg(pOrderList->size())));
        }

        for (int i = 0; i < pOrignOrderList->size(); i++)
        {
            Order* pOrignOrder = (*pOrignOrderList)[i];
            pOrignOrder->hSendHandle = hSend;
            m_pContext->addUnackOrder(pOrignOrder);
        }

        for (int i = 0; i < pOrderList->size(); i++)
        {
            emit sigNewWithdrawHandled((*pOrderList)[i]->iInternalId);
        }

        SAFE_DELETE_LIST(pOrderList);
    }

    SAFE_DELETE(pOrignOrderList);//list中的每个指针不能释放
}

void CounterThread::onQueryOrderByEntrustNo(OrderList* pOrderList)
{
    SAFE_NEW(OrderList, pOrignOrderList);
    for (int i = 0; i < pOrderList->size(); i++)
    {
        Order* pOrder = (*pOrderList)[i];

        //如果没找到原委托，直接忽略，更新runconfig
        Order* pOrignOrder = m_pContext->getOrderByEntrustNo(pOrder->iEntrustNo);
        if (nullptr == pOrignOrder)
        {
            qWarning() << "Can't find origin order, iEntrustNo = " << pOrder->iEntrustNo << ", ignore";
            emit sigRunLog(RunLog_Important, QString(QStringLiteral("撤单失败，无法找到原委托（委托序号：%1）").arg(pOrder->iEntrustNo)));
            emit sigNewWithdrawHandled(pOrder->iInternalId);
        }

        pOrignOrderList->push_back(pOrignOrder);
    }
    SAFE_DELETE(pOrderList);
    emit sigQueryOrderByEntrustNoResult(pOrignOrderList);

}