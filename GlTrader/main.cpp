#include "stdafx.h"

//#include <vld.h>

#include "DlgLogin.h"
#include <QtWidgets/QApplication>

void setStyle()
{
    QFile file(QString(":/image/blue.css"));//从资源载入
    //QFile file(QString("image/blue.css"));//从外部文件载入
    file.open(QFile::ReadOnly);
    QString qss = QLatin1String(file.readAll());
    qApp->setStyleSheet(qss);
    qApp->setPalette(QPalette(QColor("#EBF2FF")));
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    setStyle();

    DlgLogin w;
    w.show();
    return a.exec();
}
